/* 
 * File:   tcp_cleint.h
 * Author: neer
 *
 * Created on 4 March, 2016, 1:40 PM
 */

#ifndef TCP_CLIENT_H
#define	TCP_CLIENT_H

#ifdef	__cplusplus
extern "C" {
#endif

    extern unsigned int TcpClientPut(unsigned char ch);
    extern unsigned int TcpClientPutS(char *str);
    extern unsigned int TcpClientGetEmptyByteCntTx(void);
    extern unsigned int TcpClientIsAvailable(void);
    extern unsigned int TcpClientGetS(char *str);
    extern unsigned int TcpClientDisconnect(void);
    extern unsigned int TcpClientConnect(void);
    extern unsigned int TcpClientGet(char *arr, unsigned int len);
    extern unsigned int TcpClientGetBody(char *body);
#ifdef	__cplusplus
}
#endif

#endif	/* TCP_CLIENT_H */

