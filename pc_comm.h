/* 
 * File:   pc_comm.h
 * Author: neer
 *
 * Created on 4 March, 2016, 11:30 AM
 */

#ifndef PC_COMM_H
#define	PC_COMM_H

#ifdef	__cplusplus
extern "C" {
#endif

    extern void PcCommExecuteCmd(char *cmdStr, char *replyStr);


#ifdef	__cplusplus
}
#endif

#endif	/* SRVCMDS_H */

