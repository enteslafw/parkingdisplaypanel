
#include <xc.h>
//#include "EnteslaMifareRW.h"
#include "GenericTypeDefs.h"
#include "global_vars.h"
#include "app_funcs.h"
#include "helper.h"
#include "log.h"
#include "Delay.h"
#include "tcp_client.h"
#include "IO.h"



void TASK_CheckInputs(void *pArgStr)
{
    curInStat.in1Stat = INPUT_1_PIN;
    curInStat.in2Stat = INPUT_2_PIN;
    curInStat.in3Stat = INPUT_3_PIN;
    curInStat.in4Stat = INPUT_4_PIN;
    
    if(curInStat.in1Stat != PrevInStat.in1Stat)
    {
        PrevInStat.in1Stat = curInStat.in1Stat;
        systemFlag.in1StatChange = 1;
    }
    
      if(curInStat.in2Stat != PrevInStat.in2Stat)
    {
        PrevInStat.in2Stat = curInStat.in2Stat;
        systemFlag.in2StatChange = 1;
    }
    
    if(curInStat.in3Stat != PrevInStat.in3Stat)
    {
        PrevInStat.in3Stat = curInStat.in3Stat;
        systemFlag.in3StatChange = 1;
    }
    
    if(curInStat.in4Stat != PrevInStat.in4Stat)
    {
        PrevInStat.in4Stat = curInStat.in4Stat;
        systemFlag.in4StatChange = 1;
    }
}

void TASK_CheckCard(void *pArgStr)
{
#if 0
    static TICKS cardTimeOutMs = 0;
    static char resp[32];
    static unsigned int i, size;
    static char prevUid[10];
    volatile unsigned int cnt;
    //    if (EMRW_CMD_SR == emrwLastExecCmd)
    {
        if (SizeOfEmrwCmdResponse() >= 3)
        {
            EmrwGetResponseS(resp);
            if ('N' == resp[0])
            {
                EmrwPutS("SL\r");
                return;
            }
            if ((MULTITASKER_GET_TICK() - cardTimeOutMs) > 500)
            {
                prevUid[0] = 0;
                cardTimeOutMs = MULTITASKER_GET_TICK();
            }
            cardTimeOutMs = MULTITASKER_GET_TICK();
            for (i = 0; i < 4; i++)
            {
                if (prevUid[i] != resp[i + 1])
                {
                    systemFlag.cardFound = 1;
                }
                prevUid[i] = resp[i + 1];
            }

            if (0 == systemFlag.cardFound)
            {
                EmrwPutS("SL\r");
                Nop();
                return;
            }

            systemFlag.cardFound = 0;
            EmrwPutS("SL\r");
            Nop();
            if (systemFlag.clientCnxStatus)
            {
                TcpClientPut(ConvertToHexHigherNibble(resp[1]));
                TcpClientPut(ConvertToHexLowerNibble(resp[1]));

                TcpClientPut(ConvertToHexHigherNibble(resp[2]));
                TcpClientPut(ConvertToHexLowerNibble(resp[2]));

                TcpClientPut(ConvertToHexHigherNibble(resp[3]));
                TcpClientPut(ConvertToHexLowerNibble(resp[3]));

                TcpClientPut(ConvertToHexHigherNibble(resp[4]));
                TcpClientPut(ConvertToHexLowerNibble(resp[4]));

                for (cnt = 0; cnt < 379; cnt++)
                {
                    _LATD1 ^= 1;
                    Delay10us(33);
                }
                //                _LATD1 = 1;
                //                DelayMs(125);
                _LATD1 = 0;
                DelayMs(66);
                //                _LATD1 = 1;
                for (cnt = 0; cnt < 750; cnt++)
                {
                    _LATD1 ^= 1;
                    Delay10us(20);
                }
                //                DelayMs(150);
                _LATD1 = 0;
            }
        }
    }
#endif
}
