
#ifndef GLCD_H
#define GLCD_H    
 
//*****USER SETTINGS************

#define GLCD_DATA_PORT      LATE
#define GLCD_DATA_IN        (PORTE&0x00FF)
#define GLCD_DATA_DIR       TRISE

//***End of USER SETTINGS*******    

#define GLCD_BLACK  1
#define GLCD_WHITE  0

#define GLCD_LINE_2     9
#define GLCD_LINE_3     16
#define GLCD_LINE_4     24
#define GLCD_LINE_5     32
#define GLCD_LINE_6     40
#define GLCD_LINE_7     48
#define GLCD_LINE_8     56

//*******Public Protoype **********
void GlcdInit(void);
void GlcdSetCursor(unsigned char xPos, unsigned char yPos);
void GlcdIncCursor(unsigned char mode);
void GlcdPixelColor(unsigned char xPos, unsigned char yPos, unsigned char color);
void GlcdHoriLine(unsigned char y, unsigned char x1, unsigned char x2);
void GlcdVertLine(unsigned char x, unsigned char y1, unsigned char y2);
void GlcdDrawBox(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2);
void GlcdDrawLine(char x1, char y1, char x2, char y2);
void GlcdDrawCircle(unsigned int cx, unsigned int cy, unsigned int radius);
void GlcdPutCh(unsigned char data);
void GlcdPutS(char *text);
void GlcdPutSHighlight(char *text, unsigned char start, unsigned char end);
void GlcdPutImage(unsigned char *image);
void GlcdClrScreen(void);
void GlcdSetColor(unsigned char colorToSet);
void GlcdPutSWithCursor(char *text);
//*****End of Public Protoype ******

#endif //GLCD_H
