
#include <xc.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
//#include "GenericTypeDefs.h"
#include "global_vars.h"
#include "pc_comm.h"
#include "multitasker.h"
#include "CircularBuffer.h"
#include "delay.h"
#include "helper.h"
#include "tcp_client.h"
//#include "EnteslaMifareRW.h"
#include "app_funcs.h"
#include "uart2.h"
#include "En_Consol.h"

static void _GetIp(char *);
static void _GetPort(char *replyStr);
static void _GetCal(char *);
static void _GetTime(char *replyStr);
static void _GetRtc(char *replyStr);

static void _SetSrvIp(char *replyStr);
static void _SetRtcTime(char *replyStr);
static void _SetRtcCal(char *replyStr);
static void _SetSrvPort(char *replyStr);

static void _SetLedMsg(char *replyStr);
static void _SetDefLFile(char *replyStr);
static void _SetDefPFile(char *replyStr);
static void _SetDefNFile(char *replyStr);

static void _SendLedMsg(char *msg, char *replyStr);

typedef void(*pfnSetCb)(char *replyBuf);

typedef struct
{
    unsigned int numOfArgs;
    char *setStr; // variable to be set
    pfnSetCb pFnSetCb; // A function pointer to response callback function.
} PcCommSetCmdEntry;
PcCommSetCmdEntry PcCommSetCmdTable[] = {

    //    {
    //        .numOfArgs = 3,
    //        .setStr = "BLOCK",
    //        .pFnSetCb = _SetBlock
    //    },
    {
        .numOfArgs = 1,
        .setStr = "SRVIP",
        .pFnSetCb = _SetSrvIp
    },

    {
        .numOfArgs = 1,
        .setStr = "SRVPORT",
        .pFnSetCb = _SetSrvPort
    },

    {
        .numOfArgs = 1,
        .setStr = "TIME",
        .pFnSetCb = _SetRtcTime
    },

    {
        .numOfArgs = 1,
        .setStr = "CAL",
        .pFnSetCb = _SetRtcCal
    },


    {
        .numOfArgs = 1,
        .setStr = "DIGIT",
        .pFnSetCb = _SetLedMsg
    },

    {
        .numOfArgs = 0,
        .setStr = "DEFLFILE",
        .pFnSetCb = _SetDefLFile
    },

    {
        .numOfArgs = 0,
        .setStr = "DEFPFILE",
        .pFnSetCb = _SetDefPFile
    },

    {
        .numOfArgs = 0,
        .setStr = "DEFNFILE",
        .pFnSetCb = _SetDefNFile
    },

    {
        .numOfArgs = 0,
        .setStr = NULL,
        .pFnSetCb = NULL
    }
};

typedef void(*pfnGetCb)(char *replyBuf);

typedef struct
{
    unsigned int numOfArgs;
    char *getStr; // variable to be get
    pfnGetCb pFnGetCb; // A function pointer to response callback function.
} PcCommGetCmdEntry;
PcCommGetCmdEntry PcCommGetCmdTable[] = {
    //    {
    //        .numOfArgs = 2,
    //        .getStr = "BLOCK",
    //        .pFnGetCb = _GetBlock
    //    },

    {
        .numOfArgs = 0,
        .getStr = "SRVIP",
        .pFnGetCb = _GetIp
    },

    {
        .numOfArgs = 0,
        .getStr = "SRVPORT",
        .pFnGetCb = _GetPort
    },

    {
        .numOfArgs = 0,
        .getStr = "CAL",
        .pFnGetCb = _GetCal
    },

    {
        .numOfArgs = 0,
        .getStr = "TIME",
        .pFnGetCb = _GetTime
    },

    {
        .numOfArgs = 0,
        .getStr = "RTC",
        .pFnGetCb = _GetRtc
    },

    {
        .numOfArgs = 0,
        .getStr = NULL,
        .pFnGetCb = NULL
    }
};

static char *cmd, *token, *arg[10];

void PcCommExecuteCmd(char *cmdStr, char *replyStr)
{
    PcCommSetCmdEntry *pSetCmdEntry;
    PcCommGetCmdEntry *pGetCmdEntry;
    //    EMRW_CMD_PARAM cmdParam;
    unsigned int status, numOfArguments;

    pSetCmdEntry = PcCommSetCmdTable;
    pGetCmdEntry = PcCommGetCmdTable;

    if (strlen(cmdStr) > 0)
    {
        numOfArguments = 0;
        /* get the first token */
        token = strtok(cmdStr, " ");
        if (token != NULL)
        {
            cmd = token;
            token = strtok(NULL, " ");
            /* walk through other tokens */
            while (token != NULL)
            {
                arg[numOfArguments] = token;
                numOfArguments++;
                token = strtok(NULL, " ");
            }

            if (0 == strcmp(cmd, "SET"))
            {
                while (pSetCmdEntry->setStr != NULL)
                {
                    if (0 == strcmp(arg[0], pSetCmdEntry->setStr))
                    {
                        if (numOfArguments != pSetCmdEntry->numOfArgs + 1)
                        {
                            strcpy(replyStr, "ERROR\r\n");
                            return;
                        }
                        else
                        {
                            pSetCmdEntry->pFnSetCb(replyStr);
                            return;
                        }
                    }
                    else
                    {
                        pSetCmdEntry++;
                    }
                }
                if (pSetCmdEntry->setStr == NULL)
                {
                    strcpy(replyStr, "ERROR\r\n");
                }
            }
            else if (0 == strcmp(cmd, "GET"))
            {
                while (pGetCmdEntry->getStr != NULL)
                {
                    if (0 == strcmp(arg[0], pGetCmdEntry->getStr))
                    {
                        /*if (numOfArguments != pGetCmdEntry->numOfArgs + 1)
                        {
                            strcpy(replyStr, "ERROR\r\n");
                            return;
                        }*/
                        if (numOfArguments >= 2)
                        {
                            strcpy(replyStr, "ONLY ONE GET 'COMMAND' POSSIBLE\r\n");
                            return;
                        }
                        else if (0 == numOfArguments)
                        {
                            strcpy(replyStr, "GET 'COMMAND' MISSING\r\n");
                            return;
                        }
                        else
                        {
                            pGetCmdEntry->pFnGetCb(replyStr);
                            return;
                        }
                    }
                    else
                    {
                        pGetCmdEntry++;
                    }
                }
                if (pGetCmdEntry->getStr == NULL)
                {
                    strcpy(replyStr, "ERROR\r\n");
                }
            }
            else if (0 == strcmp(cmd, "SAVE"))
            {
                if (numOfArguments >= 1)
                {
                    strcpy(replyStr, "ERROR\r\n");
                }
                else
                {
                    systemFlag.updateSysParamFile = 1;
                    return;
                }
            }
                //            else if (0 == strcmp(cmd, "STOP"))
                //            {
                //                cmdParam.cmd = EMRW_CMD_TR;
                //                status = EmrwExecCmd(&cmdParam);
                //                strcpy(replyStr, "OK\r\n");
                //            }
            else if (0 == strcmp(cmd, "RST"))
            {
                Reset();
            }
            else if (0 == strcmp(cmd, "R"))
            {
                systemFlag.setDefLFile = 1;
                systemFlag.setDefPFile = 1;
                systemFlag.setDefNFile = 1;
                strcpy(replyStr, "OK\r\n");
            }
            else if ((cmd[0] == '<'))
            {
                _SendLedMsg(cmd, replyStr);
            }
            else
            {
                strcpy(replyStr, "INVALID CMD\r\n");
            }
        }
        else
        {
            strcpy(replyStr, "ERROR\r\n");
        }
    }
    systemFlag.pcCommCmdRxd = 0;
}

static void _SetSrvIp(char *replyStr)
{
    char *ipStr;

    ipStr = arg[1];
    strcpy(systemParam.srvIp, ipStr);
    AppSaveSystemParamToFile(&systemParam);
    strcpy(replyStr, "OK\r\n");
}

static void _SetSrvPort(char *replyStr)
{
    char *pStr;

    pStr = arg[1];
    if (strlen(pStr) > 5)
    {
        strcpy(replyStr, "ERROR: INVALID PORT NUM\r\n");
        return;
    }
    if (!IsNum(pStr))
    {
        strcpy(replyStr, "ERROR: NOT NUM!\r\n");
        return;
    }
    strcpy(systemParam.srvPort, pStr);
    AppSaveSystemParamToFile(&systemParam);
    strcpy(replyStr, "OK\r\n");
}

static void _SetLedMsg(char *replyStr)
{
    UART2PrintString("SET DIGIT ");
    UART2PrintString(arg[1]);
    UART2PrintString("\r");
    //    strcpy(systemParam.ledMatrixMsg, arg[1]);
    strcpy(replyStr, "OK\r\n");
}

static void _SendLedMsg(char *msg, char *replyStr)
{
    char cmnd[3], devID[5], digitVal[5];
    int i = 0, idMatched = 0;
    DWORD_VAL tmp;

    if (strlen(replyStr) != 12)
    {
        replyStr[0] = 0;
        return;
    }

    //send through modbus protocol
    if (msg[0] == '<' && msg[11] == '>')
    {

        //         if ((msg[1] >= '0' && msg[1] <= '9') && ((msg[2] >= '0' && msg[2] <= '9')))
        cmnd[0] = msg[1];
        cmnd[1] = msg[2];
        cmnd[2] = 0;

        devID[0] = msg[3];
        devID[1] = msg[4];
        devID[2] = msg[5];
        devID[3] = msg[6];
        devID[4] = 0;

        digitVal[0] = msg[7];
        digitVal[1] = msg[8];
        digitVal[2] = msg[9];
        digitVal[3] = msg[10];
        digitVal[4] = 0;

        switch (atoi(cmnd))
        {
        case 3://Put Value, <03DDDDVVVV>
            for (i = 0; i < MAX_LED_DISPLAY; i++)
            {
                if (0 == strcmp(systemParam.ledDisplayParam[i].devId, devID))
                {
                    //devId matched, send msg to it's associated modbus id
                    //if (1 == systemParam.ledDisplayParam[i].isEnable)
                    {
                        idMatched = 1;
                        strcpy(systemParam.ledDisplayParam[i].lastValue, digitVal);
                        systemFlag.updateLedDisplay[i] = 1;
                        systemFlag.isConnected[i] = 0;
                        //                        ModbusPresetMultipleRegisters((i + 1), 0x0001, 0x0002, digitVal);
                        //                        DelayMs(1000);
                        //                        if (ModbusIsAvailable() > 0)
                        //                        {
                        //                            ;
                        //                        }
                    }
                }
            }
            if (1 == idMatched)
            {
                replyStr[0] = 'Y';
                replyStr[1] = 0;
                AppSaveSystemParamToFile(&systemParam);
            }
            else
            {
                replyStr[0] = 0;
            }
            break;

        case 4://Get Value, <04DDDDXXXX>
            for (i = 0; i < MAX_LED_DISPLAY; i++)
            {
                if (0 == strcmp(systemParam.ledDisplayParam[i].devId, devID))
                {
                    //devId matched, send msg to it's associated modbus id
                    idMatched = 1;
                    strcpy(replyStr, systemParam.ledDisplayParam[i].lastValue);
                    //                    replyStr[0] = systemParam.ledDisplayParam[i].lastValue[0];
                    //                    replyStr[1] = systemParam.ledDisplayParam[i].lastValue[1];
                    //                    replyStr[2] = systemParam.ledDisplayParam[i].lastValue[2];
                    //                    replyStr[3] = systemParam.ledDisplayParam[i].lastValue[3];
                    break;
                }
            }
            if (0 == idMatched)
            {
                replyStr[0] = 0;
            }
            break;

        case 1://Add Value, <01DDDDVVVV>
            for (i = 0; i < MAX_LED_DISPLAY; i++)
            {
                if (0 == strcmp(systemParam.ledDisplayParam[i].devId, devID))
                {
                    idMatched = 1;
                    tmp.Val = atoi(digitVal) + atoi(systemParam.ledDisplayParam[i].lastValue);
                    if ((tmp.Val < 0L) || (tmp.Val > 9999L))
                    {
                        replyStr[0] = 'W';
                        replyStr[1] = 0;
                        return;
                    }
                    else
                    {
                        UintToAsciiFourBytes(tmp.Val, systemParam.ledDisplayParam[i].lastValue);
                        systemFlag.updateLedDisplay[i] = 1;
                    }
                }
            }
            if (1 == idMatched)
            {
                replyStr[0] = 'Y';
                replyStr[1] = 0;
                AppSaveSystemParamToFile(&systemParam);
            }
            else
            {
                replyStr[0] = 0;
            }
            break;

        case 2://Subtract Value, <02DDDDVVVV>
            for (i = 0; i < MAX_LED_DISPLAY; i++)
            {
                if (0 == strcmp(systemParam.ledDisplayParam[i].devId, devID))
                {
                    idMatched = 1;
                    tmp.Val = atoi(systemParam.ledDisplayParam[i].lastValue) - atoi(digitVal);
                    if ((tmp.Val < 0L) || (tmp.Val > 9999L))
                    {
                        replyStr[0] = 'W';
                        replyStr[1] = 0;
                        return;
                    }
                    else
                    {
                        UintToAsciiFourBytes(tmp.Val, systemParam.ledDisplayParam[i].lastValue);
                        systemFlag.updateLedDisplay[i] = 1;
                    }
                }
            }
            if (1 == idMatched)
            {
                replyStr[0] = 'Y';
                replyStr[1] = 0;
                AppSaveSystemParamToFile(&systemParam);
            }
            else
            {
                replyStr[0] = 0;
            }
            break;

        case 9://Set LED Brightness, <09DDDDVVVV>
            for (i = 0; i < MAX_LED_DISPLAY; i++)
            {
                if (0 == strcmp(systemParam.ledDisplayParam[i].devId, devID))
                {
                    //devId matched, send msg to it's associated modbus id
                    //if (1 == systemParam.ledDisplayParam[i].isEnable)
                    {
                        idMatched = 1;
                        systemParam.ledDisplayParam[i].brightness = digitVal[3];
                        systemFlag.updateLedDisplay[i] = 1;
                        systemFlag.isConnected[i] = 0;
                    }
                }
            }
            if (1 == idMatched)
            {
                replyStr[0] = 'Y';
                replyStr[1] = 0;
                AppSaveSystemParamToFile(&systemParam);
            }
            else
            {
                replyStr[0] = 0;
            }
            break;

        default://Unknown Command
            replyStr[0] = 'N';
            replyStr[1] = 0;
            break;
        }

        //replyStr[1] = 0;
    }
    else
    {
        strcpy(replyStr, "ERROR\r\n");
    }
}

static void _SetRtcTime(char *replyStr)
{
    char *timeStr;

    timeStr = arg[1];
    if ((strlen(timeStr) != 4) || (0 == IsNum(timeStr)))
    {
        strcpy(replyStr, "INVALID ARG\r\n");
    }
    else
    {
        setRtcTime.hour = (timeStr[0] - '0')*10 + (timeStr[1] - '0');
        setRtcTime.min = (timeStr[2] - '0')*10 + (timeStr[3] - '0');
        setRtcTime.sec = 0;
        RTCSetTime(&setRtcTime);
        strcpy(replyStr, "OK\r\n");
    }
}

static void _SetRtcCal(char *replyStr)
{
    char *calStr;

    calStr = arg[1];
    if ((strlen(calStr) != 6) || (0 == IsNum(calStr)))
    {
        strcpy(replyStr, "INVALID ARG\r\n");
    }
    else
    {
        setRtcCal.date = (calStr[0] - '0')*10 + (calStr[1] - '0');
        setRtcCal.month = (calStr[2] - '0')*10 + (calStr[3] - '0');
        setRtcCal.year = (calStr[4] - '0')*10 + (calStr[5]) - '0';
        RTCSetCalendar(&setRtcCal);
        strcpy(replyStr, "OK\r\n");
    }

}

static void _SetDefLFile(char *replyStr)
{
    systemFlag.setDefLFile = 1;
    replyStr[0] = 0;
}

static void _SetDefPFile(char *replyStr)
{
    systemFlag.setDefPFile = 1;
    replyStr[0] = 0;
}

static void _SetDefNFile(char *replyStr)
{
    systemFlag.setDefNFile = 1;
    replyStr[0] = 0;
}

static void _GetIp(char *replyStr)
{
    Nop();
    strcpy(replyStr, systemParam.srvIp);
    strcat(replyStr, "\r\n");
}

static void _GetPort(char *replyStr)
{
    strcpy(replyStr, systemParam.srvPort);
    strcat(replyStr, "\r\n");
}

static void _GetTime(char *replyStr)
{
    Nop();
    RTCGetTimeInStrWithSeperator(replyStr, ':');
    strcat(replyStr, "\r\n");
}

static void _GetCal(char *replyStr)
{
    Nop();
    RTCGetCalendarInStrWithSeperator(replyStr, '/');
    strcat(replyStr, "\r\n");
}

static void _GetRtc(char *replyStr)
{
    TIME time;
    CALENDAR calendar;

    RTCGetTime(&time);
    RTCGetCalendar(&calendar);

    replyStr[0] = calendar.date / 10 + '0';
    replyStr[1] = calendar.date % 10 + '0';

    replyStr[2] = '/';

    replyStr[3] = calendar.month / 10 + '0';
    replyStr[4] = calendar.month % 10 + '0';

    replyStr[5] = '/';

    replyStr[6] = calendar.year / 10 + '0';
    replyStr[7] = calendar.year % 10 + '0';

    replyStr[8] = ' ';

    replyStr[9] = time.hour / 10 + '0';
    replyStr[10] = time.hour % 10 + '0';

    replyStr[11] = ':';

    replyStr[12] = time.min / 10 + '0';
    replyStr[13] = time.min % 10 + '0';

    replyStr[14] = ':';

    replyStr[15] = time.sec / 10 + '0';
    replyStr[16] = time.sec % 10 + '0';

    replyStr[17] = 0;

    strcat(replyStr, "\r\n");

}
