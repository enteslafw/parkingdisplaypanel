<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Entesla LED Display Panel</title>
<link href="/mchp.css" rel="stylesheet" type="text/css" />
<script src="/mchp.js" type="text/javascript"></script>
</head>

<body>
<div id="shadow-one"><div id="shadow-two"><div id="shadow-three"><div id="shadow-four">
<div id="page">

<div style="padding:0 0 5px 5px"><img src="/entesla.png" alt="EnTesla" /></div>
<div id="title"><div class="right">Entesla LED Display Panel</div>

<span id="hello">&nbsp;</span>
</div>


<div id="menu">
<a href="/index.htm">Home</a>
<!--a href="/index_og.htm">Home og</a-->
<!--a href="/index1.htm">Home1</a-->
<!--a href="/forms.htm">form</a-->
<!--a href="/cookies.htm">Cookies</a-->
<!--a href="/upload_Image.htm">Image Uploads</a-->
<!--a href="/email">Send E-mail</a-->
<!--a href="/dyndns">Dynamic DNS</a-->

<!--a href="/protect/serverconfig.htm">Server Configuration</a-->
<a href="/protect/config.htm">Network Configuration</a>
<a href="/protect/devicesetting.htm">Device Setting</a>
<!--a href="/snmp/snmpconfig.htm">SNMP Configuration</a-->
<!--a href="/protect/restore.htm">Restore</a -->
<!--a href="/backup.htm">Backup</a-->
<a href="/protect/factorysettings.htm">Factory Defaults</a>
<a href="/protect/auth.htm">Authentication</a>
<a href="/protect/upload.htm">Firmware Upgrade</a>
<!--a href="/protect/download.htm">Search Firmware</a-->
<a href="/restart.htm">Reboot Device</a>
</div>