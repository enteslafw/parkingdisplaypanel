
#include "CircularBuffer.h"

void InitQueue(CIRCULAR_BUFFER *cirBuff)
{
    cirBuff->start = 0;
    cirBuff->end = 0;
    cirBuff->current = 0;
}

void ClearQueue(CIRCULAR_BUFFER *cirBuff)
{
    cirBuff->start = 0;
    cirBuff->end = 0;
    cirBuff->current = 0;
}

unsigned int PushToQueueS(CIRCULAR_BUFFER *cirBuff, char *str)
{
    while (*str)
    {
        PushToQueue(cirBuff, *str++);
    }
    return 0;
}

unsigned int PushToQueue(CIRCULAR_BUFFER *cirBuff, char ch)
{
    cirBuff->buffer[cirBuff->end] = ch;
    cirBuff->end = (cirBuff->end + 1) % BUFFER_SIZE;

    if (cirBuff->current < BUFFER_SIZE)
    {
        cirBuff->current++;
    }
    else
    {
        // Overwriting the oldest. Move start to next-oldest
        cirBuff->start = (cirBuff->start + 1) % BUFFER_SIZE;
    }

    return 0;
}

unsigned int PopFromQueue(CIRCULAR_BUFFER *cirBuff, char *pCh)
{
    if (!cirBuff->current)
    {
        return 1;
    }

    *pCh = cirBuff->buffer[cirBuff->start];
    cirBuff->start = (cirBuff->start + 1) % BUFFER_SIZE;

    cirBuff->current--;
    return 0;
}

// returns size of string peeked

unsigned int PeekQueue(CIRCULAR_BUFFER *cirBuff, char *str, unsigned int size)
{
    unsigned int current = cirBuff->current, start = cirBuff->start;
    unsigned int i;

    if (!current)
    {
        return 0;
    }

    if (current > size)
    {
        current = size;
    }

    for (i = 0; i < current; i++)
    {
        str[i] = cirBuff->buffer[start];
        start = (start + 1) % BUFFER_SIZE;
    }
    //str[i] = 0;
    return i;
}

unsigned int GetFromQueue(CIRCULAR_BUFFER *cirBuff, char *pCh, unsigned int loc)
{
    if (!cirBuff->current)
    {
        return 1;
    }

    if (loc >= cirBuff->current)
    {
        return 1;
    }

    *pCh = cirBuff->buffer[(cirBuff->start + loc) % BUFFER_SIZE];

    return 0;
}
unsigned int SizeOfQueue(CIRCULAR_BUFFER *cirBuff)
{
    return cirBuff->current;
}

unsigned int SizeEmptyInQueue(CIRCULAR_BUFFER *cirBuff)
{
    return BUFFER_SIZE - cirBuff->current;
}

//
//SeeLastInQueue(CIRCULAR_BUFFER *cirBuff, char *pCh)
//This function enables the calling function to see the
//last character entered in the queue, without making
//any changes to the queue of the values of the circular buffer
//

unsigned int SeeLastInQueue(CIRCULAR_BUFFER *cirBuff, char *pCh)
{
    if (!cirBuff->current)
    {
        return 1;
    }

    *pCh = cirBuff->buffer[((cirBuff->end - 1) % BUFFER_SIZE)];
    return 0;
}

//
//unsigned int UpdateLastInQueue(CIRCULAR_BUFFER *cirBuff, char ch)
//This function will overwrite the last character on the queue
//

unsigned int UpdateLastInQueue(CIRCULAR_BUFFER *cirBuff, char ch)
{
    cirBuff->buffer[((cirBuff->end - 1) % BUFFER_SIZE)] = ch;

    return 0;
}

//Place holders for Future Development

unsigned int SeekQueue(void)
{
    return 0;
}

unsigned int FindInQueue(void)
{
    return 0;
}

unsigned int StrCpyQueue(char *DestStr, CIRCULAR_BUFFER *cirBuff)
{
    unsigned int BytesCopied;

    BytesCopied = 0;
    while (!PopFromQueue(cirBuff, DestStr + BytesCopied++));
    DestStr[BytesCopied] = 0;

    return (BytesCopied);

}

unsigned int StrStrQueue(char *searchStr, CIRCULAR_BUFFER *cirBuff)
{
    int strLen, startLoc = 0, i = 0, stat=0;
    char ch;
    strLen = strlen(searchStr);

    do
    {
        stat = GetFromQueue(cirBuff, &ch, startLoc);
        startLoc++;
        if (ch == searchStr[i])
        {
            for (i = 1; i < strLen; i++)
            {
                GetFromQueue(cirBuff, &ch, startLoc);                
                if (ch != searchStr[i]) //no match
                    break; //return (0);
                startLoc++;
            }
            if (i == strLen)
                return (startLoc - strLen);
            i=0;
        }

    }
    while (0 == stat);

    return (0);
}