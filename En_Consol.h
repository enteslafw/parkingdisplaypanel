/* 
 * File:   En_Consol.h
 * Author: Umesh
 *
 * Created on September 3, 2016, 1:07 PM
 */

#ifndef EN_CONSOL_H
#define	EN_CONSOL_H

#ifdef	__cplusplus
extern "C" {
#endif

extern unsigned char PcCommPortOpen(unsigned long baudRateToSet, unsigned long timeoutMs);
extern void PcComPutNum(unsigned int n);
extern void PcCommPutS(char *s);
extern void PcCommPutCh(char ch);
extern void PcCommPutSRx(char *str);
extern void PcCommPutROMString(ROM char* str);
extern void PcCommPutStringLen(BYTE *s, int Len);
extern void PcCommPrintHexChar(BYTE toPrint);
extern void PcCommPrintMAC(BYTE *s, BYTE Len);
extern void PcCommGetCh(char *ch);
extern unsigned int PcCommIsAvailable(void);
extern void PcCommClearRxBuff(void);
extern unsigned int PcCommGetS(char *str);
extern unsigned int  PcCommGetStringLen(char *str, int Len);

#ifdef	__cplusplus
}
#endif

#endif	/* EN_CONSOL_H */

