/*
    FTP Client Initialization and Access Routines
    PIC32MX695F512L - 100 pin TQFP
    Operating from Primary Oscillator with PLL @ 8 MHz / 2 * 20 PLL => 80 MHz Fosc => 80 MHz Fcy

    Initial Code by Scott Holland, �Code Development

 */

#define FTP_GREETING					(0u)
#define FTP_USER						(1u)
#define FTP_PASS						(2u)
#define FTP_CWD							(3u)
#define FTP_STAT						(4u)
#define FTP_SIZE						(5u)
#define FTP_TYPE						(6u)
#define FTP_PASV						(7u)
#define FTP_RETR						(8u)
#define FTP_RETR_DATA					(9u)
#define FTP_SEND						(10u)
#define FTP_DELE						(11u)
#define FTP_STOR						(12u)
#define FTP_RECV						(13u)
#define FTP_QUIT						(14u)

//smFTPC is a state machine

typedef enum _SM_FTPC { /*do not alter sequence here*/
    SM_FTPC_HOME,
    SM_FTPC_CONNECTWAIT, //command = open socket 
    SM_FTPC_GET_GREETING, //response = 220 welcome message
    SM_FTPC_SEND_USER, //command = USER _username_
    SM_FTPC_GET_USER, //response = 331 Password required for _username_
    SM_FTPC_SEND_PASS, //command = PASS _password_
    SM_FTPC_GET_PASS, //response = 230 user successfully logged in (530 login or password incorrect)


    SM_FTPC_SEND_CWD, //command = CWD _directory name_ (create directory)
    SM_FTPC_GET_CWD, //response = 250 (550 directory not found)

    //SM_FTPC_SEND_PWD,  //command = PWD  asking current directory
    //SM_FTPC_GET_PWD,   //response = 257 "/" is current directory



    SM_FTPC_SEND_PASV, //command = PASV    
    SM_FTPC_GET_PASV, //response = 227 Entering Passive mode (xxx,xxx,xxx,xxx,hhh,lll)

    SM_FTPC_CONNECT_DATA,
    SM_FTPC_WAIT_DATA,

    SM_FTPC_SEND_STOR,
    SM_FTPC_GET_STOR,

    SM_FTPC_SEND_ARRAY_DATA,
    //SM_FTPC_SEND_MORE,

    SM_FTPC_SEND_FILE_DATA,

    SM_FTPC_SEND_SIZE, //command = SIZE _file name_
    SM_FTPC_GET_SIZE, //response = 213 NO_OF_BYTES         
    SM_FTPC_SEND_RETR,
    SM_FTPC_GET_RETR,

    //        SM_FTPC_GET_DATA,
    SM_FTPC_RECV_FILE_DATA,
    SM_FTPC_RECV_ARRAY_DATA,

    //SM_FTPC_SEND_STAT,
    //SM_FTPC_GET_STAT,

    //SM_FTPC_SEND_COMPLETE,
    //SM_FTPC_SEND_DELE,
    //SM_FTPC_GET_DELE,
    //	SM_FTPC_SEND_CWD2,
    //	SM_FTPC_GET_CWD2,
    //	SM_FTPC_SEND_TYPE2,
    //	SM_FTPC_GET_TYPE2,
    //	SM_FTPC_SEND_PASV2,
    //	SM_FTPC_GET_PASV2,
    //	SM_FTPC_CONNECT_DATA2,
    //	SM_FTPC_WAIT_DATA2,


    //	SM_FTPC_WAIT_CLOSE_DATA2,


    SM_FTPC_WAIT_CLOSE_DATA,
    SM_FTPC_SEND_QUIT,
    SM_FTPC_GET_QUIT,
    SM_FTPC_DISCONNECT,
    SM_FTPC_DONE,

    SM_FTPC_SEND_TYPE, //cmommand = TYPE A (A: ASCII, I:BINARY)
    SM_FTPC_GET_TYPE //resposne = 200 Type Set to A
    /*some more FTP commands
     * Cmd = LIST /_directory name_
     * res = 150 Opening ASCII mode data connection for directory list
     * cmd = MKD /_directory name_ create directory
     * res = 550 directory already exist
     * cmd = SYST
     * res = 215 UNIX emulated by _server name_
     * cmd = FEAT 
     * res = 211 - extension supported
     * cmd = SIZE _file name_
     * res = 213 _no of bytes_
     * cmd = MDTM _file name_
     * res = 213 20160409110847.000 (YYYYMMDDHHMMSS.Msec)
     * cmd = RETR _file name_
     * res = 150 opening BINARY mode data connection for file transfer
     * 
     * UNR = 226 Transfer Complete
     * UNR = 426 Connection timeout, aborting transfer
     * UNR = 502 Command not implemented
     * UNR = 450 cant access file
     */
} SM_FTPC;

typedef enum _FTPC_CURR_PROC {
    FTPC_UPLOAD = 0,
    FTPC_DOWNLOAD
} FTPC_CURR_PROC;

typedef enum _FTP_FOR {
    FTP_UPLOAD_FILE = 0,
    FTP_DOWNLOAD_FILE,
    FTP_UPLOAD_ARRAY,
    FTP_DOWNLOAD_ARRAY

} FTP_CNX_FOR;

typedef enum _FTP_CNX_STATUS {
    FTP_CNX_COMMAND_SOCKET_OPENNING,
    FTP_CNX_COMMAND_SOCKET_OPENED,

    FTP_CNX_VALIDATING_USER,
    FTP_CNX_VALIDEDTED_USER,

    FTP_CNX_OPENNING_DIR,
    FTP_CNX_OPENNING_ASCII_MODE,
    FTP_CNX_OPENNING_PASSIVE_MODE,

    FTP_CNX_DATA_SOCKET_OPENNING,
    FTP_CNX_ACCESSING_UP_FILE,
    FTP_CNX_UPLOADING_FILE,
    FTP_CNX_UPLOADING_ARRAY,

    FTP_CNX_ACCESSING_DOWN_FILE,
    FTP_CNX_DOWNLOADING_FILE,

    FTP_CNX_CLOSING,
    FTP_CNX_CLOSED

} FTP_CNX_STATUS;

typedef enum _FTP_LAST_ERROR {
    FTP_NO_ERROR = 0,
    FTP_COMMAND_SOCKET_NOT_OPENED,

    FTP_GREETING_FAIL,
    LOGIN_FAIL,
            
    FTP_DATA_CNX_NOT_OPENED = 425,
    FTP_CNX_ABORTED = 426, //timeout
    FTP_INVALID_USER_PASS = 430,
    FTP_HOST_UNAVALABLE = 434,
    FTP_FILE_UNAVILABLE = 450, //or file busy

    FTP_INSUFICIENT_STORAGE = 452,
    FTP_USER_NOT_LOGGED_IN = 530,
    FTP_FILE_NOT_FOUND = 550 //or no access

} FTP_LAST_ERROR;

typedef enum _FTP_FILE_MODE {
    APPEND = 0,
    OVERWRITE
} FTP_FILE_MODE;

typedef struct _FTP_PARAM {
    //FTP_CNX_STATUS ftpSockCnxStatus;
    //FTP_LAST_ERROR ftpLastError;
    FTP_FILE_MODE ftpFileMode;      //0: Append , 1: Overwrite
    BYTE ftpServerName[20];         //IP or DNS of FTP Server
    WORD ftpPort;                   //FTP Server port, default=21
    BYTE ftpUserName[12];           //user name required to log in server
    BYTE ftpUserPass[12];           //user pass required to log in server
    BYTE ftpRemoteFilePath[12];     //default is "//"root dir in server, else mentioned
    BYTE ftpRemoteFileName[13];     //file name in server to do operation on it
    BYTE ftpLocalFileName[13];      //file name to stroed in local SD card

} FTP_PARAM;

//extern BOOL ftpcRunning;

// Prototypes - ****************************************************************************************
BOOL FTPC_SendInit(BYTE dat_size, BYTE dat_name, BYTE ftp_cur_proc);
//BOOL FTPC_SendInit(BYTE numClient);
void FTPClient(void);


extern BOOL FTPC_UploadFileFromSdCard(FTP_PARAM *ftpParam);
extern BOOL FTPC_UploadFromArray(FTP_PARAM *ftpParam, BYTE *ftpTxData, WORD bufLen);
extern BOOL FTPC_DownloadFileToSdCard(FTP_PARAM *ftpParam);
extern BOOL FTPC_DownLoadToArray(FTP_PARAM *ftpParam, BYTE *ftpRxData, WORD bufLen);
extern FTP_LAST_ERROR FTP_GetLastCmdError(void);
extern FTP_CNX_STATUS FTP_GetConnectionStatus(void);