
//******************************************************************************
//            This source file is proprietary property of M/S. eNTesla
//                            All rights reserved.
//******************************************************************************
//******************************************************************************
// Filename         : multitasker.c
// Version          : V1.0
// Processor        : PIC24FJ256GB106
// Compiler         : MPLAB. C30 v3.01 or higher
// IDE              : MPLAB. IDE v8.20 or later
// Programmer(s)    : Neeraj N M
// Notes            :
//******************************************************************************

#include "multitasker.h"

static pETCB pEtcbs[MAX_NUMBER_OF_TASKS];
static unsigned int lastValidTcbCnt;
TICKS multiTaskerTick = 0UL;
TICKS multiTaskerSecondsTick = 0UL;
unsigned int multitaskerStartStopFlag = MULTITASKER_START;

void MultiTaskerInit(void)
{
    //    unsigned int i;

    //    for(i=0; i<MAX_NUMBER_OF_TASKS; i++)
    //    {
    //        pEtcbs[i]=0;
    //    }
    multiTaskerTick = 0;
    lastValidTcbCnt = 0;
}

unsigned int CreateTask(pETCB pEtcb, unsigned int priority, unsigned int taskId,  \
                       unsigned long repTimeMs, unsigned int initState, pfnTask pTaskFunc)
{
    if (lastValidTcbCnt >= MAX_NUMBER_OF_TASKS)
    {
        return 0;
    }
    pEtcbs[lastValidTcbCnt] = pEtcb;
    pEtcbs[lastValidTcbCnt]->priority = priority;
    pEtcbs[lastValidTcbCnt]->pTaskFunc = pTaskFunc;
    pEtcbs[lastValidTcbCnt]->repTimeMs = repTimeMs;
    pEtcbs[lastValidTcbCnt]->taskId = taskId;
    pEtcbs[lastValidTcbCnt]->timeStamp = repTimeMs;
    pEtcbs[lastValidTcbCnt]->taskEn = initState;
    lastValidTcbCnt++;

    return 1;
}

void RunRrMultiTasker(void)
{
    static unsigned int tcbCnt = 0;

    if (MULTITASKER_START == multitaskerStartStopFlag)
    {
        if (tcbCnt >= lastValidTcbCnt)
        {
            tcbCnt = 0;
        }
        else
        {

        }
        if ((1 == pEtcbs[tcbCnt]->taskEn) && (multiTaskerTick >= pEtcbs[tcbCnt]->timeStamp))
        {
            pEtcbs[tcbCnt]->pTaskFunc(0);
            pEtcbs[tcbCnt]->timeStamp = multiTaskerTick + pEtcbs[tcbCnt]->repTimeMs;
        }
        tcbCnt = tcbCnt + 1;
    }
}

unsigned int TaskEnableDisable(unsigned int taskId, unsigned int state)
{
    unsigned int tcbCnt;

    for (tcbCnt = 0; tcbCnt < lastValidTcbCnt; tcbCnt++)
    {
        if (pEtcbs[tcbCnt]->taskId == taskId)
        {
            break;
        }
    }
    if (tcbCnt < lastValidTcbCnt)
    {
        pEtcbs[tcbCnt]->taskEn = state;
        return 1;
    }
    else
    {
        return 0;
    }
}

void EnRrMultiTasker(unsigned int startStopFlag)
{
    multitaskerStartStopFlag = startStopFlag;
}

//unsigned int SendMsgToTask(unsigned int taskId, void *pMsgForTask)
//{
//    unsigned int i;
//    if(0==lastValidTcbCnt)        
//    {
//        return 0;
//    }
//    
//    for(i=0; i<lastValidTcbCnt; i++)
//    {
//        if(pEtcbs[i]->taskId == taskId)
//        {
//            break;
//        }    
//    }
//    if(i==lastValidTcbCnt)
//    {
//        //
//        //  task ID not found!
//        //
//        return 0;
//    }    
//    pEtcbs[i]->msgFlag = 1;
//    pEtcbs[i]->pTaskMsg = pMsgForTask;    
//}

void MultiTaskerGetTick(TICKS *pTick)
{
    *pTick = MULTITASKER_GET_TICK();
}

TICKS MultiTaskerCompareTick(TICKS *pTick)
{
    TICKS localVal;

    if (*pTick > MULTITASKER_GET_TICK())
    {
        //ToDo: return Error
    }
    localVal = (MULTITASKER_GET_TICK() - *pTick);

    return (localVal);
}