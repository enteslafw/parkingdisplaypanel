

#ifndef _ENTESLA_MIFARE_RW_H
#define _ENTESLA_MIFARE_RW_H
    
    #define EMRW_BUFFER_SIZE            64
    #define EMRW_INTERRUPT_READ_TIMEOUT 100
    
    
    typedef enum
    {
        EMRW_CMD_NONE,
        EMRW_CMD_TR,    // nt acctually implemented on MIFARE reader hardware.
        EMRW_CMD_RS,
        EMRW_CMD_CR,
        EMRW_CMD_SL,
        EMRW_CMD_AU,
        EMRW_CMD_LO,
        EMRW_CMD_RBK,
        EMRW_CMD_RBE,
        EMRW_CMD_RBA,
        EMRW_CMD_WBK,
        EMRW_CMD_WBE,
        EMRW_CMD_WBA,
        EMRW_CMD_BD,
        EMRW_CMD_PD,
        EMRW_CMD_SR,
        EMRW_CMD_LS    
    } EMRW_COMMAND;
    
    typedef enum
    {
        KEY_A,
        KEY_B
    } KEY_TYPE;
    
    typedef struct
    {
        EMRW_COMMAND cmd;
        unsigned int cardType;
        unsigned int blockNum;
        unsigned int eepromLocation;
        unsigned long baud;
        unsigned int buzzer;
        unsigned int gpio0;
        unsigned int gpio1;
        unsigned int powerDwn;
        KEY_TYPE keyType;
        char *pKey;
        char *pData;
        char *pUid;
    } EMRW_CMD_PARAM;

    extern char keyA[13];
    extern EMRW_COMMAND emrwLastExecCmd;
    
    extern unsigned char EmrwOpen(unsigned long, unsigned long);
    extern void EmrwClose(void);
    extern unsigned int EmrwExecCmd(EMRW_CMD_PARAM *cmdParam);
    extern void EmrwSetTimeout(unsigned long timeout);
    extern unsigned long EmrwGetTimeout(void);
    extern void EmrwEnableInterruptReception(unsigned int expectedCnt);
    extern void EmrwDisableInterruptReception(unsigned int expectedCnt);
    extern unsigned char EmrwSetSrCmd(unsigned char eepromLoc, unsigned char blockNum);
    extern unsigned char EmrwSendSrCmd(void);
    extern unsigned char EmrwEnableBackgrndRead(unsigned int expectedCnt);
    extern unsigned int EmrwGetTemplateData(unsigned char startBlockNum, unsigned char *pTemplate, unsigned char eepromLoc);
    extern unsigned int EmrwSetTemplateData(unsigned char startBlockNum, unsigned char *pTemplate, unsigned char eepromLoc);
    extern unsigned int SizeOfEmrwCmdResponse(void);
    extern unsigned int EmrwGetResponseS(char *str);
    extern void EmrwPutS(char *str);
#endif  //  _ENTESLA_MIFARE_RW_H
