/* 
 * File:   global_vars.h
 * Author: neer
 *
 * Created on January 2, 2015, 3:14 PM
 */

#ifndef GLOBAL_VARS_H
#define	GLOBAL_VARS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "GenericTypeDefs.h"
#include "rtc.h"
#include "multitasker.h"
#include "IO.h"

    //#define FIRMWARE_FILENAME               "up4.txt" //"mifare.hex"
    //#define MACLIST_NAME                    "MacList.txt"
#define FIRMWARE_FILENAME               "endisp.img"  //this name is common with bootloader program
#define PRE_FIRMWARE_FILENAME           "endisp1.img"
#define DEFAULT_FIRMWARE                "Dendisp.img"
#define DUMMY_FIRMWARE_FILENAME         "default.frm"
#define NETPARAM_FILE                   "NetParam.txt"
#define SYSPARAM_FILE                   "sysparam.txt"
#define LOGDATA_FILE                    "log.txt"

#define UPDATE_KEY_FRM_SRV_KEY_SET 20
#define GET_KEY_FRM_SRV_MAX_TRIES 10
#define MAX_LED_DISPLAY             8

    typedef unsigned char LOG_UID[17], LOG_DATE[7], LOG_TIME[7], LOG_BLOCK1[17];

    typedef union {
        unsigned char array[sizeof (LOG_UID) + sizeof (LOG_DATE) + sizeof (LOG_TIME) + sizeof (LOG_BLOCK1)];

        struct {
            LOG_UID logUid;
            LOG_DATE logDate;
            LOG_TIME logTime;
            LOG_BLOCK1 logBlock1;
        };
    } TRANSACTION;

    typedef struct {
        unsigned char isEnable;
        unsigned char modbusId; 
        unsigned char brightness;
        unsigned char devId[5];
        unsigned char lastValue[5];
    } LED_DISPLAY_PARAM;

    typedef union {
        unsigned char array[664];

        struct {
            char srvIp[46];
            //DWORD_VAL srvIp;
            char srvPort[6];
            char loginId[20];
            char loginPass[20];
            //            char rid[20];
            //            unsigned int timeSyncMin;
            //            char keyStr[20];
            //            char cloudIpUrl[40];
            //            char cloudPort[32];
            char udpLocalPort[6];
            char udpRemotePort[6];

            LED_DISPLAY_PARAM ledDisplayParam[MAX_LED_DISPLAY];
        };

    } SYSTEM_PARAM;

    typedef struct {
        unsigned clientCnxStatus : 1;
        unsigned cardFound : 1;
        unsigned updateSysParamFile : 1;
        unsigned pcCommCmdRxd : 1;
        unsigned setRtcTime : 1;
        unsigned setRtcCal : 1;
        unsigned setRtcDay : 1;
        unsigned setDefLFile : 1;
        unsigned setDefPFile : 1;
        unsigned setDefNFile : 1;
        unsigned setFileRead : 1;
        unsigned getRtcTime : 1;
        unsigned getRtcCal : 1;
        unsigned getRtcDay : 1;
        unsigned IsMasterUser : 1;
        unsigned getKeyFrmSrv : 1;
        unsigned SearchFirmware : 1;
        unsigned in1StatChange : 1;
        unsigned in2StatChange : 1;
        unsigned in3StatChange : 1;
        unsigned in4StatChange : 1;

        unsigned updateLedDisplay[MAX_LED_DISPLAY];
        unsigned isConnected[MAX_LED_DISPLAY];
    } SYSTEM_FLAG;

    extern const char FirmwareVersionBuildDate[];
    extern const char firmwareVersion[];
    extern const char firmwareBuild[];
    extern unsigned int updateKeyFrmSrvStatus;
    extern SYSTEM_PARAM systemParam;
    extern SYSTEM_FLAG systemFlag;
    extern TIME rtcTime, setRtcTime;
    extern CALENDAR rtcCal, setRtcCal;
    extern unsigned int setRtcDay;
    extern unsigned long timeInSec;
    extern TICKS mainTimeStampMs;
    extern unsigned int totalLogsInMem;
    extern DI_STATUS curInStat, PrevInStat;

#ifdef	__cplusplus
}
#endif

#endif	/* GLOBAL_VARS_H */

