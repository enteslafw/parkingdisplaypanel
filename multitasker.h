
//******************************************************************************
//            This source file is proprietary property of M/S. eNTesla
//                            All rights reserved.
//******************************************************************************
//******************************************************************************
// Filename         : multitasker.h
// Version          : V1.0
// Processor        : PIC24FJ256GB106
// Compiler         : MPLAB. C30 v3.01 or higher
// IDE              : MPLAB. IDE v8.20 or later
// Programmer(s)    : Neeraj N M
// Notes            :
//******************************************************************************

#ifndef __MULTITASKER__
#define __MULTITASKER__

#define MAX_NUMBER_OF_TASKS 5

#define TASK_ID_1    1
#define TASK_ID_2    2
#define TASK_ID_3    3
#define TASK_ID_4    4
#define TASK_ID_5    5
#define TASK_ID_6    6
#define TASK_ID_7    7
#define TASK_ID_8    8
#define TASK_ID_9    9
#define TASK_ID_10   10
#define TASK_ID_11   11
#define TASK_ID_12   12
#define TASK_ID_13   13
#define TASK_ID_14   14
#define TASK_ID_15   15
#define TASK_ID_16   16
#define TASK_ID_17   17
#define TASK_ID_18   18
#define TASK_ID_19   19
#define TASK_ID_20   20

#define TASK_PRIORITY_1    1
#define TASK_PRIORITY_2    2
#define TASK_PRIORITY_3    3
#define TASK_PRIORITY_4    4
#define TASK_PRIORITY_5    5
#define TASK_PRIORITY_6    6
#define TASK_PRIORITY_7    7
#define TASK_PRIORITY_8    8
#define TASK_PRIORITY_9    9
#define TASK_PRIORITY_10   10
#define TASK_PRIORITY_11   11
#define TASK_PRIORITY_12   12
#define TASK_PRIORITY_13   13
#define TASK_PRIORITY_14   14
#define TASK_PRIORITY_15   15
#define TASK_PRIORITY_16   16
#define TASK_PRIORITY_17   17
#define TASK_PRIORITY_18   18
#define TASK_PRIORITY_19   19
#define TASK_PRIORITY_20   20

#define TASK_ENABLE  1
#define TASK_DISABLE 0

#define MULTITASKER_START   1
#define MULTITASKER_STOP    0

#define MULTITASKER_TICK_INC()  multiTaskerTick++
#define MULTITASKER_GET_TICK()  multiTaskerTick
#define MULTITASKER_SECONDS_TICK_INC() multiTaskerSecondsTick++
#define MULTITASKER_GET_SECONDS_TICK()  multiTaskerSecondsTick
typedef void(*pfnTask)(void *argStr);
typedef unsigned long long TICKS;

typedef struct {
    unsigned int priority;
    unsigned int taskId;
    unsigned int taskEn;
    unsigned int msgFlag;
    void *pTaskMsg;
    unsigned long repTimeMs;
    TICKS timeStamp;
    pfnTask pTaskFunc;
} ETCB; // Task Control Block

typedef ETCB* pETCB;
//
// extern variables
//
extern TICKS multiTaskerTick;
extern TICKS multiTaskerSecondsTick;

//
// extern Functions
//
extern void MultiTaskerInit(void);

extern unsigned int CreateTask(pETCB pEtcb, unsigned int priority, unsigned int taskId, \
                        unsigned long repTimeMs, unsigned int initState, pfnTask pTaskFunc);

extern unsigned int SendMsgToTask(unsigned int taskId, void *pMsgForTask);

extern unsigned int TaskEnableDisable(unsigned int taskId, unsigned int state);

extern void RunRrMultiTasker(void);

extern void EnRrMultiTasker(unsigned int startStopFlag);

extern void MultiTaskerGetTick(TICKS *pTick);

extern TICKS MultiTaskerCompareTick(TICKS *pTick);

#endif	// 	__MULTITASKER__
