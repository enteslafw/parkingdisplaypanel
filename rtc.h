/* 
 * File:   rtc.h
 * Author: neer
 *
 * Created on January 8, 2015, 7:39 PM
 */

#ifndef RTC_H
#define	RTC_H

#ifdef	__cplusplus
extern "C" {
#endif

#define Fosc	(12000000) 	// crystal
#define Fcy		(Fosc*4/2)	// w.PLL (Instruction Per Second)
#define Fsck	20000		// 50kHz I2C
#define I2C_BRG	((Fcy/2/Fsck)-1)


    //*****USER SETTINGS***************

    //***End of USER SETTINGS**********

    //*******Public Macros************
#define RTC_Wr 0xD0
#define RTC_Rd 0xD1

#define SUNDAY      1
#define MONDAY      2
#define TUESDAY     3
#define WEDNESDAY   4
#define THURSDAY    5
#define FRIDAY      6
#define SATDAY      7

    //*******End of Public Macros*****

    typedef union {
        unsigned long val;

        struct {
            unsigned char date;
            unsigned char month;
            unsigned char year;
            unsigned char dummy;
        };
    } CALENDAR;

    typedef union {
        unsigned long val;

        struct {
            unsigned char hour;
            unsigned char min;
            unsigned char sec;
            unsigned char dummy;
        };
    } TIME;

    //*******Public Protoype **********

    unsigned char _Bcd2Dec(unsigned char bcdNum);
    unsigned char _Dec2Bcd(unsigned char decNum);

    void RTCInit(void);
    void RTCByteWrite(unsigned char address, unsigned char data);
    unsigned char RTCByteRead(unsigned char address);
    // void RTCSetTime(unsigned char sec, unsigned char min, unsigned char hr);
    void RTCSetTime(TIME *ptTime);
    // void RTCSetCalendar(unsigned char date, unsigned char month, unsigned char year);
    void RTCSetCalendar(CALENDAR *ptCalendar);
    void RTCSetSec(unsigned char sec);
    void RTCSetMin(unsigned char min);
    void RTCSetHr(unsigned char hr);
    void RTCSetDay(unsigned char day);
    void RTCSetDate(unsigned char date);
    void RTCSetMonth(unsigned char month);
    void RTCSetYear(unsigned char year);
    // void RTCGetCalendar(unsigned char* pDate, unsigned char* pMonth, unsigned char* pYear);
    void RTCGetCalendar(CALENDAR *ptCalender);
    // void RTCGetTime(unsigned char* pSec, unsigned char* pMin, unsigned char* pHr) ;
    void RTCGetTime(TIME *ptTime);
    unsigned char RTCGetSec(void);
    unsigned char RTCGetMin(void);
    unsigned char RTCGetHr(void);
    unsigned char RTCGetDay(void);
    unsigned char RTCGetDate(void);
    unsigned char RTCGetMonth(void);
    unsigned char RTCGetYear(void);
    void RTCClkHalt(void);
    void RTCClkStart(void);

    void RTCGetTimeInStr(char *timeStr);
    void RTCGetCalendarInStr(char *calendarStr);
    void RTCGetTimeInStrWithSeperator(char *timeStr, char seperator);
    void RTCGetCalendarInStrWithSeperator(char *calendarStr, char seperator);

    //*****End of Public Protoype ******


#ifdef	__cplusplus
}
#endif

#endif	/* RTC_H */

