# README #

README before cloning

### What is this repository for? ###

* This is Parking Display Panel which controls/gives messages to P10_DisplayDriver to display digits on LED Matrix
* Version 1.0


### How do I get set up? ###

* Clone the repository.
* unzip nbproject.zip
* include and place microchip stack folder outside the project folder such that path will be : 
* Project-> Firmware -> ClonedFolder 
* Project-> Microchip
* if program not compiled then manually add Microchip stack file path in project include list
* This firmware uses custom linker file for encrypted bootloader. remove it from project and program the device.

### Contribution guidelines ###

* create new branch for new changes made. do not push in to same master branch.
* suggestions are welcome.

### Who do I talk to? ###

* Umesh W: umeshw@entesla.com