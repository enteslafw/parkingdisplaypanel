
/******************************************************************************/
/*  Files to Include                                                          */
/******************************************************************************/
#if defined(__XC16__)
#include <xc.h>
#elif defined(__C30__)
#include <xc.h>          /* Defines special funciton registers, CP0 regs  */
#endif

//#include <plib.h>           /* Include to use PIC32 peripheral libraries      */
#include <stdint.h>         /* For uint32_t definition                        */
#include <stdbool.h>        /* For true/false definition                      */

#include "system.h"         /* System funct/params, like osc/periph config    */
#include "glcd.h"
#include "delay.h"
//****End of INCLUDES***********************************************************

extern const unsigned char glcdFont[96][7];
unsigned char xPosi = 0, yPosi = 0;
unsigned char _color = 1;

//***********************Private Macro Definitions*****************************
#define _GlcdDelay50Us()    {uSDelay=SYS_FREQ_MHz*5;while(--uSDelay != 0);}
#define _GlcdDelay10Us()    {uSDelay=SYS_FREQ_MHz*4;while(--uSDelay != 0);}
//*******************End of Private Macro Definitions*************************
#define _GLCD_CS2_HIGH()        _LATD4=1;
#define _GLCD_CS2_LOW()         _LATD4=0;
#define _GLCD_CS1_HIGH()        _LATD5=1;
#define _GLCD_CS1_LOW()         _LATD5=0;
#define _GLCD_RS_HIGH()         _LATD7=1;
#define _GLCD_RS_LOW()          _LATD7=0;
#define _GLCD_E_HIGH()          _LATF1=1;
#define _GLCD_E_LOW()           _LATF1=0;
#define _GLCD_IS_CS2_HIGH()     _RD4
#define _GLCD_IS_CS1_HIGH()     _RD5

#define _GLCD_RW_HIGH()         _LATF0=1;
#define _GLCD_RW_LOW()          _LATF0=0;
#define _GLCD_RST_HIGH()        _LATD6=1;
#define _GLCD_RST_LOW()         _LATD6=0;

#define _GLCD_CS2_DIR_OUT()       _TRISD4=0;
#define _GLCD_CS1_DIR_OUT()       _TRISD5=0;
#define _GLCD_RS_DIR_OUT()        _TRISD7=0;
#define _GLCD_E_DIR_OUT()         _TRISF1=0;


#define _GLCD_RW_DIR_OUT()        _TRISF0=0;
#define _GLCD_RST_DIR_OUT()       _TRISD6=0;

#define _GLCD_DATA_PORT_OUTPUT()    GLCD_DATA_DIR = (GLCD_DATA_DIR & 0xFF00)
#define _GLCD_DATA_PORT_INPUT()     GLCD_DATA_DIR = (GLCD_DATA_DIR | 0xFF)                

//********************End of Private Macro Definitions*************************

//*****Variable Declarations***************************************************
volatile unsigned int uSDelay;
//*****End of Variable Declarations********************************************

//********Private Protoype ****************************************************
void _GlcdDelayMs(unsigned int delay);
void _GlcdInstWrite(unsigned char inst);
void _GlcdCheckBusy(void);
void _GlcdDataWrite(unsigned char data);
unsigned char _GlcdDataRead(void);
unsigned char _GlcdStatusRead(void);
//******End of Private Protoype ***********************************************

//******************************************************************************
// Function:        void GlcdInit(void)
//
// Input:           None
//
// Output:          None
//
// Overview:        _GLCD initialization.
//
// Note:            None
//****************************************************************************

void GlcdInit(void)
{
    GLCD_DATA_PORT = (GLCD_DATA_PORT & 0xFF00) | 0x00;
    _GLCD_DATA_PORT_OUTPUT(); //Data port as output
    _GLCD_CS1_DIR_OUT();
    _GLCD_CS2_DIR_OUT();
    _GLCD_RS_DIR_OUT();
    _GLCD_RW_DIR_OUT();
    _GLCD_E_DIR_OUT();
    _GLCD_RST_DIR_OUT();
    //GLCD_CTRL_DIR_REG1 &=  ((1<<GLCD_CS1_PIN)|(1<<GLCD_CS2_PIN)|(1<<GLCD_E_PIN)|(1<<GLCD_RS_PIN ));
    //GLCD_CTRL_DIR_REG2 &= (~((1<<GLCD_RW_PIN)|(1<<GLCD_RST_PIN)));

    _GLCD_RST_HIGH(); //GLCD Active
    _GLCD_CS1_LOW(); //Disable both chips
    _GLCD_CS2_LOW(); //Disable both chips
    _GLCD_RS_LOW(); //clear RS
    _GLCD_RW_LOW(); //clear RW
    _GLCD_E_LOW(); //clear E

    _GLCD_CS1_HIGH();
    _GlcdInstWrite(0x3f);
    _GLCD_CS1_LOW(); //display on

    _GLCD_CS2_HIGH();
    _GlcdInstWrite(0x3f); //display on
    _GLCD_CS2_LOW();                                 //clear E

    _GLCD_CS1_HIGH();
    _GlcdInstWrite(0x3f);
    _GLCD_CS1_LOW();						        //display on

	_GLCD_CS2_HIGH();
	_GlcdInstWrite(0x3f);						    //display on
    _GLCD_CS2_LOW();

	_GLCD_CS1_HIGH();
    _GlcdInstWrite(0x57);
    _GLCD_CS1_LOW();						        //display on

	_GLCD_CS1_HIGH();
	_GlcdInstWrite(0x92);						    //display on
    _GLCD_CS2_LOW();

}

//******************************************************************************
// Function:        void _GlcdInstWrite(unsigned char data)
//
// Input:           unsigned char data
//
// Output:          None
//
// Overview:        Write a Command to the _GLCD
//
// Note:            None
//****************************************************************************

void _GlcdInstWrite(unsigned char inst)
{
    //_GlcdCheckBusy();
    while (0x80 == (0x80 & _GlcdStatusRead()));
    _GLCD_RS_LOW();
    _GLCD_RW_LOW();
    _GLCD_DATA_PORT_OUTPUT(); // Port as output
    GLCD_DATA_PORT = (GLCD_DATA_PORT & 0xFF00) | (inst);
    _GLCD_E_HIGH();
    //_GlcdDelay50Us();
    _GLCD_E_LOW();
    while (0x80 == (0x80 & _GlcdStatusRead()));
    //_GlcdDelay50Us();

}

//******************************************************************************
// Function:        void _GlcdDataWrite(unsigned char data)
//
// Input:           unsigned char data
//
// Output:          None
//
// Overview:        Write Data to the GLCD
//
// Note:            None
//****************************************************************************

void _GlcdDataWrite(unsigned char data)
{
    while (0x80 == _GlcdStatusRead());
    _GLCD_RS_HIGH();
    _GLCD_RW_LOW();
    _GLCD_DATA_PORT_OUTPUT(); // Port as output
    GLCD_DATA_PORT = (GLCD_DATA_PORT & 0xFF00) | (data);
    _GLCD_E_HIGH();
    _GlcdDelay50Us();
    _GLCD_E_LOW();

}

//******************************************************************************
// Function:        void _GlcdCheckBusy(void)
//
// Input:           None
//
// Output:          None
//
// Overview:        Wait if _GLCD is Busy
//
// Note:            None
//****************************************************************************

void _GlcdCheckBusy(void)
{
    GLCD_DATA_PORT = (GLCD_DATA_PORT & 0xFF00) | 0x00;
    _GLCD_DATA_PORT_INPUT(); //DATA Lines as Input
    //_GLCD_RS_LOW();
    //_GLCD_RW_HIGH();
    if (_GLCD_IS_CS1_HIGH() == 1 && _GLCD_IS_CS2_HIGH() == 1)
    {
        _GLCD_CS2_LOW();
        while (_GlcdStatusRead() & 0x80);
        _GLCD_CS1_LOW();
        _GLCD_CS2_HIGH();
        while (_GlcdStatusRead() & 0x80);
        _GLCD_CS2_LOW();
    }
    else
    {
        while (_GlcdStatusRead() & 0x80);
    }
    _GlcdDelay50Us();
    _GLCD_DATA_PORT_OUTPUT(); //DATA lines again as Output
}

//******************************************************************************
// Function:        unsigned char _GlcdDataRead(void)
//
// Input:           None
//
// Output:          unsigned char glcdData
//
// Overview:        _GLCD Read Data
//
// Note:            None
//*****************************************************************************

unsigned char _GlcdDataRead(void)
{
    unsigned char glcdData;
    GLCD_DATA_PORT = (GLCD_DATA_PORT & 0xFF00) | 0x00;
    _GLCD_DATA_PORT_INPUT(); //Set port as input

    _GLCD_RS_HIGH();
    _GLCD_RW_HIGH();
    _GLCD_E_LOW();
    _GlcdDelay10Us();
    _GLCD_E_HIGH();
    _GlcdDelay50Us();
    glcdData = GLCD_DATA_IN; //Read data when En is High
    _GLCD_E_LOW();
    _GlcdDelay50Us();
    _GLCD_DATA_PORT_OUTPUT();

    GLCD_DATA_PORT = (GLCD_DATA_PORT & 0xFF00) | 0x00;
    _GLCD_DATA_PORT_INPUT(); //Set port as input
    _GLCD_E_HIGH();
    _GlcdDelay50Us();
    glcdData = GLCD_DATA_IN; //Read data when En is High
    _GLCD_E_LOW();
    _GlcdDelay50Us();
    _GLCD_DATA_PORT_OUTPUT();
    return (glcdData);
}

//******************************************************************************
// Function:        unsigned char _GlcdStatusRead(void)
//
// Input:           None
//
// Output:          unsigned char glcdData
//
// Overview:        Returns the GLCD status
//
// Note:            None
//*****************************************************************************

unsigned char _GlcdStatusRead(void)
{
    unsigned char glcdStatus;

    GLCD_DATA_PORT = (GLCD_DATA_PORT & 0xFF00) | 0x00;
    _GLCD_DATA_PORT_INPUT(); //Set port as input
    _GLCD_RS_LOW();
    _GLCD_RW_HIGH();
    _GLCD_E_LOW();
    _GlcdDelay10Us();
    _GLCD_E_HIGH();
    _GlcdDelay50Us();
    glcdStatus = GLCD_DATA_IN; //Read data when En is High
    _GLCD_E_LOW();
    _GlcdDelay50Us();
    return (glcdStatus);
}

//******************************************************************************
// Function:        void GlcdSetCursor(unsigned char xPos, unsigned yPos)
//
// Input:           xPos, yPos
//
// Output:          None
//
// Overview:        Sets cursor position as specified in xPos and yPos
//
// Note:            None
//****************************************************************************

void GlcdSetCursor(unsigned char xPos, unsigned char yPos)
{
    xPosi = xPos;
    yPosi = yPos;
    if (xPos > 63)
    {
        _GLCD_CS1_HIGH();
        _GLCD_CS2_LOW();
    }
    else
    {
        _GLCD_CS1_LOW();
        _GLCD_CS2_HIGH();
    }
    _GlcdInstWrite(0x40 + (xPos & 0x3f)); //Column
    _GlcdInstWrite(0xb8 + ((yPos & 0x3f) >> 3)); //row

}
//******************************************************************************
// Function:        void GlcdIncCursor(unsigned char mode)
//
// Input:           Mode input. 1 = Text, 0 = Picture
//
// Output:          None
//
// Overview:        Increments cursor
//
// Note:            None
//****************************************************************************

void GlcdIncCursor(unsigned char mode)
{
    if (++xPosi == 64)
    {
        GlcdSetCursor(xPosi, yPosi);
    }
    if (mode == 0)
    {
        if ((xPosi == 128) || (128 - xPosi < 4))
        {
            xPosi = 0;
            yPosi += 8;
            yPosi = yPosi & 0x3f;
            GlcdSetCursor(xPosi, yPosi);
        }
    }
    else
    {
        if ((xPosi == 128))
        {
            xPosi = 0;
            yPosi += 8;
            yPosi = yPosi & 0x3f;
            GlcdSetCursor(xPosi, yPosi);
        }
    }
}

//******************************************************************************
// Function:        void GlcdPixelColor(unsigned char xPos, unsigned char yPos, unsigned char color)
//
// Input:           xPos, yPos, color
//
// Output:          None
//
// Overview:        Sets cursor position as specified in xPos and yPos
//
// Note:            None
//****************************************************************************

void GlcdPixelColor(unsigned char xPos, unsigned char yPos, unsigned char color)
{
    unsigned char data;
    //_GlcdCheckBusy();
    _GlcdDelay50Us();
    if (xPos > 63)
    {
        _GLCD_CS1_HIGH();
        _GLCD_CS2_LOW();
        xPos -= 64;
    }
    else
    {
        _GLCD_CS1_LOW();
        _GLCD_CS2_HIGH();
    }
    //xPos /= 8;
    _GlcdInstWrite(0x40 + (xPos & 0x3f)); //write column address
    _GlcdInstWrite(0xb8 + (yPos >> 3)); //write row address
    data = _GlcdDataRead(); //dummy read    data = _GlcdDataRead();
    _GlcdInstWrite(0x40 + xPos); //write column address again
    if (color == 1)
    {
        data = data | (0x00 + (1 << (yPos & 7)));
    }
    else if (color == 0)
    {
        data = data & (0xFF - (1 << (yPos & 7)));
    }
    _GlcdDataWrite(data);
}
//******************************************************************************
// Function:        void GlcdHoriLine(unsigned char y, unsigned char x1, unsigned char x2)
//
// Input:           y,x1,x2
//
// Output:          None
//
// Overview:        Draws a horizontal line
//
// Note:            None
//****************************************************************************

void GlcdHoriLine(unsigned char y, unsigned char x1, unsigned char x2)
{
    unsigned char i;
    for (i = x1; i <= x2; i++)
    {
        //GlcdSetPixel(i, y);
        GlcdPixelColor(i, y, _color);
    }
}
//******************************************************************************
// Function:        void GlcdVertLine(unsigned char x, unsigned char y1, unsigned char y2)
//
// Input:           x,y1,y2
//
// Output:          None
//
// Overview:        Draws a vertical line
//
// Note:            None
//****************************************************************************

void GlcdVertLine(unsigned char x, unsigned char y1, unsigned char y2)
{
    unsigned char i;
    for (i = y1; i <= y2; i++)
    {
        //GlcdSetPixel(x, i);
        GlcdPixelColor(x, i, _color);
    }
}
//******************************************************************************
// Function:        void GlcdDrawBox(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2)
//
// Input:           x1,y1,x2,y2
//
// Output:          None
//
// Overview:        Draws a Box
//
// Note:            None
//****************************************************************************

void GlcdDrawBox(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2)
{
    GlcdVertLine(x1, y1, y2);
    GlcdVertLine(x2, y1, y2);
    GlcdHoriLine(y1, x1, x2);
    GlcdHoriLine(y2, x1, x2);
}
//******************************************************************************
// Function:        void GlcdDrawLine(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2)
//
// Input:           x1,y1,x2,y2
//
// Output:          None
//
// Overview:        Draws a line
//
// Note:            None
//****************************************************************************

void GlcdDrawLine(char x1, char y1, char x2, char y2)
{
    int c1, m1;
    signed int d1 = 0;
    signed int pixelX = x2 - x1; //number of X-pixels;
    signed int pixelY = y2 - y1; //Number of Y-pixels
    signed char xInc = 1;
    signed char yInc = 1;

    if (pixelX < 0) //If negative X direction
    {
        xInc = -1;
        pixelX = pixelX * -1;
    }
    if (pixelY < 0) //If negative Y direction
    {
        yInc = -1;
        pixelY = pixelY * -1;
    }
    if (pixelY <= pixelX)
    {
        c1 = 2 * pixelX;
        m1 = 2 * pixelY;
        while (x1 != x2)
        {
            //GlcdSetPixel(x1, y1);    //ForePlot line
            GlcdPixelColor(x1, y1, _color);
            x1 = x1 + xInc;
            d1 = d1 + m1;
            if (d1 > pixelX)
            {
                y1 = y1 + yInc;
                d1 = d1 - c1;
            }
        }
    }
    else
    {
        c1 = 2 * pixelY;
        m1 = 2 * pixelX;
        while (y1 != y2)
        {
            //GlcdSetPixel(x1, y1);    //ForePlot Line
            GlcdPixelColor(x1, y1, _color);
            y1 = y1 + yInc;
            d1 = d1 + m1;
            if (d1 > pixelY)
            {
                x1 = x1 + xInc;
                d1 = d1 - c1;
            }
        }
        //GlcdSetPixel(x1, y1);
        GlcdPixelColor(x1, y1, _color);
    }
    GlcdSetCursor(0, 0);
}

//******************************************************************************
// Function:        void GlcdDrawCircle(unsigned int cx, unsigned int cy, unsigned int radius)
//
// Input:           Center==> cx, cy and raduis length: radius
//
// Output:          None
//
// Overview:        Draws a circle of radius 'radius' at cx, cy
//
// Note:            None
//****************************************************************************

void GlcdDrawCircle(unsigned int cx, unsigned int cy, unsigned int radius)
{
    int x, y, xchange, ychange, radiusError;
    x = radius;
    y = 0;
    xchange = 1 - 2 * radius;
    ychange = 1;
    radiusError = 0;
    while (x >= y)
    {
        GlcdPixelColor(cx + x, cy + y, _color);
        GlcdPixelColor(cx - x, cy + y, _color);
        GlcdPixelColor(cx - x, cy - y, _color);
        GlcdPixelColor(cx + x, cy - y, _color);
        GlcdPixelColor(cx + y, cy + x, _color);
        GlcdPixelColor(cx - y, cy + x, _color);
        GlcdPixelColor(cx - y, cy - x, _color);
        GlcdPixelColor(cx + y, cy - x, _color);
        y++;
        radiusError += ychange;
        ychange += 2;
        if (2 * radiusError + xchange > 0)
        {
            x--;
            radiusError += xchange;
            xchange += 2;
        }
    }
}
//******************************************************************************
// Function:        void GlcdPutCh(unsigned char data)
//
// Input:           data
//
// Output:          None
//
// Overview:        Puts a character on the Glcd. 
//
// Note:            Set the cursor position before using this function.
//****************************************************************************

void GlcdPutCh(unsigned char data)
{
    unsigned char i;
    char glcdData;
    if (data < 32)
    {
        switch (data)
        {
        case 13:
            xPosi = 0;
        case 10:
            xPosi = 0;
            yPosi += 8;
            yPosi = yPosi & 63;
        }
        GlcdSetCursor(xPosi, yPosi);
    }
    else
    {
        for (i = 0; i < 7; i++)
        {
            glcdData = ~glcdFont[data - 32][i];
            if (glcdData != ~0x55)
            {
                if (_color == 0)
                {
                    glcdData = ~glcdData;
                }
                _GlcdDataWrite(glcdData);
                GlcdIncCursor(0);
            }
        }
        if (_color == 0)
        {
            _GlcdDataWrite(0xFF); //Leave a space after printing charcter
        }
        else
        {
            _GlcdDataWrite(0x00); //Leave a space after printing charcter
        }

        GlcdIncCursor(0);
    }
}

//******************************************************************************
// Function:        void GlcdPutS(char *text)
//
// Input:           *text
//
// Output:          None
//
// Overview:        Puts a string of characters on the Glcd stored in the array.  
//
// Note:            Set the cursor position before using this function.
//****************************************************************************

void GlcdPutS(char *text)
{
    while (*text)
    {
        GlcdPutCh(*text++);
    }

}

void GlcdPutSWithCursor(char *text)
{
    while (*text)
    {
        GlcdPutCh(*text++);
    }
    GlcdSetColor(GLCD_WHITE);
    GlcdPutCh(' ');
    GlcdSetColor(GLCD_BLACK);

}

void GlcdPutSHighlight(char *text, unsigned char start, unsigned char end)
{
    unsigned char localCount = 0;
    

    while (*text)
    {
        if((localCount >= start) && (localCount < end))
        {
            GlcdSetColor(GLCD_WHITE);
        }
        else
        {
            GlcdSetColor(GLCD_BLACK);
        }
        GlcdPutCh(*text++);
        localCount++;
    }
    GlcdSetColor(GLCD_BLACK);

}

//******************************************************************************
// Function:        void GlcdPutImage(static char rom *image)
//
// Input:           *image
//
// Output:          None
//
// Overview:        Displays the image on the glcd.  
//
// Note:            Set the cursor position before using this function.
//*********************************************2*******************************

void GlcdPutImage(unsigned char *image)
{

    unsigned char w, h, bitcount, byte;
    unsigned char i;

    w = 128; //y limit in pixels
    h = 8; //x limit in pages
    bitcount = 0;

    xPosi = 0;
    yPosi = 0;
    GlcdSetCursor(xPosi, yPosi);
    do
    {
        for (i = 0; i < w; i++)
        {
            if (GLCD_WHITE == _color)
            {
                _GlcdDataWrite(~(*image++));
            }
            else
            {
                _GlcdDataWrite(*image++);
            } //Since data is represented page wise,
            //we write an entire page and then increment
            GlcdIncCursor(1); //the cursor in the horizontal direction
        }
    }
    while (--h);
}
//******************************************************************************
// Function:        void GlcdClrScreen(void)
//
// Input:           None
//
// Output:          None
//
// Overview:        Clears screen and sets the cursor position to 0,0
//
// Note:            None
//****************************************************************************

void GlcdClrScreen(void)
{
	unsigned char i,j;

	_GLCD_CS1_HIGH();
	_GLCD_CS2_LOW();

	for(i=0; i<8; i++)
	{
		_GlcdInstWrite(0x40);	    //y=0
		_GlcdInstWrite(0xB8 + i);  	//x=0
		for(j=0; j<0x40; j++)
        {
             _GlcdDataWrite(0x00);  //Initial contents are 0x00
        }
	}

    _GLCD_CS1_LOW();
	_GLCD_CS2_HIGH();

	for(i=0; i<8; i++)
	{
		_GlcdInstWrite(0x40);	    //y=0
		_GlcdInstWrite(0xB8 + i);  	//x=0
		for(j=0; j<0x40; j++)
        {
             _GlcdDataWrite(0x00);  //Initial contents are 0x00
        }
	}

	GlcdSetCursor(0, 0);
}

void GlcdSetColor(unsigned char colorToSet)
{
    if (colorToSet > 0)
    {
        _color = 1;
    }
    else
    {
        _color = 0;
    }
}
//******************************************************************************
// Function:        void _GlcdDelayMs(unsigned int delay)
//
// Input:           Delay in milliseconds
//
// Output:          None
//
// Overview:        Generates a delay of specified number of milliseconds.
//
// Note:            None
//****************************************************************************

void _GlcdDelayMs(unsigned int delay)
{
    volatile unsigned int i;
    while (delay > 0)
    {
        for (i = 0; i < 8000; i++);
        delay--;
    }
}
