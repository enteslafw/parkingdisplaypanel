/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#ifdef __XC32
#include <xc.h>          /* Defines special funciton registers, CP0 regs  */
#endif

#include <plib.h>            /* Include to use PIC32 peripheral libraries     */
#include <stdint.h>          /* For uint32_t definition                       */
#include <stdbool.h>         /* For true/false definition                     */
//#include "relay.h"
//#include "led.h"
#include "HardwareProfile.h"
#include"global_vars.h"
#include "En_Consol.h"

/******************************************************************************/
/* Exception Macro Definitions                                                */
/******************************************************************************/
//https://code.csdn.net/RT-Thread/rt-thread/blob/master/libcpu/mips/pic32/exceptions.c
/*When WRITE_EXCEPTION_CAUSE_TO_FLASH is defined the PIC32 executes a self
write routine to save the exception cause register.*/

// #define WRITE_EXCEPTION_CAUSE_TO_FLASH

#ifdef WRITE_EXCEPTION_CAUSE_TO_FLASH

/* Physical Addresses which are at the end of KSEG 0 program memory. */
/* User may want to adjust these values */
#define EXCEPTION_CAUSE 0x1D007FFC
#define EXCEPTION_ADDR  0x1D007FF8

#endif

/******************************************************************************/
/* Exception Variable Declaration                                             */
/******************************************************************************/

/* static in case exception condition would stop auto variable being created  */
static enum
{
    EXCEP_IRQ = 0, /* interrupt */
    EXCEP_AdEL = 4, /* address error exception (load or ifetch) */
    EXCEP_AdES, /* address error exception (store) */
    EXCEP_IBE, /* bus error (ifetch) */
    EXCEP_DBE, /* bus error (load/store) */
    EXCEP_Sys, /* syscall */
    EXCEP_Bp, /* breakpoint */
    EXCEP_RI, /* reserved instruction */
    EXCEP_CpU, /* coprocessor unusable */
    EXCEP_Overflow, /* arithmetic overflow */
    EXCEP_Trap, /* trap (possible divide by zero) */
    EXCEP_IS1 = 16, /* implementation specfic 1 */
    EXCEP_CEU, /* CorExtend Unuseable */
    EXCEP_C2E /* coprocessor 2 */
} _excep_code;

/* static in case exception condition would stop auto variable being created */
static unsigned int _epc_code;
static unsigned int _excep_addr;

/******************************************************************************/
/* Exception Handling                                                         */
/******************************************************************************/

/* This function overrides the normal _weak_ _generic_exception_handler which
is defined in the C32 User's Guide.  The _weak_ _generic_exception_handler
just does an infinite loop. */
void _general_exception_handler(unsigned cause, unsigned status)
{
#ifdef WRITE_EXCEPTION_CAUSE_TO_FLASH
    unsigned long t0 = _CP0_GET_COUNT(); /* Used for NVMOP 6 us Delay */
#endif

    /* Mask off Mask of the ExcCode Field from the Cause Register
    Refer to the MIPs M4K Software User's manual */
    //_excep_code =_CP0_GET_CAUSE() & 0x0000007C >> 2;
    //_excep_addr =_CP0_GET_EPC();
    asm volatile("mfc0 %0,$13" : "=r" (_excep_code));
    asm volatile("mfc0 %0,$14" : "=r" (_excep_addr));

    _excep_code = (_excep_code & 0x0000007C) >> 2;

#ifdef WRITE_EXCEPTION_CAUSE_TO_FLASH
    _CP0_SET_STATUS(_CP0_GET_STATUS()&0xFFFFFFE); /* Disable Interrupts */
#endif
#ifdef WRITE_EXCEPTION_CAUSE_TO_FLASH

    /* Store the exception causes in program memory in case the part exhibited
    the problem in release mode.  Gives user a place to start debugging
    the problem. */

    NVMCON = 0x4001; /* set WREN and Word Programing mode */
    NVMADDR = EXCEPTION_CAUSE; /* PM Address at which we'll store the */
    /* cause register */
    NVMDATA = _excep_code;

    /* wait at least 6 us for LVD start-up
    assume we're running at max frequency
    (80 MHz) so we're always safe */
    {
        while (_CP0_GET_COUNT() - t0 < (80 / 2)*6);
    }

    NVMKEY = 0xAA996655;
    NVMKEY = 0x556699AA; /* unlock sequence */
    NVMCONSET = NVMCON_WR;
    while (NVMCON & NVMCON_WR); /* wait on write to finish */

    NVMCON = 0x4001; /* set WREN and Word Programing mode */
    NVMADDR = EXCEPTION_ADDR; /* PM Address at which we'll store the */
    /* exception address register */
    NVMDATA = _excep_addr;

    /* wait at least 6 us for LVD start-up
    assume we're running at max frequency
    (80 MHz) so we're always safe */
    {
        while (_CP0_GET_COUNT() - t0 < (80 / 2)*6);
    }

    NVMKEY = 0xAA996655;
    NVMKEY = 0x556699AA; /* unlock sequence */
    NVMCONSET = NVMCON_WR;
    while (NVMCON & NVMCON_WR);

    /* Write the exception cause and address to the part can be read and
    the cause determined. */
    NVMWriteWord((void*) EXCEPTION_CAUSE, _excep_code);
    NVMWriteWord((void*) EXCEPTION_ADDR, _excep_addr);

#endif
    //http://www.microchip.com/forums/m546064.aspx?print=true
    switch (_excep_code)
    {
    case EXCEP_IRQ:PcCommPutS("interrupt\r\n");
        break; //SD_UpdateErrorLog
    case EXCEP_AdEL:PcCommPutS("address error exception (load or ifetch)\r\n");
        break;
    case EXCEP_AdES:PcCommPutS("address error exception (store)\r\n");
        break;
    case EXCEP_IBE:PcCommPutS("bus error (ifetch)\r\n");
        break;
    case EXCEP_DBE:PcCommPutS("bus error (load/store)\r\n");
        break;
    case EXCEP_Sys:PcCommPutS("syscall\r\n");
        break;
    case EXCEP_Bp:PcCommPutS("breakpoint\r\n");
        break;
    case EXCEP_RI:PcCommPutS("reserved instruction\r\n");
        break;
    case EXCEP_CpU:PcCommPutS("coprocessor unusable\r\n");
        break;
    case EXCEP_Overflow:PcCommPutS("arithmetic overflow\r\n");
        break;
    case EXCEP_Trap:PcCommPutS("trap (possible divide by zero)\r\n");
        break;
    case EXCEP_IS1:PcCommPutS("implementation specfic 1\r\n");
        break;
    case EXCEP_CEU:PcCommPutS("CorExtend Unuseable\r\n");
        break;
    case EXCEP_C2E:PcCommPutS("coprocessor 2\r\n");
        break;
    default: PcCommPutS("unkown exception\r\n");
        break;
    }
    /*watchdog enabale and let the core reset it*/
    //    WDTCONbits.SWDTPS = 0x00; //0b00111;
    //    WDTCONbits.ON = 1;
    //http://www.microchip.com/forums/tm.aspx?m=524419&high=%29:
    while (1)
    {
        /* Examine _excep_code to identify the type of exception */
        /* Examine _excep_addr to find the address that caused the exception */
        switch (_excep_code)
        {
        case EXCEP_AdEL: // address error exception (load or ifetch)
        {
            //                     LED2_ON();
            //            LED_1 = 1;
        }
            break;
        case EXCEP_AdES: // address error exception (store)
        {
            //                     LED3_ON();
            //            LED_2 = 1;
        }
            break;
        case EXCEP_IBE: // bus error (ifetch)
        {
            //                    LED4_ON();
            //            LED_3 = 1;
        }
            break;
        case EXCEP_DBE: // bus error (load/store)
        {
            //                    RELAY2_ON();
            //            LED_4 = 1;
        }
            break;
        case EXCEP_Overflow: // arithmetic overflow
        {
            //                    RELAY3_ON();
            //            LED_1 = 1;
            //            LED_2 = 1;
        }
            break;
        case EXCEP_Trap: // trap (like divide by 0)
        {
            //                    RELAY4_ON();
            //            LED_3 = 1;
            //            LED_4 = 1;
        }
            break;
        default:
        {
            //The rest of the exception types are probably less common
            // while(1);
            //                    RELAY3_ON();
            //                    RELAY4_ON();
            //            LED_1 = 1;
            //            LED_2 = 1;
            //            LED_3 = 1;
            //            LED_4 = 1;
        }
        }

    }
}

/********************************************************************
 * Function: 	JumpToApp()
 *
 * Precondition:
 *
 * Input: 		None.
 *
 * Output:
 *
 * Side Effects:	No return from here.
 *
 * Overview: 	Jumps to application.
 *
 *
 * Note:		 	None.
 ********************************************************************/
void JumpToApp(void)
{
    void (*fptr)(void);
    fptr = (void (*)(void))0xBFC00000;
    ;
    fptr();
}
