
/******************************************************************************/
/*  Files to Include                                                          */
/******************************************************************************/
#ifdef __XC32
#include <xc.h>          /* Defines special funciton registers, CP0 regs  */
#endif

#include <plib.h>           /* Include to use PIC32 peripheral libraries      */
#include <stdint.h>         /* For uint32_t definition                        */
#include <stdbool.h>        /* For true/false definition                      */

//#include "system.h"         /* System funct/params, like osc/periph config    */
#include "rtc.h"
//****End of INCLUDES***********************************************************


//*******Private Protoype ******************************************************
unsigned char _Bcd2Dec(unsigned char bcdNum); // assumes valid BCD
unsigned char _Dec2Bcd(unsigned char decNum); // assumes decNum is 0-99
//*****End of Private Protoype *************************************************

void RTCInit(void)
{
    RTCClkStart(); //enable rtc clock
    RTCByteWrite(0x07, 0x10); //enable square wave of 1hz
}

//******************************************************************************
//  Function:       void RTCByteWrite(unsigned char address,unsigned char data)
//
//  Input:          RTC Register Address and Data
//
//  Output:         None
//
//  Overview:       This function will write "data" at the given register "address"
//
//  Note:           None
//******************************************************************************

void RTCByteWrite(unsigned char address, unsigned char data)
{
    StartI2C1();
    while (I2C1CONbits.SEN); //Wait till Start sequence is completed
    MasterWriteI2C1(RTC_Wr); //control byte for write (COMMAND)
    IdleI2C1(); //Wait to complete
    MasterWriteI2C1(address);
    IdleI2C1(); //Wait to complete
    MasterWriteI2C1(data); //data to device         (DATA)
    IdleI2C1(); //Wait to complete
    StopI2C1();
    while (I2C1CONbits.PEN); //Wait till stop sequence is completed
}

//******************************************************************************
//  Function:       unsigned char RTCByteRead(unsigned char address)
//
//  Input:          RTC Register Address
//
//  Output:         Data
//
//  Overview:       This function returns the "data" read at the given register
//                  "address"
//
//  Note:           None
//******************************************************************************

unsigned char RTCByteRead(unsigned char address)
{
    unsigned char byteread;
    StartI2C1();
    IdleI2C1(); //Wait to complete
    //while(I2C1CONbits.SEN );  //Wait till Start sequence is completed
    MasterWriteI2C1(RTC_Wr);
    IdleI2C1(); //Wait to complete
    MasterWriteI2C1(address);
    IdleI2C1(); //Wait to complete
    RestartI2C1(); // Issue I2C1 signal repeated start
    IdleI2C1(); //Wait to complete
    MasterWriteI2C1(RTC_Rd);
    IdleI2C1(); //Wait to complete
    byteread = MasterReadI2C1(); // Read the data (with acknowledge)
    IdleI2C1(); //Wait to complete
    StopI2C1();
    IdleI2C1(); //Wait to complete
    //while(I2C1CONbits.PEN);  //Wait till stop sequence is completed
    return (byteread);
}

//*****************************************************************************
//  Function:       void RTCSetTime(TIME *ptTime)
//
//  Input:          pointer to TIME Structure
//
//  Output:         None
//
//  Overview:       This function will set the specified time in the RTC
//
//  Note:           None
//****************************************************************************

void RTCSetTime(TIME *ptTime)
{
    RTCByteWrite(0x00, _Dec2Bcd(ptTime->sec)); //seconds
    RTCByteWrite(0x01, _Dec2Bcd(ptTime->min)); //minutes
    RTCByteWrite(0x02, _Dec2Bcd(ptTime->hour)); //hour
    ptTime->dummy = 0;
}

//*****************************************************************************
//  Function:       void RTCSetDate(CALENDER *ptCalender)
//
//  Input:          pointer to CALENDER structure
//
//  Output:         None
//
//  Overview:       This function will set the specified date in the RTC
//
//  Note:           None
//****************************************************************************

void RTCSetCalendar(CALENDAR *ptCalendar)
{
    RTCByteWrite(0x04, _Dec2Bcd(ptCalendar->date)); //date
    RTCByteWrite(0x05, _Dec2Bcd(ptCalendar->month)); //month
    RTCByteWrite(0x06, _Dec2Bcd(ptCalendar->year)); //year
    //    RTCByteWrite(0x03,(ptCalendar->day));
    ptCalendar->dummy = 0;
}

//******************************************************************************
//  Function:       void RTCSetSec(unsigned char sec)
//
//  Input:          Seconds in decimal format (0 to 59)
//
//  Output:         None
//
//  Overview:       This function will store the specified value in the seconds
//                  register of the RTC
//
//  Note:           None
//******************************************************************************

void RTCSetSec(unsigned char sec)
{
    RTCByteWrite(0x00, (_Dec2Bcd(sec)&0x7F));
}

//******************************************************************************
//  Function:       void RTCSetMin(unsigned char min)
//
//  Input:          Minutes value in decimal format (0 to 59)
//
//  Output:         None
//
//  Overview:       This function will store the specified value in the Minutes
//                  register of the RTC
//
//  Note:           None
//******************************************************************************

void RTCSetMin(unsigned char min)
{
    RTCByteWrite(0x01, _Dec2Bcd(min));
}

//******************************************************************************
//  Function:       void RTCSetHr(unsigned char hr)
//
//  Input:          Hours value in decimal format (0 to 23)
//
//  Output:         None
//
//  Overview:       This function will store the specified value in the Hours
//                  register of the RTC
//
//  Note:           None
//******************************************************************************

void RTCSetHr(unsigned char hr)
{
    RTCByteWrite(0x02, (_Dec2Bcd(hr)&0x3F));
}

//******************************************************************************
//  Function:       void RTCSetDay(unsigned char day)
//
//  Input:          Day value in decimal format (1 to 7)
//
//  Output:         None
//
//  Overview:       This function will store the specified value in the Hours
//                  register of the RTC
//
//  Note:           None
//******************************************************************************

void RTCSetDay(unsigned char day)
{
    RTCByteWrite(0x03, (day));
}

//******************************************************************************
//  Function:       void RTCSetDate(unsigned char date)
//
//  Input:          Date value in decimal format (1 to 31)
//
//  Output:         None
//
//  Overview:       This function will store the specified value in the Date
//                  register of the RTC
//
//  Note:           None
//******************************************************************************

void RTCSetDate(unsigned char date)
{
    RTCByteWrite(0x04, _Dec2Bcd(date));
}

//******************************************************************************
//  Function:       void RTCSetMonth(unsigned char month)
//
//  Input:          Month value in decimal format (1 to 12)
//
//  Output:         None
//
//  Overview:       This function will store the specified value in the Month
//                  register of the RTC
//
//  Note:           None
//******************************************************************************

void RTCSetMonth(unsigned char month)
{
    RTCByteWrite(0x05, _Dec2Bcd(month));
}

//******************************************************************************
//  Function:       void RTCSetYear(unsigned char year)
//
//  Input:          Year value in decimal format (00 to 99)
//
//  Output:         None
//
//  Overview:       This function will store the specified value in the Year
//                  register of the RTC
//
//  Note:           None
//******************************************************************************

void RTCSetYear(unsigned char year)
{
    RTCByteWrite(0x06, _Dec2Bcd(year));
}

//******************************************************************************
//  Function:       void RTCGetCalendar(CALENDER *ptCalender)
//
//  Input:          Address of pointers Calender Structure
//
//  Output:         None
//
//  Overview:       This function will read the RTC for the Date, Month and Year
//                  and store the read values at the  specified addresses
//
//  Note:           None
//******************************************************************************

void RTCGetCalendar(CALENDAR *ptCalender)
{
    ptCalender->date = _Bcd2Dec(RTCByteRead(0x04)); //date
    ptCalender->month = _Bcd2Dec(RTCByteRead(0x05)); //month
    ptCalender->year = _Bcd2Dec(RTCByteRead(0x06)); //year
    //    ptCalender->day   =    (RTCByteRead(0x03)) ; //day
}

//******************************************************************************
//  Function:       void RTCGetTime(TIME *ptTime)
//
//  Input:          Pointer to TIME structure
//
//  Output:         None
//
//  Overview:       This function will read the RTC for the Seconds, Minutes and
//                  Hours and store the read values at the  specified addresses
//
//  Note:           None
//******************************************************************************

void RTCGetTime(TIME *ptTime)
{
    ptTime->sec = _Bcd2Dec(RTCByteRead(0x00)&0x7F); //seconds
    ptTime->min = _Bcd2Dec(RTCByteRead(0x01)&0x7F); //minutes
    ptTime->hour = _Bcd2Dec((RTCByteRead(0x02)&0x3F)); //hour
}

//******************************************************************************
//  Function:       unsigned char RTCGetSec(void)
//
//  Input:          None
//
//  Output:         Returns Seconds value in Decimal format
//
//  Overview:       This function will read the RTC for the Seconds value and
//                  return the read value in decmial format
//
//  Note:           None
//******************************************************************************

unsigned char RTCGetSec(void)
{
    return _Bcd2Dec((RTCByteRead(0x00))&0x7F); //seconds
}

//******************************************************************************
//  Function:       unsigned char RTCGetMin(void)
//
//  Input:          None
//
//  Output:         Returns Minutes value in Decimal format
//
//  Overview:       This function will read the RTC for the Minutes value and
//                  return the read value in decmial format
//
//  Note:           None
//******************************************************************************

unsigned char RTCGetMin(void)
{
    return _Bcd2Dec(RTCByteRead(0x01)); //minutes
}

//******************************************************************************
//  Function:       unsigned char RTCGetHr(void)
//
//  Input:          None
//
//  Output:         Returns Hours value in Decimal format
//
//  Overview:       This function will read the RTC for the Hours value and
//                  return the read value in decmial format
//
//  Note:           None
//******************************************************************************

unsigned char RTCGetHr(void)
{
    return _Bcd2Dec((RTCByteRead(0x02)&0x3F)); //hour
}

//******************************************************************************
//  Function:       unsigned char RTCGetDay(void)
//
//  Input:          None
//
//  Output:         Returns Day value in Decimal format
//
//  Overview:       This function will read the RTC for the Day value and
//                  return the read value in decmial format
//
//  Note:           None
//******************************************************************************

unsigned char RTCGetDay(void)
{
    return (RTCByteRead(0x03)); //day
}

//******************************************************************************
//  Function:       unsigned char RTCGetDate(void)
//
//  Input:          None
//
//  Output:         Returns Date value in Decimal format
//
//  Overview:       This function will read the RTC for the Date value and
//                  return the read value in decmial format
//
//  Note:           None
//******************************************************************************

unsigned char RTCGetDate(void)
{
    return _Bcd2Dec(RTCByteRead(0x04)); //date
}

//******************************************************************************
//  Function:       unsigned char RTCGetMonth(void)
//
//  Input:          None
//
//  Output:         Returns Month value in Decimal format
//
//  Overview:       This function will read the RTC for the Month value and
//                  return the read value in decmial format
//
//  Note:           None
//******************************************************************************

unsigned char RTCGetMonth(void)
{
    return _Bcd2Dec(RTCByteRead(0x05)); //month
}

//******************************************************************************
//  Function:       unsigned char RTCGetYear(void)
//
//  Input:          None
//
//  Output:         Returns Year value in Decimal format
//
//  Overview:       This function will read the RTC for the Year value and
//                  return the read value in decmial format
//
//  Note:           None
//******************************************************************************

unsigned char RTCGetYear(void)
{
    return _Bcd2Dec(RTCByteRead(0x06)); //year
}

//******************************************************************************
//  Function:       void RTCClkHalt(void)
//
//  Input:          None
//
//  Output:         None
//
//  Overview:       This function will stop the RTC clock
//
//  Note:           None
//******************************************************************************

void RTCClkHalt(void)
{
    RTCByteWrite(0x00, (1 << 7)); //Halts the oscillator of the RTC
}

//******************************************************************************
//  Function:       void RTCClkStart(void)
//
//  Input:          None
//
//  Output:         None
//
//  Overview:       This function will start the RTC clock
//
//  Note:           None
//******************************************************************************

void RTCClkStart(void)
{
    unsigned char byte;

    byte = RTCByteRead(0x00);
    RTCByteWrite(0x00, (byte & 0b01111111)); //Starts the oscillator of the RTC
}

//******************************************************************************
//  Function:       unsigned char _Bcd2Dec(unsigned char bcdNum)
//
//  Input:          BCD number
//
//  Output:         Decimal number
//
//  Overview:       This function will take the specified BCD number, convert
//                  it to decmial and return it
//
//  Note:           The function assumes that a valid BCD is given as input
//******************************************************************************

unsigned char _Bcd2Dec(unsigned char bcdNum)
{
    return ((bcdNum >> 4)*10 + (bcdNum & 0x0F));
}

//******************************************************************************
//  Function:       unsigned char _Dec2Bcd(unsigned char decNum)
//
//  Input:          Decimal number (0 to 99)
//
//  Output:         BCD number
//
//  Overview:       This function will take the specified decimal number, convert
//                  it to BCD and return it
//
//  Note:           The function assumes that the given decimal number is 2 digit
//******************************************************************************

unsigned char _Dec2Bcd(unsigned char decNum)
{
    return (decNum / 10) << 4 | (decNum % 10);
}

void RTCGetTimeInStr(char *timeStr)
{
    TIME time;

    RTCGetTime(&time);

    timeStr[0] = time.hour / 10 + '0';
    timeStr[1] = time.hour % 10 + '0';

    timeStr[2] = time.min / 10 + '0';
    timeStr[3] = time.min % 10 + '0';

    timeStr[4] = time.sec / 10 + '0';
    timeStr[5] = time.sec % 10 + '0';

    timeStr[6] = 0;

    return;
}

void RTCGetCalendarInStr(char *calendarStr)
{
    CALENDAR calendar;

    RTCGetCalendar(&calendar);

    calendarStr[0] = calendar.date / 10 + '0';
    calendarStr[1] = calendar.date % 10 + '0';

    calendarStr[2] = calendar.month / 10 + '0';
    calendarStr[3] = calendar.month % 10 + '0';

    calendarStr[4] = calendar.year / 10 + '0';
    calendarStr[5] = calendar.year % 10 + '0';

    calendarStr[6] = 0;

    return;
}

void RTCGetTimeInStrWithSeperator(char *timeStr, char seperator)
{
    TIME time;

    RTCGetTime(&time);

    timeStr[0] = time.hour / 10 + '0';
    timeStr[1] = time.hour % 10 + '0';

    timeStr[2] = seperator;

    timeStr[3] = time.min / 10 + '0';
    timeStr[4] = time.min % 10 + '0';

    timeStr[5] = seperator;

    timeStr[6] = time.sec / 10 + '0';
    timeStr[7] = time.sec % 10 + '0';

    timeStr[8] = 0;

    return;
}

void RTCGetCalendarInStrWithSeperator(char *calendarStr, char seperator)
{
    CALENDAR calendar;

    RTCGetCalendar(&calendar);

    calendarStr[0] = calendar.date / 10 + '0';
    calendarStr[1] = calendar.date % 10 + '0';

    calendarStr[2] = seperator;

    calendarStr[3] = calendar.month / 10 + '0';
    calendarStr[4] = calendar.month % 10 + '0';

    calendarStr[5] = seperator;

    calendarStr[6] = calendar.year / 10 + '0';
    calendarStr[7] = calendar.year % 10 + '0';

    calendarStr[8] = 0;

    return;
}


