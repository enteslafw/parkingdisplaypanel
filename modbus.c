//OM GAN GANPATAYE NAMAHA

#include "Compiler.h"
#include "GenericTypeDefs.h"
#include "HardwareProfile.h"
#include "global_vars.h"
#include "multitasker.h"
#include "CircularBuffer.h"
#include "En_Consol.h"
#include "helper.h"
#include "uart2.h"

#define U1RX_Clear_Intr_Status_Bit IFS0bits.U1RXIF=0;
#define U1TX_Clear_Intr_Status_Bit IFS0bits.U1TXIF=0;
#define DE_RE_PIN_DIR        (TRISFbits.TRISF3)
#define DE_RE_PIN        (LATFbits.LATF3)

static TICKS _uart1TimeoutMs = 10, _timeoutMs;

static CIRCULAR_BUFFER _uart1RxBuff, _uart1TxBuff;

static unsigned int _Uart1PutCh(unsigned char ch);
static unsigned int _Uart1PutS(unsigned char *str);
static unsigned int _Uart1GetS(char *str);
static unsigned int _Uart1GetCh(char *pCh);

unsigned char ModbusInit(unsigned long baudRateToSet, unsigned long timeoutMs)
{
    unsigned int status;
    unsigned long closestSpbrgValue;

    _TRISF8 = 0;
    _TRISF2 = 1;

    InitQueue(&_uart1RxBuff);
    InitQueue(&_uart1TxBuff);

    _uart1TimeoutMs = timeoutMs;

    //    CloseUART2();
    //    //disable UART if enabled previously
    //    U2TX_Clear_Intr_Status_Bit;
    //
    //    ConfigIntUART2(UART_RX_INT_EN | UART_RX_INT_PR6 | UART_TX_INT_EN | UART_TX_INT_PR6);
    //    OpenUART2(UART_EN | UART_BRGH_FOUR, UART_TX_ENABLE, u2brg); //configure UART and enable it

    //    UARTConfigure(UART4, UART_ENABLE_PINS_TX_RX_ONLY);
    //    UARTSetFifoMode(UART4, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    //    UARTSetLineControl(UART4, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    //    UARTSetDataRate(UART4, GetPeripheralClock(), baudRateToSet);
    //    UARTEnable(UART4, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));

    U1MODE = 0x8000; // Set UARTEN.  Note: this must be done before setting UTXEN
    IPC6bits.U1IP = 5; // Priority level 5
    U1STA = 0x00001400; // RXEN set, TXEN set
    closestSpbrgValue = ((GetPeripheralClock() + 8 * baudRateToSet) / 16 / baudRateToSet - 1);
    U1BRG = closestSpbrgValue;
    IEC0bits.U1RXIE = 1;

    DE_RE_PIN_DIR = 0; //output pin
    DE_RE_PIN = 0; //RX ENABLE
    _timeoutMs = MULTITASKER_GET_TICK();

    return 1;

}

void ModbusPutNum(unsigned int n)
{
    char str[15];

    IntToAscii(n, str);
    ModbusPutS(str);
}

void ModbusPutS(char *s)
{
    _Uart1PutS(s);
}

void ModbusPutCh(char ch)
{
    _Uart1PutCh(ch);
}

//void ModbusPutSRx(char *str)
//{
//    while (*str)
//    {
//        PushToQueue(&_uart1RxBuff, *str);
//        str++;
//    }
//}

void ModbusGetCh(char *ch)
{
    _Uart1GetCh(ch);
}

unsigned int ModbusGetS(char *s)
{
    return (_Uart1GetS(s));
}

unsigned int ModbusIsAvailable(void)
{
#if 1
    return SizeOfQueue(&_uart1RxBuff);
#else
    return (endIndx);
#endif
}

void ModbusClearRxBuff(void)
{
#if 1
    ClearQueue(&_uart1RxBuff);
#else    
    startIndx = 0;
    endIndx = 0;
    memset(uart1Inbuff, '\0', sizeof (uart1Inbuff));
#endif
}

static unsigned int _Uart1GetS(char *str)
{
    unsigned int status = 0;
    unsigned int i = 0;

    while (0 == status)
    {
        status = PopFromQueue(&_uart1RxBuff, (str + i));
        if (1 == status)
        {
            str[i] = 0;
        }
        else
        {
            i++;
        }
    }

    return i;
}

static unsigned int _Uart1GetCh(char *pCh)
{
#if 1
    unsigned int status;

    status = PopFromQueue(&_uart1RxBuff, pCh);

    return status;
#else
    if (startIndx<sizeof (uart1Inbuff))
    {
        *pCh = uart1Inbuff[startIndx];
        startIndx++;

        return 1;
    }
    else
        return 0;
#endif
}

static unsigned int _Uart1PutCh(unsigned char ch)
{
#if 1
    PushToQueue(&_uart1TxBuff, ch);
    DE_RE_PIN = 1; //rs485 tx enable
    //    EnableIntU1TX;
    INTEnable(INT_SOURCE_UART_TX(UART1), INT_ENABLED);
    IFS0bits.U1TXIF = 1;
#else
    EnableIntU1TX;

    while (BusyUART1()); // wait till the UART is busy
    WriteUART1((unsigned int) ch); // Transmit the data
    //    //transmit ONLY if TX buffer is empty
    //    while (U1STAbits.UTXBF == 1);
    //    U1TXREG = Ch;
    DisableIntU1TX; // disable the UART TX interrupt after end of complete
#endif
    return 1;
}

static unsigned int _Uart1PutS(unsigned char *str)
{
#if 1
    while (*str)
    {
        PushToQueue(&_uart1TxBuff, *str);
        str++;
    }
    DE_RE_PIN = 1; //rs485 tx enable
    //    EnableIntU1TX;
    INTEnable(INT_SOURCE_UART_TX(UART1), INT_ENABLED);
    IFS0bits.U1TXIF = 1;
#else
    unsigned char ch;
    EnableIntU1TX;
    while (*str)
    {
        while (BusyUART1()); // wait till the UART is busy
        ch = *str;
        WriteUART1((unsigned int) ch); // Transmit the data
        str++;
    }

    //IFS0bits.U1TXIF = 1;
    DisableIntU1TX; // disable the UART TX interrupt after end of complete
#endif
    return 1;
}

void ModbusPutArray(unsigned char *arr, unsigned int len)
{
#if 1
    unsigned int i;

    for (i = 0; i < len; i++)
    {
        PushToQueue(&_uart1TxBuff, arr[i]);
    }
    DE_RE_PIN = 1; //rs485 tx enable
    //    EnableIntU1TX;
    INTEnable(INT_SOURCE_UART_TX(UART1), INT_ENABLED);
    IFS0bits.U1TXIF = 1;
#else
    unsigned int i;
    EnableIntU1TX;
    for (i = 0; i < len; i++)
    {
        while (BusyUART1()); // wait till the UART is busy
        WriteUART1((unsigned int) arr[i]); // Transmit the data
    }
    DisableIntU1TX; // disable the UART TX interrupt after end of complete
#endif
}

void Uart1PutCh(char txCh)
{
    while (!UARTTransmitterIsReady(UART1));
    UARTSendDataByte(UART1, txCh);
    while (!UARTTransmissionHasCompleted(UART1));
}

unsigned int ModbusPresetMultipleRegisters(unsigned char slaveId, unsigned int startAdd, unsigned int noOfReg, unsigned char *dataBytes)
{
    unsigned int crc_word;
    unsigned char modbusQ[15]; //(noOfReg * 2) + 9

    //validate noOfReg and length of dataBytes
    modbusQ[0] = (slaveId); //modbus slave id
    modbusQ[1] = (0x10); //Preset Multiple Registers (FC=16)
    modbusQ[2] = (unsigned char) (startAdd >> 8); //starting address, ( 0001 hex = 1 , + 40001 offset = register #40002 )
    modbusQ[3] = (unsigned char) (startAdd & 0x00FF);
    modbusQ[4] = (unsigned char) (noOfReg >> 8); //no. of registers to write
    modbusQ[5] = (unsigned char) (noOfReg & 0x00FF);
    modbusQ[6] = (unsigned char) (noOfReg * 2);
    modbusQ[7] = (dataBytes[0]); //The value to write to register 40001 
    modbusQ[8] = (dataBytes[1]);
    modbusQ[9] = (dataBytes[2]); //The value to write to register 40002 
    modbusQ[10] = (dataBytes[3]);
    modbusQ[11] = (dataBytes[4]); //The value to write to register 40003 
    modbusQ[12] = (dataBytes[5]);
    crc_word = CRC_WORD(modbusQ, 13);
    modbusQ[13] = crc_word & 0x00ff;
    modbusQ[14] = crc_word >> 8;

    //send to uart
    ClearQueue(&_uart1RxBuff);
    //    ClearQueue(&_uart1TxBuff);
    //    DE_RE_PIN = 1; //tx enable
    //    for (crc_word = 0; crc_word < 13; crc_word++)
    //    {
    //        //            UART2PutChar(modbusQ[crc_word]);
    //        Uart1PutCh(modbusQ[crc_word]);
    //        DelayMs(10);
    //    }
    //    DE_RE_PIN = 0; //rx enable

    ModbusPutArray(modbusQ, 15);
}

//
// Interrupt Service routine for UART2 Transmission
//

void _U1TXInterrupt(void)
{
    static char ch = 0;

    U1TX_Clear_Intr_Status_Bit; // clear the interrupt status of UART4 TX
    if (PopFromQueue(&_uart1TxBuff, &ch))
    {
        DE_RE_PIN = 0; //rs485 rx enable
        //DisableIntU2TX; // disable the UART TX interrupt after end of complete
        INTEnable(INT_SOURCE_UART_TX(UART1), INT_DISABLED);
        // transmission
    }
    else
    {
        DE_RE_PIN = 1; //rs485 tx enable
        //while (BusyUART4()); // wait till the UART is busy
        //WriteUART4((unsigned int) ch); // Transmit the data
        while (!UARTTransmitterIsReady(UART1));
        UARTSendDataByte(UART1, (unsigned int) ch);
        while (!UARTTransmissionHasCompleted(UART1));
    }
}


//
// Interrupt Service routine for UART2 reception
//

void _U1RXInterrupt(void)
{
    static unsigned char rxdCh = 0;

    //while (!DataRdyUART2()); //wait for data reception on RX
    //    while(!U4STAbits.URXDA);

    rxdCh = UARTGetDataByte(UART1);

    //    if ('\r' == rxdCh)
    //    {
    //        systemFlag.pcCommCmdRxd = 1;
    //    }
    //    else
    //    {
    PushToQueue(&_uart1RxBuff, rxdCh);
    PcCommPutCh(rxdCh);
    //    }

    U1RX_Clear_Intr_Status_Bit; //clear the interrupt status of UART RX after reading

    //    while (!DataRdyUART2()); //wait for data reception on RX
    //    PushToQueue(&_uart1RxBuff, UARTGetDataByte(UART4));
}

void __attribute((interrupt(IPL5SOFT), vector(_UART_1_VECTOR), nomips16)) U1Interrupt(void)
{
    if (IFS0bits.U1RXIF)
        _U1RXInterrupt();
    if (IEC0bits.U1TXIE)
    {
        if (IFS0bits.U1TXIF)
            _U1TXInterrupt();
    }
}