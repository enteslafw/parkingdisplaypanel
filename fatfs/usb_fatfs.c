#include "GenericTypeDefs.h"
#include "HardwareProfile.h"
#include "USB\usb.h"
//#include "USB\usb_host_msd.h"
#include "USB\usb_host_msd_scsi.h"
#include "fatfs\diskio.h"
#include "fatfs\ff.h"

static volatile DSTATUS Stat = STA_NOINIT;

/******************************************************************************
 * Function:        DSTATUS USB_disk_initialize(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          Status after init
 *
 * Side Effects:    None
 *
 * Overview:        Function to initialize the Media called during mount
 *
 * Note:            None
 *****************************************************************************/
DSTATUS USB_disk_initialize(void)
{
#if 1
    MEDIA_INFORMATION *mediaInformation;

    //    dsk->mount = FALSE; // default invalid
    //    dsk->buffer = gDataBuffer; // assign buffer

    // Initialize the device
    mediaInformation = USBHostMSDSCSIMediaInitialize();
    if (mediaInformation->errorCode != MEDIA_NO_ERROR)
    {
        //        error = CE_INIT_ERROR;
        //        FSerrno = CE_INIT_ERROR;
        return STA_NODISK;
    }
#else
    if (!USBHostMSDSCSIMediaInitialize())
    {
        return STA_NODISK;
    }
#endif
    return 0;
}

/******************************************************************************
 * Function:        DSTATUS USB_disk_status(void)
 *
 * PreCondition:    None
 *
 * Input:           drv - Physical drive number
 *
 * Output:          Status of drive
 *
 * Side Effects:    None
 *
 * Overview:        Function to return status of drive
 *
 * Note:            None
 *****************************************************************************/
DSTATUS USB_disk_status(void
                        //BYTE drv		/* Physical drive nmuber (0..) */
                        )
{
    if (!USBHostMSDSCSIMediaDetect())
    {
        return STA_NODISK;
    }
    return 0;
}

/******************************************************************************
 * Function:        DRESULT USB_disk_read(BYTE *buff,
 *									  DWORD sector,
 *									  BYTE count)
 *
 * PreCondition:    None
 *
 * Input:           buff - pointer to the data buffer to store read data
 *					sector - start sector number (LBA)
 *					count - sector count (1..255)
 *
 * Output:          Status of read
 *
 * Side Effects:    None
 *
 * Overview:        Function to read a specific sector on the media.
 *
 * Note:            None
 *****************************************************************************/
DRESULT USB_disk_read(
                      //BYTE pdrv,		/* Physical drive nmuber (0..) */
                      BYTE *buff, /* Data buffer to store read data */
                      DWORD sector, /* Sector address (LBA) */
                      BYTE count /* Number of sectors to read (1..255) */
                      )
{
    int nSector;
    for (nSector = 0; nSector < count; nSector++)
    {
        if (!USBHostMSDSCSISectorRead(sector, buff))
        {
            return RES_NOTRDY;
        }
        sector++;
        buff += 512;
    }
    return RES_OK;
}

/******************************************************************************
 * Function:        DRESULT USB_disk_write(const BYTE *buff,
 *									   DWORD sector,
 *									   BYTE count)
 *
 * PreCondition:    None
 *
 * Input:           buff - Pointer to the data to be written
 *					sector - Start sector number (LBA)
 *					count - Sector count (1..255)
 *
 * Output:          Status of write
 *
 * Side Effects:    None
 *
 * Overview:        Function to write a specific sector on the media.
 *
 * Note:            None
 *****************************************************************************/
#if _READONLY == 0

DRESULT USB_disk_write(
                       //BYTE pdrv,			/* Physical drive nmuber (0..) */
                       const BYTE *buff, /* Data to be written */
                       DWORD sector, /* Sector address (LBA) */
                       BYTE count /* Number of sectors to write (1..255) */
                       )
{
    int nSector;
    for (nSector = 0; nSector < count; nSector++)
    {
        //        BYTE rc;
        //        rc = USBHostMSDSCSISectorWrite(sector, (BYTE *)buff, 0);
        //        if (rc == USB_MSD_DEVICE_NOT_FOUND)
        //        {
        //            return RES_NOTRDY;
        //        }
        //        if (rc != USB_SUCCESS)
        //        {
        //            return RES_ERROR;
        //        }
        if (!USBHostMSDSCSISectorWrite(sector, (BYTE *) buff, 0))
        {
            return RES_NOTRDY;
        }
        sector++;
        buff += 512;

    }
    return RES_OK;
}
#endif

/******************************************************************************
 * Function:        DRESULT USB_disk_ioctl(BYTE ctrl, void* buff)
 *
 * PreCondition:    None
 *
 * Input:           ctrl - control code
 *					buff - buffer to send/receive data block
 *
 * Output:          Success/Failure
 *
 * Side Effects:    None
 *
 * Overview:        Perform miscellaneous control functions
 *
 * Note:            None
 *****************************************************************************/
DRESULT USB_disk_ioctl(
                       //BYTE drv,		/* Physical drive nmuber (0..) */
                       BYTE ctrl, /* Control code */
                       void *buff /* Buffer to send/receive control data */
                       )
{
    switch (ctrl)
    {
    case GET_SECTOR_SIZE:
        *(WORD *) buff = 512;
        return RES_OK;

#if _READONLY == 0
    case CTRL_SYNC:
        return RES_OK;

    case GET_SECTOR_COUNT:
        *(DWORD *) buff = 0; // Number of sectors on the volume
        return RES_OK;

    case GET_BLOCK_SIZE:
        return RES_OK;
#endif
    }
    return RES_PARERR;
}

DRESULT USB_disk_Deinitialize(void)
{
    if(!USBHostMSDSCSIMediaReset())
        return RES_NOTRDY;
    
    return RES_OK;
}