/* 
 * File:   mmc.h
 * Author: Umesh
 *
 * Created on August 22, 2016, 1:00 PM
 */

#ifndef MMC_H
#define	MMC_H

#ifdef	__cplusplus
extern "C" {
#endif

#define _READONLY	0	/* 1: Read-only mode */
#define _USE_IOCTL	1

    DSTATUS MMC_disk_initialize(void);
    DSTATUS MMC_disk_status(void);
    DRESULT MMC_disk_read(
            BYTE *buff, /* Pointer to the data buffer to store read data */
            DWORD sector, /* Start sector number (LBA) */
            BYTE count /* Sector count (1..255) */
            );

#if _READONLY == 0
    DRESULT MMC_disk_write(
            const BYTE *buff, /* Pointer to the data to be written */
            DWORD sector, /* Start sector number (LBA) */
            BYTE count /* Sector count (1..255) */
            );
#endif

    DRESULT MMC_disk_ioctl(
            BYTE ctrl, /* Control code */
            void *buff /* Buffer to send/receive data block */
            );
    
    DRESULT MMC_disk_Deinitialize(void);

#ifdef	__cplusplus
}
#endif

#endif	/* MMC_H */

