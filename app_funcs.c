
#include <xc.h>
#include "global_vars.h"
#include "app_funcs.h"
#include "rtc.h"
#include "fatfs/diskio.h"
#include "fatfs/ff.h"
#include "log.h"

unsigned long get_fattime(void)
{
    //Currnet time is returned with packed into a DWORD value. The bit field is as follows:
    //bit31:25 => Year origin from the 1980 (0..127)
    //bit24:21 => Month (1..12)
    //bit20:16 => Day of the month(1..31)
    //bit15:11 => Hour (0..23)
    //bit10:5 => Minute (0..59)
    //bit4:0 => Second / 2 (0..29)
    unsigned long tmr = 0, tmr1;
    CALENDAR calendar;
    TIME time;

    Nop();
    Nop();
    Nop();
    RTCGetCalendar(&calendar);
    RTCGetTime(&time);

    tmr = 0;

    tmr1 = 0;
    tmr1 |= (calendar.year + 2000) - 1980;
    tmr1 <<= 25;
    tmr |= tmr1;


    tmr1 = 0;
    tmr1 |= calendar.month;
    tmr1 <<= 21;
    tmr |= tmr1;

    tmr1 = 0;
    tmr1 |= calendar.date;
    tmr1 <<= 16;
    tmr |= tmr1;

    tmr1 = 0;
    tmr1 |= time.hour;
    tmr1 <<= 11;
    tmr |= tmr1;

    tmr1 = 0;
    tmr1 |= time.min;
    tmr1 <<= 5;
    tmr |= tmr1;

    tmr1 = 0;
    tmr1 |= time.sec / 2;
    //    tmr1 <<= 0;
    tmr |= tmr1;

    Nop();
    Nop();
    Nop();
    return tmr;
}

void AppInit(void)
{
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int status;

    res = f_open(&fsrc, _SD_ SYSPARAM_FILE, FA_OPEN_EXISTING | FA_READ);
    if (res != FR_OK)
    {
        //
        //  we reach here if file not found
        //
        status = AppMakeDefaultSystemParamFile();
        if (0 == status)
        {
            Nop();
        }
    }
    else
    {
        res = f_close(&fsrc);
        if (res != FR_OK)
        {
            Nop();
        }
    }

    res = f_open(&fsrc, _SD_ LOGDATA_FILE, FA_OPEN_EXISTING | FA_READ);
    if (res != FR_OK)
    {
        //
        //  we reach here if file not found
        //
        status = AppMakeDefaultLogFile();
        if (0 == status)
        {
            Nop();
            Nop();
        }
    }
    else
    {
        res = f_close(&fsrc);
        if (res != FR_OK)
        {
            Nop();
        }
    }

    //    AppMakeDefaultSystemParamFile();
    //    AppMakeDefaultLogFile();
    AppGetSystemParamFrmFile(&systemParam);
    
     //check if new firmware is upgraded
    res = f_open(&fsrc, _SD_ PRE_FIRMWARE_FILENAME, FA_OPEN_EXISTING | FA_READ);
    if (res != FR_OK)
    {
        //file not found
        //no new firmware upgrade.
//        AppLog(MSG_LOG, "System Restarted");
        PcCommPutS("FirmWare ");
        PcCommPutS(firmwareVersion);
        PcCommPutS(" ");
        PcCommPutS(firmwareBuild);
        PcCommPutS("\r\n");
    }
    else
    {
        //file found
        res = f_close(&fsrc);
        if (res != FR_OK)
        {
            Nop();
        }
        res = f_unlink(_SD_ DEFAULT_FIRMWARE);
        Nop();
        res = f_rename(_SD_ PRE_FIRMWARE_FILENAME, _SD_ DEFAULT_FIRMWARE);
        Nop();
//        AppLog(MSG_LOG, "New Firmware Upgraded");
//        AppLog(MSG_LOG, firmwareVersion);
//        AppLog(MSG_LOG, firmwareBuild);
    }
}

unsigned int AppMakeDefaultSystemParamFile(void)
{
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int size, numOfBytesWritten;
    SYSTEM_PARAM sysPar;

    strcpy(sysPar.srvIp, "192.168.0.4");
    strcpy(sysPar.srvPort, "12345");
    strcpy(sysPar.loginId, "admin");
    strcpy(sysPar.loginPass, "admin");
    //    strcpy(sysPar.rid, "TM000");
    //    strcpy(sysPar.keyStr, "FFFFFFFFFFFF");
    //    sysPar.timeSyncMin = 600;

    strcpy(sysPar.udpLocalPort, "30243");
    strcpy(sysPar.udpRemotePort, "30243");
    //    strcpy(sysPar.cloudIpUrl, "www.timemuster.com");
    //    strcpy(sysPar.cloudPort, "80");

    for (size = 0; size < MAX_LED_DISPLAY; size++)
    {
        sysPar.ledDisplayParam[size].isEnable = 1;       
        sysPar.ledDisplayParam[size].modbusId = (char) (size + 1);
        sysPar.ledDisplayParam[size].brightness = '5';
        strcpy(sysPar.ledDisplayParam[size].devId, "1234");
        strcpy(sysPar.ledDisplayParam[size].lastValue, "8888");
    }

    //    sysPar.logDataFlag = 0;
    //    sysPar.updateLogTimeoutSec = 300; // 5 mins
    //    sysPar.numberOfLogsInMail = 50;
    //    strcpy(sysPar.bearerApnStr, DEFAULT_BEARER_APN_STR);
    //    strcpy(sysPar.popServerAddrStr, DEFAULT_POP_SERVER_ADDR_STR);
    //    strcpy(sysPar.popServerPortNumStr, DEFAULT_POP_SERVER_PORT_NUM_STR);
    //    strcpy(sysPar.smtpServerAddrStr, DEFAULT_SMTP_SERVER_ADDR_STR);
    //    strcpy(sysPar.smtpServerPortNumStr, DEFAULT_SMTP_SERVER_PORT_NUM_STR);
    //    strcpy(sysPar.emailUserIdStr, DEFAULT_EMAIL_USER_ID_STR);
    //    strcpy(sysPar.emailPswStr, DEFAULT_EMAIL_USER_PSW_STR);
    //    strcpy(sysPar.emailFromAddrStr, DEFAULT_EMAIL_FROM_ADDR_STR);
    //    strcpy(sysPar.emailFromNameStr, DEFAULT_EMAIL_FROM_NAME_STR);
    //    strcpy(sysPar.emailToAddrStr, DEFAULT_EMAIL_TO_ADDR_STR);
    //    strcpy(sysPar.emailToNameStr, DEFAULT_EMAIL_TO_NAME_STR);
    //    strcpy(sysPar.emailCcToAddrStr, "");
    //    strcpy(sysPar.emailCcToNameStr, "");
    //    strcpy(sysPar.emailBccToAddrStr, "");
    //    strcpy(sysPar.emailBccToNameStr, "");
    //    strcpy(sysPar.emailSubjectStr, DEFAULT_EMAIL_SUB_STR);
    //    strcpy(sysPar.pumpInfoStr, DEFAULT_PUMP_INFO_STR);

    res = f_open(&fsrc, _SD_ SYSPARAM_FILE, FA_CREATE_ALWAYS | FA_WRITE);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    size = sizeof (SYSTEM_PARAM);
    res = f_write(&fsrc, sysPar.array, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 0;
    }

    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    return 1;
}

unsigned int AppGetSystemParamFrmFile(SYSTEM_PARAM *sysPar)
{
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int size, numOfBytesRead;

    res = f_open(&fsrc, _SD_ SYSPARAM_FILE, FA_OPEN_EXISTING | FA_READ);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    size = sizeof (SYSTEM_PARAM);
    res = f_read(&fsrc, sysPar->array, size, &numOfBytesRead);
    if ((res != FR_OK) || (numOfBytesRead != size))
    {
        f_close(&fsrc);
        return 0;
    }

    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    return 1;
}

unsigned int AppMakeDefaultLogFile(void)
{
    LOG_INFO logInfo;
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int numOfBytesWritten, size;
    unsigned char ch;

    res = f_open(&fsrc, _SD_ LOGDATA_FILE, FA_CREATE_ALWAYS | FA_WRITE);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    logInfo.current = 0;
    logInfo.start = 0;
    logInfo.end = 0;

    //
    //  Add default entries to file
    //
    size = sizeof (logInfo.array);
    res = f_write(&fsrc, logInfo.array, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 0;
    }

    ch = '\r';
    size = sizeof (ch);
    res = f_write(&fsrc, &ch, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 0;
    }

    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }
    return 1;
}

unsigned int AppSaveSystemParamToFile(SYSTEM_PARAM *sysPar)
{
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int size, numOfBytesWritten;

    res = f_open(&fsrc, _SD_ SYSPARAM_FILE, FA_CREATE_ALWAYS | FA_WRITE);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    size = sizeof (SYSTEM_PARAM);
    res = f_write(&fsrc, sysPar->array, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 0;
    }

    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    return 1;
}
