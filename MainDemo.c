/*********************************************************************
 *
 *  Main Application Entry Point and TCP/IP Stack Demo
 *  Module for Microchip TCP/IP Stack
 *   -Demonstrates how to call and use the Microchip TCP/IP stack
 *   -Reference: Microchip TCP/IP Stack Help (TCPIP Stack Help.chm)1150

 *
 *********************************************************************
 * FileName:        MainDemo.c
 * Dependencies:    TCPIP.h
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.11b or higher
 *                  Microchip C30 v3.24 or higher
 *                  Microchip C18 v3.36 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2010 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *      ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *      used in conjunction with a Microchip ethernet controller for
 *      the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 * File Description:
 * Change History:
 * Rev   Description
 * ----  -----------------------------------------
 * 1.0   Initial release
 * V5.36 ---- STACK_USE_MPFS support has been removed 
 ********************************************************************/
/*
 * This macro uniquely defines this file as the main entry point.
 * There should only be one such definition in the entire project,
 * and this file must define the AppConfig variable as described below.
 */
#define THIS_IS_STACK_APPLICATION

// Include all headers for any enabled TCPIP Stack functions
#include "TCPIP Stack/TCPIP.h"
// Include functions specific to this stack application
#include "MainDemo.h"
#include "global_vars.h"
#include "rtc.h"
//#include "EnteslaMifareRW.h"
#include "multitasker.h"
#include "tasks.h"
#include "fatfs/diskio.h"
#include "fatfs/ff.h"
#include "app_funcs.h"
#include "pc_comm.h"
#include "tcp_client.h"
#include "log.h"
#include "helper.h"
#include "EMS_FTPClient.h"
#include "glcd.h"
#include "IO.h"
#include "usb_config.h"
#include "USB/usb.h"
#include "USB/usb_host_msd.h"
#include "En_Consol.h"
#include "fatfs/diskio.h"
#include "fatfs/ff.h"
#include "modbus.h"

// Declare AppConfig structure and some other supporting stack variables
APP_CONFIG AppConfig;
static unsigned short wOriginalAppConfigChecksum; // Checksum of the ROM defaults for AppConfig
BYTE AN0String[8];
FTP_PARAM ftpParam;
//char jsonObjArrStr[2048];

// Private helper functions.
// These may or may not be present in all applications.
static void InitAppConfig(void);
static void InitializeBoard(void);
static void ProcessIO(void);

static FATFS Fatfs, FatFsUsb;

static enum _MAIN_APP_SM
{
    STEP1 = 0,
    STEP2,
    STEP3,
    STEP4,
    STEP5,
    STEP6,
    STEP7,
    STEP8,
    STEP9
} MAIN_APP_SM = STEP1;

//BYTE ftpDataBuffer[] = {
//    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '#', '}', '!', '"', '*', '$', '%',
//    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '#', '}', '!', '"', '*', '$', '%',
//    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '#', '}', '!', '"', '*', '$', '%',
//    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '#', '}', '!', '"', '*', '$', '%',
//    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '#', '}', '!', '"', '*', '$', '%',
//    'a', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '#', '}', '!', '"', '*', '$', '%',
//    '\0'
//};
BOOL deviceAttached = FALSE;
char writeOnce = 0;

//#define InitTextIO()    UART2Init()
//#define PutChar(c)      UART2PutChar(c)
//#define PutHex(h)       UART2PutHex(h)
//#define PrintString(s)  UART2PrintString(s)

//void _general_exception_handler(unsigned cause, unsigned status)
//{
//    Nop();
//    Nop();
//    while (1);
//}

//
// Main application entry point.
//
unsigned int numOfLogs;
//TIME sactTime;
//CALENDAR sactCal;
char srvActivityCalStr[16] = "NA";
char srvActivityTimeStr[16] = "NA";

// *****************************************************************************
// *****************************************************************************
// Macros
// *****************************************************************************
// *****************************************************************************

#define IsNumm(c)            ((('0' <= c) && (c <= '9')) ? TRUE : FALSE)
#define UpperCase(c)        (('a'<= c) && (c <= 'z') ? c - 0x20 : c)
#define SkipWhiteSpace()    { while (commandInfo.buffer[commandInfo.index] == ' ') commandInfo.index++; }

// *****************************************************************************
// *****************************************************************************
// Internal Function Prototypes
// *****************************************************************************
// *****************************************************************************

int main(void)
{
    unsigned int len, currentDisplay = 0;

    //    char cmdStr[] = "GET gkP";
    //    char replyStr[32]={0};
    _TRISD7 = 0;
    _LATE4 = 0;
    _LATE3 = 0;
    _TRISE4 = 0;
    _TRISE3 = 0;
    //    TIME rtcTime, rtcTimeR;
    //    CALENDAR rtcCal;
    static DWORD dwLastIP = 0;
    ETCB checkInputsTcb; //checkCardTcb, 
    TICKS checkLogsTickMs = 0, localTicks = 0, checkServerTick = 0;
    //    TICKS waitForDcTicks = 0, updateDateTimeTicksS = 0, getKeyTicksS = 0;
    TICKS ledTicksMs = 0;
    //    char body[128];
    unsigned int i; //j, setKeyFrmSrvTries = 0
    //    TRANSACTION trans;
    //unsigned int tcpDataLen;
    char dummyStr[512];
    unsigned int ledState = 0;

    DSTATUS dstatus;
    FIL fsrc, fsrc1; /* file objects */
    FRESULT res; /* FatFs function common result code */
    int numOfBytesWritten, numOfBytesRead;


    MultiTaskerInit();
    //    CreateTask(&checkCardTcb, TASK_PRIORITY_3, TASK_ID_1, 150,  \
//	                                        TASK_ENABLE, TASK_CheckCard);

    CreateTask(&checkInputsTcb, TASK_PRIORITY_3, TASK_ID_1, 15,  \
	                                        TASK_ENABLE, TASK_CheckInputs);
    // Initialize application specific hardware
    //AppMakeDefaultSystemParamFile();
    InitializeBoard();

    //    UART2Init();
    //    PcCommExecuteCmd(cmdStr, replyStr);/

    ////    rtcTime.hour = 17;
    ////    rtcTime.min = 32;
    ////    rtcTime.sec = 0;
    ////
    ////    rtcCal.month = 2;
    ////    rtcCal.date = 29;
    ////    rtcCal.year = 16;
    ////
    ////    rtcTimeR.hour = 10;
    ////    rtcTimeR.min = 10;
    ////    rtcTimeR.sec = 5;

    //    RTCSetTime(&rtcTime);
    //    RTCSetCalendar(&rtcCal);
    //    RTCSetDay(FRIDAY);

    DelayMs(1000);
    //    RTCGetTime(&rtcTimeR);  

    Nop();
    Nop();

    // Initialize stack-related hardware components that may be 
    // required by the UART configuration routines
    TickInit();

#if defined(STACK_USE_MPFS2)
    MPFSInit();
#endif

    // Initialize Stack and application related NV variables into AppConfig.
    InitAppConfig();

    // Initialize core stack layers (MAC, ARP, TCP, UDP) and
    // application modules (HTTP, SNMP, etc.)
    StackInit();

    //#if defined (STACK_USE_EMS_FTP_CLIENT)
    //    if (systemFlag.SearchFirmware)
    //        FTPC_SendInit(91, 1, FTPC_DOWNLOAD);
    //#endif

    checkLogsTickMs = MULTITASKER_GET_TICK();
    localTicks = MULTITASKER_GET_TICK();
    checkServerTick = MULTITASKER_GET_TICK();

    //start TCPIP client
    TcpClientDisconnect();

    //    InitTextIO();
    PcCommPutS("\r\n\r\n***** ETAM - 1000 *****\r\n\r\n");

    // Initialize USB layers
    USBInitialize(0);

    for (i = 0; i < MAX_LED_DISPLAY; i++)
    {
        systemFlag.updateLedDisplay[i] = 1;
        systemFlag.isConnected[i] = 0;
    }
    //
    // Now that all items are initialized, begin the co-operative
    // multitasking loop.  This infinite loop will continuously 
    // execute all stack-related tasks, as well as your own
    // application's functions.  Custom functions should be added
    // at the end of this loop.
    // Note that this is a "co-operative multi-tasking" mechanism
    // where every task performs its tasks (whether all in one shot
    // or part of it) and returns so that other tasks can do their
    // job.
    // If a task needs very long time to do its job, it must be broken
    // down into smaller pieces so that other tasks can have CPU time.
    //
    while (1)
    {
#if 0
        if ((MULTITASKER_GET_TICK() - checkLogsTickMs) > 1000)
        {
            //                    if (SizeOfEmrwCmdResponse())
            //                    {
            //                        EmrwPutS("OK Wait!\r\n");
            //                        break;
            //                    }
            //            ModbusPutS("uart ");
            for (i = 0; i < MAX_LED_DISPLAY; i++)
            {
                if ((1 == systemParam.ledDisplayParam[i].isEnable) && (1 == systemFlag.updateLedDisplay[i]))
                {

                    ModbusPresetMultipleRegisters((i + 1), 0x0001, 0x0002, systemParam.ledDisplayParam[i].lastValue);
                    DelayMs(1000);
                    if (ModbusIsAvailable() > 0)
                    {
                        systemFlag.updateLedDisplay[i] = 0;
                    }
                }
            }
            checkLogsTickMs = MULTITASKER_GET_TICK();
        }

#else
        switch (MAIN_APP_SM)
        {
        case STEP1:
            if (currentDisplay >= MAX_LED_DISPLAY)
                currentDisplay = 0;
            if ((1 == systemParam.ledDisplayParam[currentDisplay].isEnable) && (1 == systemFlag.updateLedDisplay[currentDisplay]))
            {
                //intToStr((int) systemParam.ledDisplayParam[currentDisplay].brightness, dummyStr, 2);
                dummyStr[0] = 0x00;
                dummyStr[1] = systemParam.ledDisplayParam[currentDisplay].brightness;
                strcpy(&dummyStr[2], systemParam.ledDisplayParam[currentDisplay].lastValue);
                ModbusPresetMultipleRegisters((currentDisplay + 1), 0x0000, 0x0003, dummyStr);
                systemFlag.isConnected[currentDisplay] = 0;
                ledTicksMs = MULTITASKER_GET_TICK();
                MAIN_APP_SM = STEP2;
            }
            else
            {
                currentDisplay++;
            }
            break;

        case STEP2:
            if (ModbusIsAvailable() > 4) //minimum response is greater than 4 bytes
            {
                //check received packet
                systemFlag.updateLedDisplay[currentDisplay] = 0;
                systemFlag.isConnected[currentDisplay] = 1;
                MAIN_APP_SM = STEP3;
                break;
            }
            if ((MULTITASKER_GET_TICK() - ledTicksMs) > 1000)
            {
                MAIN_APP_SM = STEP3;
                break;
            }
            break;

        case STEP3:
            ledTicksMs = MULTITASKER_GET_TICK();
            MAIN_APP_SM = STEP4;
            break;

        case STEP4:
            if ((MULTITASKER_GET_TICK() - ledTicksMs) > 100)
            {
                currentDisplay++;
                MAIN_APP_SM = STEP1;
                break;
            }
            break;

        case STEP5:
            break;

        case STEP6:
            break;

        case STEP7:
            break;

        case STEP8:
            break;

        case STEP9:
            break;

        default:
            MAIN_APP_SM = STEP1;
            break;
        }
#endif
        RunRrMultiTasker();


        // This task performs normal stack task including checking
        // for incoming packet, type of packet and calling
        // appropriate stack entity to process it.
        StackTask();

        // This tasks invokes each of the core stack application tasks
        StackApplications();

        // Process application specific tasks here.
        // For this demo app, this will include the Generic TCP 
        // client and servers, and the SNMP, Ping, and SNMP Trap
        // demos.  Following that, we will process any IO from
        // the inputs on the board itself.
        // Any custom modules or processing you need to do should
        // go here.
        //generictcpclient() handles serial commands in it
#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)
        GenericTCPClient();
        //this function takes the data from tcp
#endif

#if defined(STACK_USE_GENERIC_TCP_SERVER_EXAMPLE)
        GenericTCPServer();
#endif

#if defined(STACK_USE_ICMP_CLIENT)
        PingDemo();
#endif

#if defined(STACK_USE_BERKELEY_API)
        BerkeleyTCPClientDemo();
        BerkeleyTCPServerDemo();
        BerkeleyUDPClientDemo();
#endif

#if defined (STACK_USE_EMS_FTP_CLIENT)
        //if(TRUE == ftpcRunning)
        FTPClient();
        //        else
        if (systemFlag.SearchFirmware)
        {
            //                FTPC_SendInit(91,1,FTPC_DOWNLOAD);
            systemFlag.SearchFirmware = 0;
            strcpy(ftpParam.ftpServerName, "188.166.228.28");
            strcpy(ftpParam.ftpUserName, "ftpuser");
            strcpy(ftpParam.ftpUserPass, "ftpuser1234");
            //            strcpy(ftpParam.ftpServerName, "192.168.1.103");
            //            strcpy(ftpParam.ftpUserName, "umesh");
            //            strcpy(ftpParam.ftpUserPass, "umesh");
            strcpy(ftpParam.ftpRemoteFilePath, "//");
            strcpy(ftpParam.ftpRemoteFileName, "upload1.txt");
            strcpy(ftpParam.ftpLocalFileName, "image1.hex");
            ftpParam.ftpPort = 21;
            ftpParam.ftpFileMode = OVERWRITE;

            FTPC_DownloadFileToSdCard(&ftpParam);
            //FTPC_DownLoadToArray(&ftpParam, ftpDataBuffer, strlen(ftpDataBuffer));
        }
#endif

        ProcessIO(); //nothing is written in this function
        //monitoring of Inputs can be done here
        //monitor fire switch, exit switch etc.


        // If the local IP address has changed (ex: due to DHCP lease change)
        // write the new IP address to the LCD display, UART, and Announce 
        // service
        if (dwLastIP != AppConfig.MyIPAddr.Val)
        {
            dwLastIP = AppConfig.MyIPAddr.Val;
            DisplayIPValue(AppConfig.MyIPAddr);
#if defined(STACK_USE_ANNOUNCE)
            AnnounceIP();
#endif
        }

#if defined(USE_GLCD)
        if ((MULTITASKER_GET_TICK() - updateDispTimeMs) > 500)
        {
            updateDispTimeMs = MULTITASKER_GET_TICK();
            if (0 == userCardContFlashedFlag)
            {
                //update graphic LCD

            }
        }
#endif

#if defined(CHECK_SERVER_FLAG)
        if ((MULTITASKER_GET_TICK() - checkServerTick) > 5000)
        {
            checkServerTick = MULTITASKER_GET_TICK();
            //call function to check server connectivity
            //
        }
#endif


    }
}

// Writes an IP address to the LCD display and the UART as available

void DisplayIPValue(IP_ADDR IPVal)
{
    // printf("%u.%u.%u.%u", IPVal.v[0], IPVal.v[1], IPVal.v[2], IPVal.v[3]);

    BYTE IPDigit[4];

    BYTE i;

    //display where?
    PcCommPutS("\r\nMyIP: ");
    for (i = 0; i < sizeof (IP_ADDR); i++)
    {
        uitoa((WORD) IPVal.v[i], IPDigit);
        PcCommPutS(IPDigit);
        if (i == sizeof (IP_ADDR) - 1)
            break;
        else
            PcCommPutCh('.');
    }
    PcCommPutS("\r\n");
}

static void ProcessIO(void)
{
    char dummyStr[200];

    if (1 == systemFlag.pcCommCmdRxd)
    {
        DelayMs(100);
        if (PcCommGetS(dummyStr) > 0)
        {
            PcCommExecuteCmd(dummyStr, dummyStr);
            if (dummyStr[0] != 0)
                PcCommPutS(dummyStr);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="command response to log data parameter">
    if (1 == systemFlag.setDefLFile)
    {
        if (1 == AppMakeDefaultLogFile())
        {
            PcCommPutS("OK");
        }
        else
        {
            PcCommPutS("ERROR");
        }
        PcCommPutS("\r\n");
        systemFlag.setDefLFile = 0;
    }

    if (1 == systemFlag.setDefPFile)
    {
        if (1 == AppMakeDefaultSystemParamFile())
        {
            AppGetSystemParamFrmFile(&systemParam);
            PcCommPutS("OK");
            //nwkRegFlag = 0;  //this is required for getting right APN
            //            HexStrToAsciiStr(AppConfig.MyMACAddr.v, systemParam.myMacAddInStr, MY_ADDRESS_LENGTH);
        }
        else
        {
            PcCommPutS("ERROR");
        }
        PcCommPutS("\r\n");
        systemFlag.setDefPFile = 0;
    }

    if (1 == systemFlag.setDefNFile)
    {
        if (1 == AppMakeDefaultNetSystemParamFile())
        {
            AppGetNetSystemParamFromFile(&AppConfig);
            PcCommPutS("OK");
            //nwkRegFlag = 0;  //this is required for getting right APN
        }
        else
        {
            PcCommPutS("ERROR");
        }
        PcCommPutS("\r\n");
        systemFlag.setDefNFile = 0;
        DelayMs(50);
        Reset(); //this is required to take effect of net parameters
    }
    //</editor-fold>
}

/****************************************************************************
  Function:
    static void InitializeBoard(void)

  Description:
    This routine initializes the hardware.  It is a generic initialization
    routine, using definitions in HardwareProfile.h to determine specific 
    initialization.

  Precondition:
    None

  Parameters:
    None - None

  Returns:
    None

  Remarks:
    None
 ***************************************************************************/
static void InitializeBoard(void)
{
    volatile unsigned int cnt;
    // Enable multi-vectored interrupts
    INTEnableSystemMultiVectoredInt();

    // Enable optimal performance
    SYSTEMConfigPerformance(GetSystemClock());
    mOSCSetPBDIV(OSC_PB_DIV_1); // Use 1:1 CPU Core:Peripheral clocks

    TRISA = 0x00;
    TRISB = 0x00;
    TRISC = 0x00;
    TRISD = 0x00;
    TRISE = 0x00;
    TRISF = 0x00;
    DelayMs(500);

    _LATD1 = 0;
    _TRISD1 = 0; // buzzer

    INPUT_1_DIR = 1;
    INPUT_2_DIR = 1;
    INPUT_3_DIR = 1;
    INPUT_4_DIR = 1;
    OUTPUT_1_DIR = 0;
    OUTPUT_2_DIR = 0;
    OUTPUT_3_DIR = 0;
    OUTPUT_4_DIR = 0;

    // Use Timer 1 for 16-bit and 32-bit processors
    // 1:256 prescale
    T2CONbits.TCKPS = 3;
    // Base
    PR2 = 10000; // for 1mSec
    // Clear counter
    TMR2 = 0;
    // Enable timer interrupt
    IPC2bits.T2IP = 2; // Interrupt priority 2 (low)
    IFS0CLR = _IFS0_T2IF_MASK;
    IEC0SET = _IEC0_T2IE_MASK;
    // Start timer
    T2CONbits.TON = 1;

    PcCommPortOpen(115200, 300);

    DSTATUS dstatus;
    dstatus = disk_initialize(DEV_MMC); //SD card init
    if (dstatus & STA_NOINIT)
    {
        Nop();
        Nop();
        PcCommPutS("SD/MMC failed to Initialize\r\n");
        //        while (1);
        DelayMs(5000);
        Reset();
    }

    //if (FR_OK != f_mount(0, &Fatfs))
    if (FR_OK != f_mount(&Fatfs, _SD_, 0)) // Mode option 0:Do not mount (delayed mount), 1:Mount immediately
    {
        Nop();
        Nop();
        PcCommPutS("SD/MMC FAT failed to Initialize\r\n");
        //        while (1);
        DelayMs(5000);
        Reset();
    }
    //    systemFlag.currSdCardStat = 1;
    //    systemFlag.prevSdCardStat = 1;

#if defined(ENC100_CS_TRIS)
    ENC100_CS_IO = (ENC100_INTERFACE_MODE == 0);
    ENC100_CS_TRIS = 0;
#endif

    //
    // Init I2C
    //
    CloseI2C1();
    DisableIntSI2C1; //Disable I2C interrupt
    OpenI2C1(I2C_ON | I2C_SLW_DIS, 247);
    //    RTCInit();


    AppInit();
    //    EmrwOpen(9600, 2000); //initialize mifare read write module on uart2
    ModbusInit(9600, 100);
    DelayMs(500);
    Nop();
    //    ModbusPutS("uart1 ");
    //    AppMakeDefaultLogFile();
    //initialize buzzer pins here 
    //test beep 

    //    _LATD1 = 1;
    //    DelayMs(125);
    //    _LATD1 = 0;
    for (cnt = 0; cnt < 379; cnt++)
    {
        _LATD1 ^= 1;
        Delay10us(33);
    }
    //                _LATD1 = 1;
    //                DelayMs(125);
    _LATD1 = 0;
}

/*********************************************************************
 * Function:        void InitAppConfig(void)
 *
 * PreCondition:    MPFSInit() is already called.
 *
 * Input:           None
 *
 * Output:          Write/Read non-volatile config variables.
 *
 * Side Effects:    None
 *
 * Overview:        None
 *
 * Note:            None
 ********************************************************************/
// MAC Address Serialization using a MPLAB PM3 Programmer and 
// Serialized Quick Turn Programming (SQTP). 
// The advantage of using SQTP for programming the MAC Address is it
// allows you to auto-increment the MAC address without recompiling 
// the code for each unit.  To use SQTP, the MAC address must be fixed
// at a specific location in program memory.  Uncomment these two pragmas
// that locate the MAC address at 0x1FFF0.  Syntax below is for MPLAB C 
// Compiler for PIC18 MCUs. Syntax will vary for other compilers.
//#pragma romdata MACROM=0x1FFF0
static ROM BYTE SerializedMACAddress[6] = {MY_DEFAULT_MAC_BYTE1, MY_DEFAULT_MAC_BYTE2, MY_DEFAULT_MAC_BYTE3, MY_DEFAULT_MAC_BYTE4, MY_DEFAULT_MAC_BYTE5, MY_DEFAULT_MAC_BYTE6};
//#pragma romdata

static void InitAppConfig(void)
{
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int status;

    res = f_open(&fsrc, "1:NetParam.txt", FA_OPEN_EXISTING | FA_READ);
    if (res != FR_OK)
    {
        //
        //  we reach here if file not found
        //
        status = AppMakeDefaultNetSystemParamFile();
        if (0 == status)
        {
            Nop();
        }
    }
    else
    {
        res = f_close(&fsrc);
        if (res != FR_OK)
        {
            Nop();
        }
        AppGetNetSystemParamFromFile(&AppConfig);
    }


    DisplayIPValue(AppConfig.MyIPAddr);

}

unsigned int AppMakeDefaultNetSystemParamFile(void)
{
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int size, numOfBytesWritten;
    //APP_CONFIG AppConfig;


    while (1)
    {
        // Start out zeroing all AppConfig bytes to ensure all fields are 
        // deterministic for checksum generation
        memset((void*) &AppConfig, 0x00, sizeof (AppConfig));

        AppConfig.Flags.bIsDHCPEnabled = TRUE;
        AppConfig.Flags.bInConfigMode = TRUE;
        memcpypgm2ram((void*) &AppConfig.MyMACAddr, (ROM void*) SerializedMACAddress, sizeof (AppConfig.MyMACAddr));
        //        {
        //            _prog_addressT MACAddressAddress;
        //            MACAddressAddress.next = 0x157F8;
        //            _memcpy_p2d24((char*)&AppConfig.MyMACAddr, MACAddressAddress, sizeof(AppConfig.MyMACAddr));
        //        }
        AppConfig.MyIPAddr.Val = MY_DEFAULT_IP_ADDR_BYTE1 | MY_DEFAULT_IP_ADDR_BYTE2 << 8ul | MY_DEFAULT_IP_ADDR_BYTE3 << 16ul | MY_DEFAULT_IP_ADDR_BYTE4 << 24ul;
        AppConfig.DefaultIPAddr.Val = AppConfig.MyIPAddr.Val;
        AppConfig.MyMask.Val = MY_DEFAULT_MASK_BYTE1 | MY_DEFAULT_MASK_BYTE2 << 8ul | MY_DEFAULT_MASK_BYTE3 << 16ul | MY_DEFAULT_MASK_BYTE4 << 24ul;
        AppConfig.DefaultMask.Val = AppConfig.MyMask.Val;
        AppConfig.MyGateway.Val = MY_DEFAULT_GATE_BYTE1 | MY_DEFAULT_GATE_BYTE2 << 8ul | MY_DEFAULT_GATE_BYTE3 << 16ul | MY_DEFAULT_GATE_BYTE4 << 24ul;
        AppConfig.PrimaryDNSServer.Val = MY_DEFAULT_PRIMARY_DNS_BYTE1 | MY_DEFAULT_PRIMARY_DNS_BYTE2 << 8ul | MY_DEFAULT_PRIMARY_DNS_BYTE3 << 16ul | MY_DEFAULT_PRIMARY_DNS_BYTE4 << 24ul;
        AppConfig.SecondaryDNSServer.Val = MY_DEFAULT_SECONDARY_DNS_BYTE1 | MY_DEFAULT_SECONDARY_DNS_BYTE2 << 8ul | MY_DEFAULT_SECONDARY_DNS_BYTE3 << 16ul | MY_DEFAULT_SECONDARY_DNS_BYTE4 << 24ul;

        // Load the default NetBIOS Host Name
        memcpypgm2ram(AppConfig.NetBIOSName, (ROM void*) MY_DEFAULT_HOST_NAME, 16);
        FormatNetBIOSName(AppConfig.NetBIOSName);

        // Compute the checksum of the AppConfig defaults as loaded from ROM
        wOriginalAppConfigChecksum = CalcIPChecksum((BYTE*) & AppConfig, sizeof (AppConfig));

        break;
    }
    //DisplayIPValue(AppConfig->MyIPAddr);

    res = f_open(&fsrc, "1:NetParam.txt", FA_CREATE_ALWAYS | FA_WRITE);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    size = sizeof (APP_CONFIG);
    res = f_write(&fsrc, (void*) &AppConfig, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 0;
    }

    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    return 1;
}

unsigned int AppGetNetSystemParamFromFile(APP_CONFIG *Appconfig)
{
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int size, numOfBytesRead;

    res = f_open(&fsrc, "1:NetParam.txt", FA_OPEN_EXISTING | FA_READ);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    size = sizeof (APP_CONFIG);
    res = f_read(&fsrc, Appconfig, size, &numOfBytesRead);
    if ((res != FR_OK) || (numOfBytesRead != size))
    {
        f_close(&fsrc);
        return 0;
    }

    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    return 1;
}

unsigned int AppSaveNetSystemParamToFile(APP_CONFIG *Appconfig)
{
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int size, numOfBytesWritten;

    res = f_open(&fsrc, "1:NetParam.txt", FA_CREATE_ALWAYS | FA_WRITE);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }
    //DisplayIPValue(AppConfig->MyIPAddr);
    size = sizeof (APP_CONFIG);
    res = f_write(&fsrc, Appconfig, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 0;
    }

    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    return 1;
}

void __attribute((interrupt(ipl2), vector(_TIMER_2_VECTOR), nomips16)) _T2Interrupt(void)
{
    static unsigned int cnt = 0;

    // Reset interrupt flag
    IFS0bits.T2IF = 0;
    cnt++;
    MULTITASKER_TICK_INC();
    if (cnt >= 1000)
    {
        cnt = 0;
        MULTITASKER_SECONDS_TICK_INC();
    }
    disk_timerproc();
}

//******************************************************************************
//******************************************************************************
// USB Support Functions
//******************************************************************************
//******************************************************************************

/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event,
                void *data, DWORD size )

  Summary:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.

  Description:
    This is the application event handler.  It is called when the stack has
    an event that needs to be handled by the application layer rather than
    by the client driver.  If the application is able to handle the event, it
    returns TRUE.  Otherwise, it returns FALSE.

  Precondition:
    None

  Parameters:
    BYTE address    - Address of device where event occurred
    USB_EVENT event - Identifies the event that occured
    void *data      - Pointer to event-specific data
    DWORD size      - Size of the event-specific data

  Return Values:
    TRUE    - The event was handled
    FALSE   - The event was not handled

  Remarks:
    The application may also implement an event handling routine if it
    requires knowledge of events.  To do so, it must implement a routine that
    matches this function signature and define the USB_HOST_APP_EVENT_HANDLER
    macro as the name of that function.
 ***************************************************************************/

BOOL USB_ApplicationEventHandler(BYTE address, USB_EVENT event, void *data, DWORD size)
{
#ifndef MINIMUM_BUILD
    switch (event)
    {
    case EVENT_VBUS_REQUEST_POWER:
        // The data pointer points to a byte that represents the amount of power
        // requested in mA, divided by two.  If the device wants too much power,
        // we reject it.
        //if (((USB_VBUS_POWER_EVENT_DATA*) data)->current <= (100 / 2))
        //{
        return TRUE;
        //}
        //else
        //{
        //    PcCommPutS("\r\n***** USB Error - device requires too much current *****\r\n");
        //}
        break;

    case EVENT_VBUS_RELEASE_POWER:
        // Turn off Vbus power.
        // The PIC24F with the Explorer 16 cannot turn off Vbus through software.

        //This means that the device was removed
        deviceAttached = FALSE;
        writeOnce = 0;
        return TRUE;
        break;

    case EVENT_HUB_ATTACH:
        PcCommPutS("\r\n***** USB Error - hubs are not supported *****\r\n");

        return TRUE;
        break;

    case EVENT_UNSUPPORTED_DEVICE:
        PcCommPutS("\r\n***** USB Error - device is not supported *****\r\n");
        PcCommPutS("   If the device is a thumb drive, it probably uses a\r\n");
        PcCommPutS("   transport protocol other than SCSI, such as SFF-8070i.\r\n");
        return TRUE;
        break;

    case EVENT_CANNOT_ENUMERATE:
        PcCommPutS("\r\n***** USB Error - cannot enumerate device *****\r\n");
        return TRUE;
        break;

    case EVENT_CLIENT_INIT_ERROR:
        PcCommPutS("\r\n***** USB Error - client driver initialization error *****\r\n");
        return TRUE;
        break;

    case EVENT_OUT_OF_MEMORY:
        PcCommPutS("\r\n***** USB Error - out of heap memory *****\r\n");
        return TRUE;
        break;

    case EVENT_UNSPECIFIED_ERROR: // This should never be generated.
        PcCommPutS("\r\n***** USB Error - unspecified *****\r\n");
        return TRUE;
        break;
    default:
        break;

    }
#endif

    return FALSE;
}



//******************************************************************************
//******************************************************************************
// Internal Functions
//******************************************************************************
//******************************************************************************

#if defined(EEPROM_CS_TRIS) || defined(SPIFLASH_CS_TRIS)

void SaveAppConfig(const APP_CONFIG *ptrAppConfig)
{
    NVM_VALIDATION_STRUCT NVMValidationStruct;

    // Ensure adequate space has been reserved in non-volatile storage to 
    // store the entire AppConfig structure.  If you get stuck in this while(1) 
    // trap, it means you have a design time misconfiguration in TCPIPConfig.h.
    // You must increase MPFS_RESERVE_BLOCK to allocate more space.
#if defined(STACK_USE_MPFS2)
    if (sizeof (NVMValidationStruct) + sizeof (AppConfig) > MPFS_RESERVE_BLOCK)
        while (1);
#endif

    // Get proper values for the validation structure indicating that we can use 
    // these EEPROM/Flash contents on future boot ups
    NVMValidationStruct.wOriginalChecksum = wOriginalAppConfigChecksum;
    NVMValidationStruct.wCurrentChecksum = CalcIPChecksum((BYTE*) ptrAppConfig, sizeof (APP_CONFIG));
    NVMValidationStruct.wConfigurationLength = sizeof (APP_CONFIG);

    // Write the validation struct and current AppConfig contents to EEPROM/Flash
#if defined(EEPROM_CS_TRIS)
    XEEBeginWrite(0x0000);
    XEEWriteArray((BYTE*) & NVMValidationStruct, sizeof (NVMValidationStruct));
    XEEWriteArray((BYTE*) ptrAppConfig, sizeof (APP_CONFIG));
#else
    SPIFlashBeginWrite(0x0000);
    SPIFlashWriteArray((BYTE*) & NVMValidationStruct, sizeof (NVMValidationStruct));
    SPIFlashWriteArray((BYTE*) ptrAppConfig, sizeof (APP_CONFIG));
#endif
}
#endif