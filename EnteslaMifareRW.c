
#include <xc.h>
#include <string.h>
#include <stdlib.h>
#include <uart.h>
#include <plib.h> 
#include "GenericTypeDefs.h"
#include "Compiler.h"
#include "delay.h"
#include "EnteslaMifareRW.h"
#include "multitasker.h"
#include "CircularBuffer.h"
#include "helper.h"
#include "global_vars.h"

#define U2RX_Clear_Intr_Status_Bit IFS1bits.U2RXIF=0;
#define U2TX_Clear_Intr_Status_Bit IFS1bits.U2TXIF=0;

typedef struct
{
    EMRW_COMMAND cmd;
    unsigned int responseSize;
} EMRW_CMD_INFO;

EMRW_COMMAND emrwLastExecCmd = EMRW_CMD_NONE;
static EMRW_CMD_INFO cmdInfo[] = {
    { EMRW_CMD_TR, 3},
    { EMRW_CMD_RS, 13},
    { EMRW_CMD_CR, 0},
    { EMRW_CMD_SL, 7},
    { EMRW_CMD_AU, 3},
    { EMRW_CMD_LO, 3},
    { EMRW_CMD_RBK, 23},
    { EMRW_CMD_RBE, 23},
    { EMRW_CMD_RBA, 18},
    { EMRW_CMD_WBK, 7},
    { EMRW_CMD_WBE, 7},
    { EMRW_CMD_WBA, 3},
    { EMRW_CMD_BD, 3},
    { EMRW_CMD_PD, 3},
    { EMRW_CMD_SR, 23},
    { EMRW_CMD_LS, 3}
};

char keyA[13] = "FFFFFFFFFFFF";
static TICKS _uart2TimeoutMs = 10, _timeoutMs;

static CIRCULAR_BUFFER _uart2RxBuff, _uart2TxBuff;

//static unsigned int _Uart2GetCh(char *pCh);
static unsigned int _Uart2PutCh(unsigned char ch);
static unsigned int _Uart2GetS(char *str);

//static unsigned char CmdStr[64]={0};

// timer module needs to be initialized before calling this function

unsigned char EmrwOpen(unsigned long baudRateToSet, unsigned long timeoutMs)
{
    EMRW_CMD_PARAM cmdParam;
    unsigned int status;
    unsigned long closestSpbrgValue;

    TRISFbits.TRISF5 = 0;
    TRISFbits.TRISF4 = 1;

    InitQueue(&_uart2RxBuff);
    InitQueue(&_uart2TxBuff);

    _uart2TimeoutMs = timeoutMs;

    //    CloseUART2();
    //    //disable UART if enabled previously
    //    U2TX_Clear_Intr_Status_Bit;
    //
    //    ConfigIntUART2(UART_RX_INT_EN | UART_RX_INT_PR6 | UART_TX_INT_EN | UART_TX_INT_PR6);
    //    OpenUART2(UART_EN | UART_BRGH_FOUR, UART_TX_ENABLE, u2brg); //configure UART and enable it
    U2MODE = 0x8000; // Set UARTEN.  Note: this must be done before setting UTXEN
    IPC8bits.U2IP = 6; // Priority level 6
    U2STA = 0x00001400; // RXEN set, TXEN set
    closestSpbrgValue = ((GetPeripheralClock() + 8 * baudRateToSet) / 16 / baudRateToSet - 1);
    U2BRG = closestSpbrgValue;
    IEC1bits.U2RXIE = 1;

    _timeoutMs = MULTITASKER_GET_TICK();

    //    while (1)
    //        _Uart2PutCh('U');

    cmdParam.cmd = EMRW_CMD_TR;
    status = EmrwExecCmd(&cmdParam);
    if (0 == status)
    {
        Nop();
        return 0;
    }
    EmrwPutS("SL\r");
    //    char data[] = "1234567890123456";
    //    char uid[16];
    //    cmdParam.cmd = EMRW_CMD_WBE;
    //    cmdParam.keyType = KEY_A;
    //    cmdParam.eepromLocation = 0;
    //    cmdParam.blockNum = 2;
    //    cmdParam.pData = data;
    //    cmdParam.pUid = uid;
    //    status = EmrwExecCmd(&cmdParam);
    //    if (0 == status)
    //    {
    //        Nop();
    //        return 0;
    //    }

    //    cmdParam.cmd = EMRW_CMD_LO;
    //    cmdParam.keyType = KEY_A;
    //    cmdParam.eepromLocation = 0;
    //    cmdParam.pKey = keyA;
    //    status = EmrwExecCmd(&cmdParam);
    //    if (0 == status)
    //    {
    //        Nop();
    //        return 0;
    //    }
    //
    //    cmdParam.cmd = EMRW_CMD_LS;
    //    cmdParam.keyType = KEY_A;
    //    cmdParam.eepromLocation = 0;
    //    cmdParam.blockNum = 1;
    //    status = EmrwExecCmd(&cmdParam);
    //    if (0 == status)
    //    {
    //        Nop();
    //        return 0;
    //    }
    //
    //    cmdParam.cmd = EMRW_CMD_SR;
    //    status = EmrwExecCmd(&cmdParam);
    //    return status;
}

//
//  return 0 -> fail, return 1 -> success
//

unsigned int EmrwExecCmd(EMRW_CMD_PARAM *cmdParam)
{
    unsigned int i, size, responseSize;
    char responseStr[25];

    size = sizeof (cmdInfo) / sizeof (EMRW_CMD_INFO);

    for (i = 0; i < size; i++)
    {
        if (cmdParam->cmd == cmdInfo[i].cmd)
        {
            Nop();
            break;
        }
    }

    if (i >= size)
    {
        return 0;
    }
    responseSize = cmdInfo[i].responseSize;

    switch (cmdInfo[i].cmd)
    {
    case EMRW_CMD_TR:
        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);

        _timeoutMs = MULTITASKER_GET_TICK();

        _Uart2PutCh('0');
        _Uart2PutCh(13);

        while (3 != _uart2RxBuff.current)
        {
            if ((MULTITASKER_GET_TICK() - _timeoutMs) > _uart2TimeoutMs)
            {
                ClearQueue(&_uart2RxBuff);
                ClearQueue(&_uart2TxBuff);
                return 0;
            }
        }

        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);
        emrwLastExecCmd = EMRW_CMD_TR;
        return 1;

        break;

    case EMRW_CMD_RS:

        break;

    case EMRW_CMD_CR:

        break;

    case EMRW_CMD_SL:

        break;

    case EMRW_CMD_AU:

        break;

    case EMRW_CMD_LO:

        if (cmdParam->eepromLocation > 15)
        {
            return 0;
        }

        //            if(cmdParam->blockNum > 255)
        //            {
        //                return 0;
        //            }

        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);

        PushToQueueS(&_uart2TxBuff, "LO");

        if (cmdParam->keyType == KEY_A)
        {
            PushToQueue(&_uart2TxBuff, 'A');
        }
        else if (cmdParam->keyType == KEY_B)
        {
            PushToQueue(&_uart2TxBuff, 'B');
        }
        else
        {
            return 0;
        }

        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->eepromLocation/10);
        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->eepromLocation%10);
        PushToQueue(&_uart2TxBuff, ConvertToHexHigherNibble(cmdParam->eepromLocation));
        PushToQueue(&_uart2TxBuff, ConvertToHexLowerNibble(cmdParam->eepromLocation));

        PushToQueueS(&_uart2TxBuff, cmdParam->pKey);

        PushToQueue(&_uart2TxBuff, '\r');

        _timeoutMs = MULTITASKER_GET_TICK();

        EnableIntU2TX;
        IFS1bits.U2TXIF = 1;

        while (_uart2RxBuff.current != responseSize)
        {
            if ((MULTITASKER_GET_TICK() - _timeoutMs) > _uart2TimeoutMs)
            {
                ClearQueue(&_uart2RxBuff);
                ClearQueue(&_uart2TxBuff);
                return 0;
            }
        }

        _Uart2GetS(responseStr);
        if ('S' == responseStr[0])
        {
            ClearQueue(&_uart2RxBuff);
            ClearQueue(&_uart2TxBuff);
            emrwLastExecCmd = EMRW_CMD_LO;
            return 1;
        }
        else
        {
            ClearQueue(&_uart2RxBuff);
            ClearQueue(&_uart2TxBuff);
            return 0;
        }
        break;

    case EMRW_CMD_RBE:
        if (cmdParam->eepromLocation > 15)
        {
            return 0;
        }

        //            if(cmdParam->blockNum > 255)
        //            {
        //                return 0;
        //            }

        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);

        PushToQueueS(&_uart2TxBuff, "RBE");

        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->blockNum/10);
        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->blockNum%10);
        PushToQueue(&_uart2TxBuff, ConvertToHexHigherNibble(cmdParam->blockNum));
        PushToQueue(&_uart2TxBuff, ConvertToHexLowerNibble(cmdParam->blockNum));

        if (cmdParam->keyType == KEY_A)
        {
            PushToQueue(&_uart2TxBuff, 'A');
        }
        else if (cmdParam->keyType == KEY_B)
        {
            PushToQueue(&_uart2TxBuff, 'B');
        }
        else
        {
            return 0;
        }

        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->eepromLocation/10);
        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->eepromLocation%10);
        PushToQueue(&_uart2TxBuff, ConvertToHexHigherNibble(cmdParam->eepromLocation));
        PushToQueue(&_uart2TxBuff, ConvertToHexLowerNibble(cmdParam->eepromLocation));

        PushToQueue(&_uart2TxBuff, '\r');

        _timeoutMs = MULTITASKER_GET_TICK();

        EnableIntU2TX;
        IFS1bits.U2TXIF = 1;

        while (_uart2RxBuff.current != responseSize)
        {
            if ((MULTITASKER_GET_TICK() - _timeoutMs) > _uart2TimeoutMs)
            {
                ClearQueue(&_uart2RxBuff);
                ClearQueue(&_uart2TxBuff);
                return 0;
            }
        }

        _Uart2GetS(responseStr);

        cmdParam->cardType = responseStr[0];

        for (i = 0; i < 4; i++)
        {
            cmdParam->pUid[i] = responseStr[i + 1];
        }
        cmdParam->pUid[4] = 0;

        for (i = 0; i < 16; i++)
        {
            cmdParam->pData[i] = responseStr[i + 5];
        }
        cmdParam->pData[16] = '\0';

        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);
        emrwLastExecCmd = EMRW_CMD_RBE;
        return 1;
        break;

    case EMRW_CMD_RBK:

        break;

    case EMRW_CMD_RBA:
        break;

    case EMRW_CMD_WBK:
        break;

    case EMRW_CMD_WBE:
        if (cmdParam->eepromLocation > 15)
        {
            return 0;
        }

        //            if(cmdParam->blockNum > 255)
        //            {
        //                return 0;
        //            }

        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);

        PushToQueueS(&_uart2TxBuff, "WBE");

        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->blockNum/10);
        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->blockNum%10);
        PushToQueue(&_uart2TxBuff, ConvertToHexHigherNibble(cmdParam->blockNum));
        PushToQueue(&_uart2TxBuff, ConvertToHexLowerNibble(cmdParam->blockNum));

        if (cmdParam->keyType == KEY_A)
        {
            PushToQueue(&_uart2TxBuff, 'A');
        }
        else if (cmdParam->keyType == KEY_B)
        {
            PushToQueue(&_uart2TxBuff, 'B');
        }
        else
        {
            return 0;
        }

        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->eepromLocation/10);
        //            PushToQueue(&_uart2TxBuff, '0' + cmdParam->eepromLocation%10);
        PushToQueue(&_uart2TxBuff, ConvertToHexHigherNibble(cmdParam->eepromLocation));
        PushToQueue(&_uart2TxBuff, ConvertToHexLowerNibble(cmdParam->eepromLocation));

        for (i = 0; i < 16; i++)
        {
            PushToQueue(&_uart2TxBuff, cmdParam->pData[i]);
        }

        PushToQueue(&_uart2TxBuff, '\r');

        _timeoutMs = MULTITASKER_GET_TICK();

        EnableIntU2TX;
        IFS1bits.U2TXIF = 1;

        while (_uart2RxBuff.current != responseSize)
        {
            if ((MULTITASKER_GET_TICK() - _timeoutMs) > _uart2TimeoutMs)
            {
                ClearQueue(&_uart2RxBuff);
                ClearQueue(&_uart2TxBuff);
                return 0;
            }
        }

        _Uart2GetS(responseStr);

        //        PushToQueue(&_uart2TxBuff, '\r');
        //        EnableIntU2TX;
        //        IFS1bits.U2TXIF = 1;

        cmdParam->cardType = responseStr[0];

        for (i = 0; i < 4; i++)
        {
            cmdParam->pUid[i] = responseStr[i + 1];
        }
        cmdParam->pUid[4] = 0;

        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);
        emrwLastExecCmd = EMRW_CMD_WBE;
        return 1;
        break;

    case EMRW_CMD_WBA:
        break;

    case EMRW_CMD_BD:
        break;

    case EMRW_CMD_PD:
        break;

    case EMRW_CMD_SR:
        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);

        _timeoutMs = MULTITASKER_GET_TICK();

        _Uart2PutCh('0');
        _Uart2PutCh(13);

        while (3 != _uart2RxBuff.current)
        {
            if ((MULTITASKER_GET_TICK() - _timeoutMs) > _uart2TimeoutMs)
            {
                ClearQueue(&_uart2RxBuff);
                ClearQueue(&_uart2TxBuff);
                return 0;
            }
        }

        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);

        PushToQueueS(&_uart2TxBuff, "SR");

        PushToQueue(&_uart2TxBuff, '\r');

        EnableIntU2TX;
        IFS1bits.U2TXIF = 1;

        emrwLastExecCmd = EMRW_CMD_SR;
        return 1;
        break;

    case EMRW_CMD_LS:
        ClearQueue(&_uart2RxBuff);
        ClearQueue(&_uart2TxBuff);

        PushToQueueS(&_uart2TxBuff, "LS");

        if (cmdParam->keyType == KEY_A)
        {
            PushToQueue(&_uart2TxBuff, 'A');
        }
        else if (cmdParam->keyType == KEY_B)
        {
            PushToQueue(&_uart2TxBuff, 'B');
        }
        else
        {
            return 0;
        }

        PushToQueue(&_uart2TxBuff, '0' + cmdParam->eepromLocation / 10);
        PushToQueue(&_uart2TxBuff, '0' + cmdParam->eepromLocation % 10);

        PushToQueue(&_uart2TxBuff, '0' + cmdParam->blockNum / 10);
        PushToQueue(&_uart2TxBuff, '0' + cmdParam->blockNum % 10);


        PushToQueue(&_uart2TxBuff, '\r');

        _timeoutMs = MULTITASKER_GET_TICK();

        EnableIntU2TX;
        IFS1bits.U2TXIF = 1;

        while (_uart2RxBuff.current != responseSize)
        {
            if ((MULTITASKER_GET_TICK() - _timeoutMs) > _uart2TimeoutMs)
            {
                ClearQueue(&_uart2RxBuff);
                ClearQueue(&_uart2TxBuff);
                return 0;
            }
        }

        _Uart2GetS(responseStr);

        if ('S' == responseStr[0])
        {
            ClearQueue(&_uart2RxBuff);
            ClearQueue(&_uart2TxBuff);
            emrwLastExecCmd = EMRW_CMD_LS;
            return 1;
        }
        break;

    default:
        break;
    }

    return 0;
}

void EmrwClose(void)
{
    CloseUART2();
    return;
}

void EmrwSetTimeout(unsigned long timeout)
{
    _uart2TimeoutMs = timeout;
}

unsigned long EmrwGetTimeout(void)
{
    return _uart2TimeoutMs;
}

void EmrwEnableInterruptReception(unsigned int expectedCnt)
{
    //
    //  Turn ON Uart1 RX Interrupt
    //
    //    PIR1bits.RCIF = 0;
    //    PIE1bits.RC1IE = 1;
    //    RCSTAbits.CREN = 1;
    //    EmrwBuffer.expectedCnt = expectedCnt; // Expected Packet Size    
}

void EmrwDisableInterruptReception(unsigned int expectedCnt)
{
    //
    //  Turn OFF Uart1 RX Interrupt
    //
    //    PIE1bits.RC1IE = 0;
    //    EmrwBuffer.expectedCnt = expectedCnt; // Expected Packet Size    
}

unsigned char EmrwEnableBackgrndRead(unsigned int expectedCnt)
{
    //    EmrwBuffer.byteRxdFlag     = 0;
    //    EmrwBuffer.packetRxdFlag   = 0;
    //    EmrwBuffer.timeoutFlag     = 0;
    //    EmrwBuffer.lastRxdByte     = 0;
    //    EmrwBuffer.bufCnt          = 0;
    //    EmrwBuffer.cntr            = 0;    
    //    EmrwBuffer.bufferEn        = 1;
    //    EmrwBuffer.timeoutCnt      = EMRW_INTERRUPT_READ_TIMEOUT;
    //    
    //    EmrwEnableInterruptReception(expectedCnt);
    return 1;
}

unsigned char EmrwSetSrCmd(unsigned char eepromLoc, unsigned char blockNum)
{
    //   unsigned char status;
    //    
    //    Uart1PutS_P("LSA");
    //    _Uart2PutCh(btohexa_high(eepromLoc));
    //    _Uart2PutCh(btohexa_low(eepromLoc));
    //    _Uart2PutCh(btohexa_high(blockNum));
    //    _Uart2PutCh(btohexa_low(blockNum));
    //    Uart1PutS_P("\r");
    //    DelayMs(50);
    return 1;
}

unsigned char EmrwSendSrCmd(void)
{
    //    Uart1PutS_P("SR\r");
    return 1;
}

unsigned char EmrwCheckCardPresence(void)
{
    //    Uart1PutS_P("SR\r");
    return 1;
}

unsigned int EmrwGetTemplateData(unsigned char startBlockNum, unsigned char *pTemplate, unsigned char eepromLoc)
{
    char uid[4];
    unsigned int i, k, status;
    unsigned int l = 0;
    char blockData[16];
    EMRW_CMD_PARAM cmdParam;

    for (i = 0; i < 32; i++)
    {
        if (0 != ((startBlockNum + i + 1) % 4))
        {
            cmdParam.cmd = EMRW_CMD_RBE;
            cmdParam.eepromLocation = eepromLoc;
            cmdParam.blockNum = startBlockNum + i;
            cmdParam.keyType = KEY_A;
            cmdParam.pData = blockData;
            cmdParam.pUid = uid;
            status = EmrwExecCmd(&cmdParam);
            if (status != 1)
            {
                return 0;
            }

            for (k = 0; k < 16; k++)
            {
                pTemplate[l++] = blockData[k];
            }
        }
    }
    return 1;
}

unsigned int EmrwSetTemplateData(unsigned char startBlockNum, unsigned char *pTemplate, unsigned char eepromLoc)
{
    char uid[4];
    unsigned int i, k, status;
    unsigned int l = 0;
    char blockData[16];
    EMRW_CMD_PARAM cmdParam;

    for (i = 0; i < 32; i++)
    {
        if (0 != ((startBlockNum + i + 1) % 4))
        {
            for (k = 0; k < 16; k++)
            {
                blockData[k] = pTemplate[l++];
            }
            cmdParam.cmd = EMRW_CMD_WBE;
            cmdParam.eepromLocation = eepromLoc;
            cmdParam.blockNum = startBlockNum + i;
            cmdParam.keyType = KEY_A;
            cmdParam.pData = blockData;
            cmdParam.pUid = uid;
            status = EmrwExecCmd(&cmdParam);
            if (status != 1)
            {
                return 0;
            }
        }
    }
    PushToQueue(&_uart2TxBuff, '\r');
    EnableIntU2TX;
    IFS1bits.U2TXIF = 1;
    return 1;
}

//
//  Make sure tht we have received complete response before calling
//  this function. This function will return the size of received response.
//

unsigned int EmrwGetResponseS(char *str)
{
    return _Uart2GetS(str);
}

unsigned int SizeOfEmrwCmdResponse(void)
{
    return SizeOfQueue(&_uart2RxBuff);
}

static unsigned int _Uart2GetS(char *str)
{
    unsigned int status = 0;
    unsigned int i = 0;

    while (0 == status)
    {
        status = PopFromQueue(&_uart2RxBuff, (str + i));
        if (1 == status)
        {
            str[i] = 0;
        }
        else
        {
            i++;
        }
    }

    return i;
}

//static unsigned int _Uart2GetCh(char *pCh)
//{
//    unsigned int status;
//
//    status = PopFromQueue(&_uart2RxBuff, pCh);
//
//    return status;
//}

static unsigned int _Uart2PutCh(unsigned char ch)
{
    PushToQueue(&_uart2TxBuff, ch);
    EnableIntU2TX;
    IFS1bits.U2TXIF = 1;

    return 1;
}

void EmrwPutS(char *str)
{
    while (*str)
        _Uart2PutCh(*str++);
}

//static unsigned char Rxdata[10];
//static unsigned char DataAvailable=0;
//static unsigned char Txdata[] = "Microchip";

//
// Interrupt Service routine for UART2 Transmission
//

void _U2TXInterrupt(void)
{
    static char ch = 0;

    U2TX_Clear_Intr_Status_Bit; // clear the interrupt status of UART2 TX
    if (PopFromQueue(&_uart2TxBuff, &ch))
    {
        DisableIntU2TX; // disable the UART TX interrupt after end of complete
        // transmission
    }
    else
    {
        while (BusyUART2()); // wait till the UART is busy
        WriteUART2((unsigned int) ch); // Transmit the data
    }
}


//
// Interrupt Service routine for UART2 reception
//

void _U2RXInterrupt(void)
{
    U2RX_Clear_Intr_Status_Bit; //clear the interrupt status of UART2 RX     

    //    while (!DataRdyUART2()); //wait for data reception on RX
    PushToQueue(&_uart2RxBuff, ReadUART2());
}

void __attribute((interrupt(ipl6), vector(_UART2_VECTOR), nomips16)) U2Interrupt(void)
{
    if (IFS1bits.U2RXIF)
        _U2RXInterrupt();
    if (IEC1bits.U2TXIE)
    {
        if (IFS1bits.U2TXIF)
            _U2TXInterrupt();
    }
}
