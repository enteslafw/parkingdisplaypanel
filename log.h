
#ifndef LOG_H
#define LOG_H

#define MAX_TRANSACTIONS 50000

typedef union {
    char array[6];

    struct {
        unsigned short int current;
        unsigned short int start;
        unsigned short int end;
    };
} LOG_INFO;



unsigned int PushLog(TRANSACTION *pTransaction);
unsigned int PopLog(TRANSACTION *pTransaction);
unsigned int GetLog(TRANSACTION *pTransaction, unsigned int logNum);
unsigned int GetTotalLogs(unsigned int *pTotalLogs);
#endif //  LOG_H
