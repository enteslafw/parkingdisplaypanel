
#ifndef _BUZZER_H
#define _BUZZER_H

    //******** User settings ********
	#define INPUT   1
	#define OUTPUT  0
	#define HIGH    1
	#define LOW     0
	
	#define BUZZER_PORT_DIR TRISEbits.TRISE7
	#define BUZZER_PIN      LATEbits.LATE7
    //****** User settings End ******
    
    #define BuzzOn()    BUZZER_PIN=HIGH 
    #define BuzzOff()   BUZZER_PIN=LOW
    
    void BuzzInit(void);
    void BuzzStartUp(void);
    void BuzzDone(void);
    void BuzzOneTime(unsigned int);
    void BuzzTwoTimes(unsigned int, unsigned int, unsigned int);
    void BuzzMs(unsigned int);
    
#endif	// _BUZZER_H

