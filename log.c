
#define USE_AND_OR

/* Device header file */
#if defined(__XC16__)
#include <xc.h>
#elif defined(__C30__)
#if defined(__PIC24E__)
#include <p24Exxxx.h>
#elif defined (__PIC24F__)||defined (__PIC24FK__)
#include <p24Fxxxx.h>
#elif defined(__PIC24H__)
#include <p24Hxxxx.h>
#endif
#elif defined(__XC32__)
#include <xc.h>
#endif
#include "global_vars.h"
#include "log.h"
#include "rtc.h"
#include "fatfs/ff.h"
#include "fatfs/diskio.h"

//
//  0-success, 1-fail
//

unsigned int PushLog(TRANSACTION *pTransaction)
{
    LOG_INFO logInfo;
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int numOfBytesRead, numOfBytesWritten, size;
    char ch;

    res = f_open(&fsrc, "1:log.txt", FA_OPEN_EXISTING | FA_READ | FA_WRITE);
    if (res != FR_OK)
    {
        Nop();
        return 1;
    }

    //
    // Get log details
    //
    size = sizeof (logInfo.array);
    res = f_read(&fsrc, logInfo.array, size, &numOfBytesRead);
    if ((res != FR_OK) || (numOfBytesRead != size))
    {
        f_close(&fsrc);
        return 1;
    }

    // Move to end of the file to append data
    res = f_lseek(&fsrc, sizeof (logInfo.array) + 1 + (unsigned long) logInfo.end * (sizeof (pTransaction->array) + 1));
    if (res != FR_OK)
    {
        f_close(&fsrc);
        return 1;
    }

    size = sizeof (pTransaction->array);
    res = f_write(&fsrc, pTransaction->array, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 1;
    }

    ch = '\r';
    size = sizeof (ch);
    res = f_write(&fsrc, &ch, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 1;
    }

    logInfo.end = (logInfo.end + 1) % MAX_TRANSACTIONS;
    if (logInfo.current < MAX_TRANSACTIONS)
    {
        logInfo.current++;
    }
    else
    {
        // Overwriting the oldest. Move start to next-oldest
        logInfo.start = (logInfo.start + 1) % MAX_TRANSACTIONS;
    }

    // Move to start of log file and update log info
    res = f_lseek(&fsrc, 0);
    if (res != FR_OK)
    {
        f_close(&fsrc);
        return 1;
    }
    size = sizeof (logInfo.array);
    res = f_write(&fsrc, logInfo.array, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 1;
    }

    ch = '\r';
    size = sizeof (ch);
    res = f_write(&fsrc, &ch, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 1;
    }
    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 1;
    }
    return 0;
}

//
//  0-success, 1-fail
//

unsigned int PopLog(TRANSACTION *pTransaction)
{
    LOG_INFO logInfo;
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int numOfBytesRead, numOfBytesWritten, size;
    unsigned char ch;


    res = f_open(&fsrc, "1:log.txt", FA_OPEN_EXISTING | FA_READ | FA_WRITE);
    if (res != FR_OK)
    {
        Nop();
        return 1;
    }

    //
    // Get log details
    //
    size = sizeof (logInfo.array);
    res = f_read(&fsrc, logInfo.array, size, &numOfBytesRead);
    if ((res != FR_OK) || (numOfBytesRead != size))
    {
        f_close(&fsrc);
        return 1;
    }

    if (!logInfo.current)
    {
        res = f_close(&fsrc);
        return 1;
    }

    // Move the transaction to be popped
    res = f_lseek(&fsrc, sizeof (logInfo.array) + 1 + (unsigned long) logInfo.start * (sizeof (pTransaction->array) + 1));
    if (res != FR_OK)
    {
        f_close(&fsrc);
        return 1;
    }

    // read out transaction
    size = sizeof (pTransaction->array);
    res = f_read(&fsrc, pTransaction->array, size, &numOfBytesRead);
    if ((res != FR_OK) || (numOfBytesRead != size))
    {
        f_close(&fsrc);
        return 1;
    }

    logInfo.start = (logInfo.start + 1) % MAX_TRANSACTIONS;
    logInfo.current--;

    // Move to start of log file and update log info
    res = f_lseek(&fsrc, 0);
    if (res != FR_OK)
    {
        f_close(&fsrc);
        return 1;
    }
    size = sizeof (logInfo.array);
    res = f_write(&fsrc, logInfo.array, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 1;
    }

    ch = '\r';
    size = sizeof (ch);
    res = f_write(&fsrc, &ch, size, &numOfBytesWritten);
    if ((res != FR_OK) || (numOfBytesWritten != size))
    {
        f_close(&fsrc);
        return 1;
    }
    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 1;
    }
    return 0;
}

//
//   0-success, 1-fail
//   logNum==>'0' is first log   
//

unsigned int GetLog(TRANSACTION *pTransaction, unsigned int logNum)
{
    LOG_INFO logInfo;
    //    TRANSACTION transaction;
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int numOfBytesRead, size;

    res = f_open(&fsrc, "1:log.txt", FA_OPEN_EXISTING | FA_READ | FA_WRITE);
    if (res != FR_OK)
    {
        Nop();
        return 1;
    }

    //
    // Get log details
    //
    size = sizeof (logInfo.array);
    res = f_read(&fsrc, logInfo.array, size, &numOfBytesRead);
    if ((res != FR_OK) || (numOfBytesRead != size))
    {
        f_close(&fsrc);
        return 1;
    }

    logInfo.start = (logInfo.start + logNum) % MAX_TRANSACTIONS;

    if (!logInfo.current)
    {
        res = f_close(&fsrc);
        return 1;
    }

    if ((logInfo.current) < logNum)
    {
        res = f_close(&fsrc);
        return 1;
    }

    // Move the transaction to be popped
    res = f_lseek(&fsrc, sizeof (logInfo.array) + 1 + (unsigned long) logInfo.start * (sizeof (pTransaction->array) + 1));
    if (res != FR_OK)
    {
        f_close(&fsrc);
        return 1;
    }

    // read out transaction
    size = sizeof (pTransaction->array);
    res = f_read(&fsrc, pTransaction->array, size, &numOfBytesRead);
    if ((res != FR_OK) || (numOfBytesRead != size))
    {
        f_close(&fsrc);
        return 1;
    }

    res = f_close(&fsrc);
    if (res != FR_OK)
    {
        Nop();
        return 1;
    }
    return 0;


}

unsigned int GetTotalLogs(unsigned int *pTotalLogs)
{
    LOG_INFO logInfo;
    FIL fsrc; /* file objects */
    FRESULT res; /* FatFs function common result code */
    unsigned int numOfBytesRead, size;

    res = f_open(&fsrc, "1:log.txt", FA_OPEN_EXISTING | FA_READ | FA_WRITE);
    if (res != FR_OK)
    {
        Nop();
        return 1;
    }

    //
    // Get log details
    //
    size = sizeof (logInfo.array);
    res = f_read(&fsrc, logInfo.array, size, &numOfBytesRead);
    if ((res != FR_OK) || (numOfBytesRead != size))
    {
        f_close(&fsrc);
        return 1;
    }

    res = f_close(&fsrc);

    *pTotalLogs = logInfo.current;
    return 0;
}

/*
unsigned int PushToQueue(CIRCULAR_BUFFER *cirBuff, unsigned char ch)
{
    cirBuff->buffer[cirBuff->end] = ch;
    cirBuff->end = (cirBuff->end + 1) % BUFFER_SIZE;

    if (cirBuff->current < BUFFER_SIZE)
    {
        cirBuff->current++;
    }
    else
    {
        // Overwriting the oldest. Move start to next-oldest
        cirBuff->start = (cirBuff->start + 1) % BUFFER_SIZE;
    }
    
    return 0;
}

unsigned int PopFromQueue(CIRCULAR_BUFFER *cirBuff, unsigned char *pCh)
{
    if (!cirBuff->current)
    { 
        return 1;
    }

 *pCh = cirBuff->buffer[cirBuff->start];
    cirBuff->start = (cirBuff->start + 1) % BUFFER_SIZE;

    cirBuff->current--;
    return 0;
}
 */
