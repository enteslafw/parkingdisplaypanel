
#include <xc.h>//#include <p24fxxxx.h>
#include "delay.h"
#include "buzzer.h"

void BuzzInit(void)
{
	BUZZER_PORT_DIR = OUTPUT;
}

void BuzzStartUp(void)
{
    BuzzTwoTimes(100, 70, 50);   
}

void BuzzDone(void)
{
    
}

void BuzzMs(unsigned int buzzTimeMs)
{
    BuzzOn();
    DelayMs(buzzTimeMs);
    BuzzOff();
}

void BuzzTwoTimes(unsigned int firstBuzzTimeMs, unsigned int buzzOffTimeMs, \
                    unsigned int secondBuzzTimeMs)
{
    BuzzOn();
    DelayMs(firstBuzzTimeMs);
    BuzzOff();
    DelayMs(buzzOffTimeMs);
    BuzzOn();
    DelayMs(secondBuzzTimeMs);
    BuzzOff();  
}
