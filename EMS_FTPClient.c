/*********************************************************************
 *
 *  Application to FTP Clinet for uploading and dowloading files
 *  Support for HTTP2 module in Microchip TCP/IP Stack
 *  FTP Client Initialization and Access Routines
 *  The client writes a file to an FTP (NAS) server
 *********************************************************************
 * FileName:        EMS_FTPClient.c
 * Dependencies:    TCP/IP stack
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *					Microchip C30 v3.12 or higher
 *					Microchip C18 v3.30 or higher
 *					HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         EnTesla, India.
 *
 * Tested On
 * Controller : PIC32MX795F512L
 * Compiler   : XC32 v1.21 or above
 * Hardware   : mifare + Ethernet board
 * Dependancies : fatfs/ff.h and global_vars.h
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Umesh Walkar     	16/04/07	Original
 ********************************************************************/

#include "TCPIPConfig.h"

#if defined (STACK_USE_EMS_FTP_CLIENT)


#include <stdlib.h>
#include <stdio.h>
#include "Compiler.h"
#include "GenericTypedefs.h"

#include "TCPIP Stack/TCPIP.h"
#include "EMS_FTPClient.h"
#include "fatfs/ff.h"
#include "global_vars.h"

// #define ftp_debug                          // uncomment to debug
#if defined(ftp_debug)
#define FTP_DPRINT(x)           //UART4_PUTS(x)x //call back function e.g. UART_PUTS()
#endif

#define MAX_FTP_TX_RX_BUFFER            1200  //should same TCP_PURPOSE_FTP_DATA TX/RX length

static FTP_PARAM _ftpParam;
static FTP_CNX_FOR _ftpCnxFor;
static FTP_CNX_STATUS _ftpCnxStatus;
static FTP_LAST_ERROR _ftpLastError;

static TCP_SOCKET FTPSocket;                // main ftp command socket
static TCP_SOCKET FTPDataSocket;            // ftp data socket
static WORD_VAL FTPDataPort;                // ftp data port number, as supplied by server
static DWORD ftpcTimer;                     // ftp operation timeout
static SM_FTPC smFTPC;                      // ftp client state
static WORD LogSize;

#if defined (ftp_debug)
static BYTE smFTPC_alt;
static BOOL ftpcEarlyDisconnect = 0;
int counter;
#endif

static BOOL ftpcTimeout, ftpcRunning = 0; //ftpcExist
static BOOL _ftpcIsRootDir = 0;

//BYTE FTP_ServerName[13] = {'1', '9', '2', '.', '1', '6', '8', '.', '0', '.', '3', '\0'}; // serveraddress                                 
//WORD FTP_COMMAND_PORT = (WORD) 21; // port                                          
//BYTE ftp_username[10] = "umesh"; // {'u', 'm', 'e', 's', 'h', '\0'}; // user                                          
//BYTE ftp_password[10] = "umesh"; //{'u', 'm', 'e', 's', 'h', '\0'}; // password                                      
//BYTE ftp_file_path[10] = "\\"; //{'\',D', 'A', 'T', '\0'}; // directory on the server                       
//static BYTE ftp_file_name[13]; // filename                                      

BYTE vBuffer[MAX_FTP_TX_RX_BUFFER];
//BYTE LogBuffer[10], rcvFile[10], LogName[9];
//BYTE ftpCurProcedure;

//BYTE vDataBuffer[91] = {
//    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '#', '{', '!', '"', '*', '$', '%',
//    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '#', '}', '!', '"', '*', '$', '%',
//    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', '#', '}', '!', '"', '*', '$', '%',
//    '\0'
//};
BYTE *vDataBuffer;

//BYTE rcvFileSource;
DWORD rcvFileLimit, rcvFileSize; //, rcvPos, ftp_send_size;

static FIL fsrc; /* file objects */
FRESULT res;     /* FatFs function common result code */
FILINFO fileInfo;
unsigned int numOfBytesWritten;

BOOL FTPC_ConnectOrTimeout(TCP_SOCKET ftpSock, BYTE duration);
BOOL ReadLine(WORD len, BYTE type);

//-------------------------------------------------------------------------------------------------------------------------------
// - initializes login & file variables, sets state machine to initiate a connection                                             
//-------------------------------------------------------------------------------------------------------------------------------

//BOOL FTPC_SendInit(BYTE dat_size, BYTE dat_name, BYTE ftp_cur_proc)
//{
//    LogSize = strlen(vDataBuffer); //dat_size;
//    ftpCurProcedure = ftp_cur_proc;
//    //if (dat_name==log_temperatur) sprintf(ftp_file_STOR, "Temperat.dat");
//    //if (dat_name==log_logbuch  ) sprintf(ftp_file_STOR, "Logbuch.dat");
//
//    if (ftpCurProcedure == FTPC_UPLOAD)
//        strcpypgm2ram(ftp_file_name, (ROM char*) "up2.txt");
//    else
//        strcpypgm2ram(ftp_file_name, (ROM char*) "image.hex");
//
//    rcvFileLimit = sizeof (rcvFile) - 2;
//    if (strlen(FTP_ServerName) > 6)
//    {
//        smFTPC = SM_FTPC_HOME; // start connection attempt                      
//        ftpcRunning = TRUE; // tells main() to keep calling FTPClient()      
//        return TRUE;
//    }
//    return FALSE;
//}

//-------------------------------------------------------------------------------------------------------------------------------
//    This client read/writes a file to an FTP server
//-------------------------------------------------------------------------------------------------------------------------------

void FTPClient(void)
{
    static BYTE cntAttempts;
    WORD cntReady, cntRead , y;  //, x

    if (!ftpcRunning)
        return;

#if defined(ftp_debug)
    if (smFTPC > 0 && r_set == 1) FTP_DPRINT("\r\nSM_FTPC alt = %d", smFTPC);
#endif
    //    if (r_set == 1)
    //    {
    //        r_set = 0;
    //        smFTPC = SM_FTPC_DONE;
#if defined(ftp_debug)
    FTP_DPRINT("\r\nSM_FTPC_FTPC neu = %d", smFTPC);
#endif
    //    }

    switch (smFTPC)
    {
        // <editor-fold defaultstate="collapsed" desc="Open FTP COMMAND SOCKET">
    case SM_FTPC_HOME:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_HOME [%s]", FTP_ServerName);
        }
#endif
        _ftpCnxStatus = FTP_CNX_COMMAND_SOCKET_OPENNING;
        //                           TCPOpen handles all DNS and/or ARP resolution traffic
        FTPSocket = TCPOpen((DWORD) & _ftpParam.ftpServerName[0], TCP_OPEN_RAM_HOST, _ftpParam.ftpPort, TCP_PURPOSE_FTP_COMMAND);
        if (FTPSocket == INVALID_SOCKET)
        {
            cntAttempts += 1;
            if (cntAttempts > 5)
            {
                smFTPC = SM_FTPC_DONE;
            }
            break;
        }
        smFTPC = SM_FTPC_CONNECTWAIT; //smFTPC += 1;
        ftpcTimer = TickGet();
        break;

    case SM_FTPC_CONNECTWAIT:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_CONNECTWAIT");
        }
#endif

        if (TCPIsConnected(FTPSocket) == FALSE)
        {
            if (TickGet() - ftpcTimer > 5 * TICK_SECOND)
            {
                smFTPC = SM_FTPC_DISCONNECT;
                ftpcTimeout = TRUE;
            }
            break;
        }
        _ftpCnxStatus = FTP_CNX_COMMAND_SOCKET_OPENED;
        smFTPC = SM_FTPC_GET_GREETING; //smFTPC += 1;
        ftpcTimer = TickGet();
        break; // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Check Welcome Message from FTP Server">
    case SM_FTPC_GET_GREETING:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_GREETING");
        }
#endif
        //adjust time-out based upon network environment: Intranet/Internet, expected traffic, etc.
        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE) // returns FALSE only if disconnected
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 4) // minimum length for a proper reply
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_GREETING) == TRUE)
            {
                smFTPC = SM_FTPC_SEND_USER; //smFTPC += 1;
            }
        }
        break; // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Send Login User Name">
    case SM_FTPC_SEND_USER:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_USER");
        }
#endif
        _ftpCnxStatus = FTP_CNX_VALIDATING_USER;
        if (FTPC_ConnectOrTimeout(FTPSocket, 0) == FALSE) // no time-out test is necessary
        {
            break;
        }
        //                           could use (strlen() + 7) vs (sizeof() + 7) -> larger code
        y = sizeof (_ftpParam.ftpUserName) + 7;
        if (TCPIsPutReady(FTPSocket) > y)
        {
            sprintf(vBuffer, "USER %s\r\n", _ftpParam.ftpUserName);
            TCPPutString(FTPSocket, vBuffer);
            TCPFlush(FTPSocket);
            ftpcTimer = TickGet(); // ALWAYS RESTART TIME-OUT ON A TRANSMISSION
            smFTPC = SM_FTPC_GET_USER; //smFTPC += 1;
        }
        break;

    case SM_FTPC_GET_USER:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_USER");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE)
        {
            break;
        }

        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 4)
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_USER) == TRUE)
            {
                smFTPC = SM_FTPC_SEND_PASS; //smFTPC += 1;
            }
        }
        break; // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Send Login Password">
    case SM_FTPC_SEND_PASS:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_PASS");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 0) == FALSE)
        {
            break;
        }
        y = sizeof (_ftpParam.ftpUserPass) + 7;
        if (TCPIsPutReady(FTPSocket) > y)
        {
            sprintf(vBuffer, "PASS %s\r\n", _ftpParam.ftpUserPass);
            TCPPutString(FTPSocket, vBuffer);
            TCPFlush(FTPSocket);
            ftpcTimer = TickGet();
            smFTPC = SM_FTPC_GET_PASS; // smFTPC += 1;
        }
        break;

    case SM_FTPC_GET_PASS:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_PASS");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE)
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 4)
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_PASS) == TRUE)
            {
                if (rcvFileSize == 0) // rcvFile is available
                {
                    smFTPC = SM_FTPC_SEND_CWD; //smFTPC += 1;
                }
                else
                {
                    smFTPC = SM_FTPC_SEND_CWD; // don't fill rcvFile[] while still processing
                    _ftpCnxStatus = FTP_CNX_VALIDEDTED_USER;
                }
            }
        }
        break; // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Change working directory">
    case SM_FTPC_SEND_CWD:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_CWD2");
        }
#endif
        _ftpCnxStatus = FTP_CNX_OPENNING_DIR;
        if (FTPC_ConnectOrTimeout(FTPSocket, 0) == FALSE)
        {
            break;
        }
        y = sizeof (_ftpParam.ftpRemoteFilePath) + 6;
        if (TCPIsPutReady(FTPSocket) > y)
        {
            sprintf(vBuffer, "CWD %s\r\n", _ftpParam.ftpRemoteFilePath);
            TCPPutString(FTPSocket, vBuffer);
            TCPFlush(FTPSocket);
            ftpcTimer = TickGet();
            smFTPC = SM_FTPC_GET_CWD; //smFTPC += 1;
        }
        break;

    case SM_FTPC_GET_CWD:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_CWD");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE)
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 4)
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_CWD) == TRUE || _ftpcIsRootDir == TRUE)
            {
                smFTPC = SM_FTPC_SEND_TYPE; //smFTPC += 1;
            }
        }
        break; // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="FTP TYPE Command - ASCII mode used">
    case SM_FTPC_SEND_TYPE:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_TYPE");
        }
#endif
        _ftpCnxStatus = FTP_CNX_OPENNING_ASCII_MODE;
        if (FTPC_ConnectOrTimeout(FTPSocket, 0) == FALSE)
        {
            break;
        }
        if (TCPIsPutReady(FTPSocket) > 8)
        {
            sprintf(vBuffer, "TYPE A\r\n");
            TCPPutString(FTPSocket, vBuffer);
            TCPFlush(FTPSocket);
            ftpcTimer = TickGet();
            smFTPC = SM_FTPC_GET_TYPE; //smFTPC += 1;
        }
        break;

    case SM_FTPC_GET_TYPE:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_TYPE");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE)
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 4)
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_TYPE) == TRUE)
            {
                smFTPC = SM_FTPC_SEND_PASV; //smFTPC += 1;
            }
        }
        break; // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Open In Passive Mode">

    case SM_FTPC_SEND_PASV:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_PASV");
        }
#endif
        _ftpCnxStatus = FTP_CNX_OPENNING_PASSIVE_MODE;
        if (FTPC_ConnectOrTimeout(FTPSocket, 0) == FALSE)
        {
            break;
        }
        if (TCPIsPutReady(FTPSocket) > 6)
        {
            sprintf(vBuffer, "PASV\r\n");
            TCPPutString(FTPSocket, vBuffer);
            TCPFlush(FTPSocket);
            ftpcTimer = TickGet();
            smFTPC = SM_FTPC_GET_PASV; //smFTPC += 1;
        }
        break;

    case SM_FTPC_GET_PASV:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_PASV");
        }
#endif

        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE)
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 25)
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_PASV) == TRUE)
            {
                smFTPC = SM_FTPC_CONNECT_DATA; //smFTPC += 1;
                cntAttempts = 0;
            }
        }
        break; // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Opening FTP DATA SOCKET">

    case SM_FTPC_CONNECT_DATA:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_CONNECT_DATA Port=[%u]", FTPDataPort.Val);
        }
#endif
        _ftpCnxStatus = FTP_CNX_DATA_SOCKET_OPENNING;
        FTPDataSocket = TCPOpen((PTR_BASE) & TCPGetRemoteInfo(FTPSocket)->remote, TCP_OPEN_NODE_INFO, FTPDataPort.Val, TCP_PURPOSE_FTP_DATA);
        //TCP_OPEN_IP_ADDRESS
        if (FTPDataSocket == INVALID_SOCKET)
        {
            cntAttempts += 1;
            if (cntAttempts > 5)
            {
                smFTPC = SM_FTPC_DONE;
            }
            break;
        }
        smFTPC = SM_FTPC_WAIT_DATA; //smFTPC += 1;
        ftpcTimer = TickGet();
        break;

    case SM_FTPC_WAIT_DATA:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_WAIT_DATA");
        }
#endif
        if (TCPIsConnected(FTPDataSocket) == FALSE)
        {
            if (TickGet() - ftpcTimer > 5 * TICK_SECOND)
            {
                smFTPC = SM_FTPC_DISCONNECT;
            }
            break;
        }

        if (_ftpCnxFor == FTP_UPLOAD_FILE || _ftpCnxFor == FTP_UPLOAD_ARRAY)
            smFTPC = SM_FTPC_SEND_STOR;
        else
            smFTPC = SM_FTPC_SEND_SIZE;

        //smFTPC += 1;
        ftpcTimer = TickGet();
        break; // </editor-fold>

        /****** uploading file procedure */
        // <editor-fold defaultstate="collapsed" desc="Uploading Procedure">
    case SM_FTPC_SEND_STOR:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_STOR");
        }
#endif
        _ftpCnxStatus = FTP_CNX_ACCESSING_UP_FILE;
        if (FTPC_ConnectOrTimeout(FTPSocket, 0) == FALSE)
        {
            break;
        }
        y = sizeof (_ftpParam.ftpRemoteFileName) + 7;
        if (TCPIsPutReady(FTPSocket) > y)
        {
            if (_ftpParam.ftpFileMode == OVERWRITE)
                sprintf(vBuffer, "STOR %s\r\n", _ftpParam.ftpRemoteFileName); //create new file
            else
                sprintf(vBuffer, "APPE %s\r\n", _ftpParam.ftpRemoteFileName); // Append to file

            TCPPutString(FTPSocket, vBuffer);
            TCPFlush(FTPSocket);
            ftpcTimer = TickGet();
            //smFTPC += 1;
            smFTPC = SM_FTPC_GET_STOR;
        }
        break;

    case SM_FTPC_GET_STOR:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_STOR");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE)
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 4)
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_STOR) == TRUE)
            {
                ftpcTimer = TickGet();
                //smFTPC += 1;
                if (_ftpCnxFor == FTP_UPLOAD_FILE)
                    smFTPC = SM_FTPC_SEND_FILE_DATA;
                else
                    smFTPC = SM_FTPC_SEND_ARRAY_DATA;

#if defined(ftp_debug)
                FTP_DPRINT("\r\nSize=%d", LogSize);
#endif
                //memcpy((void *) vDataBuffer,(void *)  DAT, LogSize); //it will pass the data
                //memcpy((void *) vDataBuffer,(ROM char*) "umesh", LogSize); //it will pass the data
            }
        }
        break;

    case SM_FTPC_SEND_FILE_DATA:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_DATA");
        }
#endif
        _ftpCnxStatus = FTP_CNX_UPLOADING_FILE;
        if (FTPC_ConnectOrTimeout(FTPDataSocket, 55) == FALSE)
        {
            break;
        }
        cntReady = TCPIsPutReady(FTPDataSocket);

        res = f_read(&fsrc, vBuffer, cntReady, &LogSize);
        if (res == FR_OK)
        {
            if (LogSize <= 0)
            {
                //this means file is read completely
                if (rcvFileLimit == rcvFileSize)
                {
                    f_close(&fsrc);
                    smFTPC = SM_FTPC_WAIT_CLOSE_DATA;
                }
                //ftpcTimer = TickGet();
                break;
            }
            else //if (cntReady >= LogSize)
            {
                cntRead = TCPPutArray(FTPDataSocket, vBuffer, LogSize);
                rcvFileSize += LogSize;
                //smFTPC += 1;
                LogSize = 0;
                ftpcTimer = TickGet();
            }
        }
        TCPFlush(FTPDataSocket);
        break;

    case SM_FTPC_SEND_ARRAY_DATA:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_DATA");
        }
#endif
        _ftpCnxStatus = FTP_CNX_UPLOADING_ARRAY;
        if (FTPC_ConnectOrTimeout(FTPDataSocket, 8) == FALSE)
        {
            break;
        }

        if (LogSize <= 0) //all bytes sent in previous session
        {
            smFTPC = SM_FTPC_WAIT_CLOSE_DATA;
            break;
        }

        cntReady = TCPIsPutReady(FTPDataSocket);
        if (cntReady >= LogSize)
        { //sent all bytes
            cntRead = TCPPutArray(FTPDataSocket, &vDataBuffer[rcvFileLimit], LogSize);
            //smFTPC += 1;
            smFTPC = SM_FTPC_WAIT_CLOSE_DATA;
            LogSize = 0;
            // ftpcTimer = TickGet();
        }
        else
        {
            cntRead = TCPPutArray(FTPDataSocket, &vDataBuffer[rcvFileLimit], cntReady);
            rcvFileLimit += cntReady;
            LogSize -= cntReady;
            //            y = 0;
            //            for (x = cntReady + 1; x < rcvFileSize; x++)
            //            {
            //                vDataBuffer[y] = vDataBuffer[x];
            //                y += 1;
            //            }
            //            rcvFileSize = y - 1;
        }
        ftpcTimer = TickGet();
        TCPFlush(FTPDataSocket);
        break;
        // </editor-fold>

        /****** downloading file procedure */
        // <editor-fold defaultstate="collapsed" desc="Downloading Procedure">
    case SM_FTPC_SEND_SIZE:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_STOR");
        }
#endif
        _ftpCnxStatus = FTP_CNX_ACCESSING_DOWN_FILE;
        if (FTPC_ConnectOrTimeout(FTPSocket, 0) == FALSE)
        {
            break;
        }
        y = sizeof (_ftpParam.ftpRemoteFileName) + 7;
        if (TCPIsPutReady(FTPSocket) > y)
        {
            sprintf(vBuffer, "SIZE %s\r\n", _ftpParam.ftpRemoteFileName); //create new file
            TCPPutString(FTPSocket, vBuffer);
            TCPFlush(FTPSocket);
            ftpcTimer = TickGet();
            smFTPC = SM_FTPC_GET_SIZE; //smFTPC += 1;
        }
        break;


    case SM_FTPC_GET_SIZE:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_STOR");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE)
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 4)
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_SIZE) == TRUE)
            {
                ftpcTimer = TickGet();
                smFTPC = SM_FTPC_SEND_RETR; //smFTPC += 1;
#if defined(ftp_debug)
                FTP_DPRINT("\r\nSize=%d", rcvFileSize);
#endif
                //vDataBuffer[0] = '\0';
                rcvFileLimit = rcvFileSize; //this is the size of remote files data

                if (_ftpCnxFor == FTP_DOWNLOAD_FILE)
                {
                    if (_ftpParam.ftpFileMode == OVERWRITE)
                        res = f_open(&fsrc, _ftpParam.ftpLocalFileName, FA_CREATE_ALWAYS | FA_WRITE);
                    else
                        res = f_open(&fsrc, _ftpParam.ftpLocalFileName, FA_OPEN_ALWAYS | FA_WRITE);
                }
                //memcpy((void *) vDataBuffer,(void *)  DAT, LogSize); //it will pass the data
                //memcpy((void *) vDataBuffer,(ROM char*) "umesh", LogSize); //it will pass the data
            }
        }
        break;

    case SM_FTPC_SEND_RETR:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_STOR");
        }
#endif
        _ftpCnxStatus = FTP_CNX_DOWNLOADING_FILE;
        if (FTPC_ConnectOrTimeout(FTPSocket, 0) == FALSE)
        {
            break;
        }
        y = sizeof (_ftpParam.ftpRemoteFileName) + 7;
        if (TCPIsPutReady(FTPSocket) > y)
        {
            sprintf(vBuffer, "RETR %s\r\n", _ftpParam.ftpRemoteFileName); //retrieve new file
            TCPPutString(FTPSocket, vBuffer);
            TCPFlush(FTPSocket);
            ftpcTimer = TickGet();
            smFTPC = SM_FTPC_GET_RETR; //smFTPC += 1;
        }
        break;

    case SM_FTPC_GET_RETR:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_STOR");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE)
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 4)
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_RETR) == TRUE)
            {
                ftpcTimer = TickGet();
                if (_ftpCnxFor == FTP_DOWNLOAD_FILE)
                    smFTPC = SM_FTPC_RECV_FILE_DATA;
                else
                    smFTPC = SM_FTPC_RECV_ARRAY_DATA;
                //smFTPC += 1;

#if defined(ftp_debug)
                FTP_DPRINT("\r\nSize=%d", LogSize);
#endif
                //memcpy((void *) vDataBuffer,(void *)  DAT, LogSize); //it will pass the data
                //memcpy((void *) vDataBuffer,(ROM char*) "umesh", LogSize); //it will pass the data
            }
        }
        break;


    case SM_FTPC_RECV_FILE_DATA:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_RECV_COMPLETE");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 55) == FALSE)
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPDataSocket);
        cntRead = TCPGetArray(FTPDataSocket, vBuffer, cntReady);
        if (cntReady > 0)
        {
            ftpcTimer = TickGet();
            //strncat(vDataBuffer,vBuffer,cntRead);
            res = f_write(&fsrc, vBuffer, cntRead, &numOfBytesWritten);
            if ((res != FR_OK) || (numOfBytesWritten != cntRead))
            {
                //f_close(&fsrc);
                //return 0;
            }
            rcvFileSize -= cntRead;
            if (rcvFileSize <= 0)
            {
                f_close(&fsrc);
                ftpcTimer = TickGet();
                //smFTPC += 1;
                smFTPC = SM_FTPC_WAIT_CLOSE_DATA;

                //#if defined (ftp_debug)
                f_stat(_ftpParam.ftpLocalFileName, &fileInfo);
                //sprintf(vDataBuffer, "file size received=%ld", fileInfo.fsize);
                if (rcvFileLimit != fileInfo.fsize)
                {
                    //file not downloaded completely. remove it
                    f_unlink(_ftpParam.ftpLocalFileName);
                }
                //                else
                //                {
                //                    if (systemFlag.SearchFirmware)
                //                        systemFlag.SearchFirmware = 0; //firmware file downloaded
                //                }
                //       strcpy(curHTTP.data,"file received");
                //#endif

                cntReady = TCPIsGetReady(FTPSocket);
                if (cntReady >= 4)
                {
                    cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
                    if (ReadLine(cntRead, FTP_RECV) == TRUE)
                    {
                        //f_close(&fsrc);
                        ftpcTimer = TickGet();
                        //smFTPC += 1;
                        smFTPC = SM_FTPC_WAIT_CLOSE_DATA;
                    }
                }
            }
        }
        break;

    case SM_FTPC_RECV_ARRAY_DATA:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_RECV_COMPLETE");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 55) == FALSE)
        {
            break;
        }

        cntReady = TCPIsGetReady(FTPDataSocket);
        cntRead = TCPGetArray(FTPDataSocket, vBuffer, cntReady);
        if (cntRead > 0)
        {
            ftpcTimer = TickGet();
            strncat(vDataBuffer, vBuffer, cntRead);
            LogSize -= cntRead;
            if (LogSize <= 0) //all bytes recv in previous session
            {
                smFTPC = SM_FTPC_WAIT_CLOSE_DATA;
                break;
            }

        }
        else
        {
            /***** in case LogSize is expected more than actually bytes aailable to receive from server
             * check for server sending transfer complete status */
            cntReady = TCPIsGetReady(FTPSocket);
            if (cntReady >= 4)
            {
                cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
                if (ReadLine(cntRead, FTP_RECV) == TRUE)
                {
                    //f_close(&fsrc);
                    ftpcTimer = TickGet();
                    //smFTPC += 1;
                    smFTPC = SM_FTPC_WAIT_CLOSE_DATA;
                }
            }
        }
        break;
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Closing Connection">
    case SM_FTPC_WAIT_CLOSE_DATA: /* client disconnects the data channel */
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_CLOSE_DATA");
        }
#endif
        _ftpCnxStatus = FTP_CNX_CLOSING;
        if (TCPGetTxFIFOFull(FTPDataSocket) != 0)
        {
            if (TickGet() - ftpcTimer > 5 * TICK_SECOND)
            {
                smFTPC = SM_FTPC_DISCONNECT;
                ftpcTimeout = TRUE;
            }
            break;
        }
        else
        {
            TCPDisconnect(FTPDataSocket);
        }

        //smFTPC += 1;
        smFTPC = SM_FTPC_SEND_QUIT;
        ftpcTimer = TickGet();
        break;

    case SM_FTPC_SEND_QUIT:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_SEND_QUIT");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 0) == FALSE)
        {
            break;
        }
        if (TCPIsPutReady(FTPSocket) > 6)
        {
            sprintf(vBuffer, "QUIT\r\n");
            TCPPutString(FTPSocket, vBuffer);
            TCPFlush(FTPSocket);
            ftpcTimer = TickGet();
            smFTPC = SM_FTPC_GET_QUIT; //smFTPC += 1;
        }
        break;

    case SM_FTPC_GET_QUIT:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_GET_QUIT");
        }
#endif
        if (FTPC_ConnectOrTimeout(FTPSocket, 5) == FALSE)
        {
            break;
        }
        cntReady = TCPIsGetReady(FTPSocket);
        if (cntReady >= 4)
        {
            cntRead = TCPGetArray(FTPSocket, vBuffer, cntReady);
            if (ReadLine(cntRead, FTP_QUIT) == TRUE)
            {
                smFTPC = SM_FTPC_DISCONNECT; //smFTPC += 1;
            }
        }
        break; // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Disconnect FTP Socket Connection">
    case SM_FTPC_DISCONNECT:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_DISCONNECT");
        }
#endif

        TCPDisconnect(FTPSocket);
        if (TCPIsConnected(FTPSocket) == FALSE)
        {
            FTPSocket = INVALID_SOCKET;
            if (FTPDataSocket != INVALID_SOCKET)
            {
                TCPDisconnect(FTPDataSocket);
                FTPDataSocket = INVALID_SOCKET;
            }
            smFTPC = SM_FTPC_DONE; //smFTPC += 1;
        }
        break;

    case SM_FTPC_DONE:
#if defined(ftp_debug)
        if (smFTPC != smFTPC_alt)
        {
            smFTPC_alt = smFTPC;
            FTP_DPRINT("\r\nSM_FTPC_DONE");
        }
#endif
        _ftpCnxStatus = FTP_CNX_CLOSED;
        cntAttempts = 0; // reset counter for next call into FTPClient()
        ftpcRunning = FALSE; // tells main() to stop calling FTPClient()
        break; // </editor-fold>

    default: smFTPC = SM_FTPC_DONE;
    }
}

//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------
//     FTP Client Support Functions                                                                                              
//-------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------
//    ConnectOrTimeout - tests socket connection status & time-out progression                                                   
//    returns FALSE if connection has been broken, else TRUE to allow state to complete before DISCONNECT                        
//-------------------------------------------------------------------------------------------------------------------------------

BOOL FTPC_ConnectOrTimeout(TCP_SOCKET ftpSock, BYTE duration)
{
#if defined (ftp_debug)
    static SM_FTPC lastFSM;
#endif

    if (TCPIsConnected(ftpSock) == FALSE)
    {
        smFTPC = SM_FTPC_DISCONNECT;
#if defined (ftp_debug)
        lastFSM = smFTPC;
        ftpcEarlyDisconnect = TRUE; // allows tra. of WHY the process failed in debug
#endif
        return FALSE;
    }

    if (duration != 0)
    {
        if (TickGet() - ftpcTimer > duration * TICK_SECOND)
        {
#if defined (ftp_debug)
            lastFSM = smFTPC; // allows tra. of WHERE t.process failed in debug
#endif
            smFTPC = SM_FTPC_DISCONNECT; // may be superceded in respective state         
            ftpcTimeout = TRUE; // allows tra. of WHY the process failed in debug
        }
    }
    return TRUE;
}

//-------------------------------------------------------------------------------------------------------------------------------
//*     ReadLine - processes FTP response strings to determine when a command has completed                                      
//-------------------------------------------------------------------------------------------------------------------------------

BOOL ReadLine(WORD len, BYTE type)
{
    WORD ResponseCode;
    WORD wTemp;
    char *comma;

    _ftpLastError = FTP_NO_ERROR;

    if (type == FTP_RETR_DATA) // this 'type' uses vDataBuffer vs. vBuffer      
    {
        goto TestType; // don't try to process a response code          
    }

    //   Null terminate the ASCII response string, allows string functions to process the response                                   
    vBuffer[len + 1] = '\0';
    ResponseCode = atoi((char*) vBuffer);
    if ((ResponseCode == 0) && (type != FTP_STAT)) // non-data line, doesn't start with a number    
    {
        _ftpLastError = ResponseCode;
        return FALSE;
    }

    if ((ResponseCode > 99) && (ResponseCode < 600)) // filter all multi-line replies                 
    {
        if (vBuffer[3] == '-')
        {
#if defined(ftp_debug)
            FTP_DPRINT("\r\n");
            for (counter = 0; counter < 20; counter++)
            {
                FTP_DPRINT("%s", vBuffer[counter]);
            }
#endif
            _ftpLastError = ResponseCode;
            return FALSE;
        }
    }
TestType:
    switch (type)
    {
    case FTP_GREETING:
        if (ResponseCode != 220)
        {
            _ftpLastError = ResponseCode; //FTP_GREETING_FAIL;
            return FALSE;
        }
        break;
    case FTP_USER:
        if (ResponseCode != 331)
        {
            _ftpLastError = ResponseCode;
            return FALSE;
        }
        break;
    case FTP_PASS:
        if (ResponseCode != 230)
        {
            _ftpLastError = ResponseCode;
            return FALSE;
        }
        break;
    case FTP_CWD:
        if (ResponseCode != 250)
        {
            _ftpLastError = ResponseCode;
            return FALSE;
        }
        break;
    case FTP_TYPE:
        if (ResponseCode != 200)
        {
            _ftpLastError = ResponseCode;
            return FALSE;
        }
        break;
    case FTP_PASV:
        //               response string looks something like '227 Entering Pass...(xxx,xxx,xxx,xxx,hhh,lll)'
        if (ResponseCode == 227)
        {
            for (wTemp = 0; wTemp < 4; wTemp++)
            {
                comma = strchr(vBuffer, ','); // find a ','
                *comma = ' '; // replace with a ' ', allows finding next ','
            }
            FTPDataPort.v[1] = (BYTE) atoi((char*) comma); // hhh
            comma = strchr(vBuffer, ',');
            *comma = ' ';
            FTPDataPort.v[0] = (BYTE) atoi((char*) comma); // lll
        }
        else
        {
            _ftpLastError = ResponseCode;
            return FALSE;
        }
        break;
    case FTP_STOR:
        if ((ResponseCode == 150) || (ResponseCode == 125))
        {
            break;
        }
        else
        {
            _ftpLastError = ResponseCode;
            return FALSE;
        }
        break;

    case FTP_SIZE:
        if (ResponseCode == 213) //213 _no of bytes_
        {
            //retrive no of bytes
            comma = strchr(vBuffer, '\r'); // find a ','
            *comma = '\0';
            comma = strchr(vBuffer, ' '); // find a ','
            comma++;

            rcvFileSize = atoi((char*) comma);
            break;
        }//        else if (ResponseCode == 550)
            //        {
            //            //file not found
            //        }
        else
        {
            _ftpLastError = ResponseCode;
            return FALSE;

        }
        break;

    case FTP_RETR:
        if ((ResponseCode == 150) || (ResponseCode == 125))
        {
            break;
        }
        else
        {
            _ftpLastError = ResponseCode;
            return FALSE;
        }
        break;

    case FTP_RECV:
        if (ResponseCode != 226)
        {
            _ftpLastError = ResponseCode;
            return FALSE;
        }
        break;
    case FTP_QUIT:
        if (ResponseCode != 221)
        {
            _ftpLastError = ResponseCode;
            return FALSE;
        }
        break;
    default:
        _ftpLastError = ResponseCode;
        return FALSE;
    }
    return TRUE;
}

/*****************************************************************************
  Function:
        BOOL FTPC_UploadFileFromSdCard(FTP_PARAM *ftpParam)

  Description:
        sets FTP Client routine to uploads file to FTP Server

  Precondition: FTP_PARAM *ftpParam
        structure passed to this function should fill with appropriate values

  Parameters:
        FTP_PARAM *ftpParam - structre defining FTP operation details.

  Return Values:
        1 - Success
        0 - fail
 ***************************************************************************/
BOOL FTPC_UploadFileFromSdCard(FTP_PARAM *ftpParam)
{
    if (ftpcRunning)
        return 0;

    if (ftpParam->ftpServerName == NULL)
        return 0;

    if (ftpParam->ftpUserName == NULL)
        return 0;

    if (ftpParam->ftpUserPass == NULL)
        return 0;

    if (ftpParam->ftpRemoteFilePath == NULL)
    {
        strcpy(_ftpParam.ftpRemoteFilePath, "//");
        //return 0;
    }
    else
        strcpy(_ftpParam.ftpRemoteFilePath, ftpParam->ftpRemoteFilePath);

    if (_ftpParam.ftpRemoteFilePath[0] == '//' && _ftpParam.ftpRemoteFilePath[1] == '\0')
        _ftpcIsRootDir = TRUE;
    else
        _ftpcIsRootDir = FALSE;

    if (ftpParam->ftpRemoteFileName == NULL)
        return 0;

    if (ftpParam->ftpLocalFileName == NULL)
        return 0;

    strcpy(_ftpParam.ftpServerName, ftpParam->ftpServerName);
    strcpy(_ftpParam.ftpUserName, ftpParam->ftpUserName);
    strcpy(_ftpParam.ftpUserPass, ftpParam->ftpUserPass);

    strcpy(_ftpParam.ftpRemoteFileName, ftpParam->ftpRemoteFileName);
    strcpy(_ftpParam.ftpLocalFileName, ftpParam->ftpLocalFileName);

    _ftpParam.ftpFileMode = ftpParam->ftpFileMode;
    _ftpParam.ftpPort = ftpParam->ftpPort;


    if (_ftpParam.ftpPort == 0)
        _ftpParam.ftpPort = 21;

    _ftpLastError = FTP_NO_ERROR;
    _ftpCnxFor = FTP_UPLOAD_FILE;

    res = f_stat(_ftpParam.ftpLocalFileName, &fileInfo);
    rcvFileLimit = fileInfo.fsize;
    rcvFileSize = 0;

    res = f_open(&fsrc, _ftpParam.ftpLocalFileName, FA_OPEN_EXISTING | FA_READ);
    if (res != FR_OK)
    {
        Nop();
        return 0;
    }

    if (strlen(_ftpParam.ftpServerName) > 6)
    {
        smFTPC = SM_FTPC_HOME; // start connection attempt                      
        ftpcRunning = TRUE; // tells main() to keep calling FTPClient()      
        return TRUE;
    }
    return 0;
}

/*****************************************************************************
  Function:
        BOOL FTPC_DownloadFileToSdCard(FTP_PARAM *ftpParam)

  Description:
        sets FTP Client routine to download file from FTP Server

  Precondition: FTP_PARAM *ftpParam
        structure passed to this function should fill with appropriate values

  Parameters:
        FTP_PARAM *ftpParam - structre defining FTP operation details.

  Return Values:
        1 - Success
        0 - fail
 ***************************************************************************/
BOOL FTPC_DownloadFileToSdCard(FTP_PARAM *ftpParam)
{
    if (ftpcRunning)
        return 0;

    if (ftpParam->ftpServerName == NULL)
        return 0;

    if (ftpParam->ftpUserName == NULL)
        return 0;

    if (ftpParam->ftpUserPass == NULL)
        return 0;

    if (ftpParam->ftpRemoteFilePath == NULL)
    {
        strcpy(_ftpParam.ftpRemoteFilePath, "//");
        //return 0;
    }
    else
        strcpy(_ftpParam.ftpRemoteFilePath, ftpParam->ftpRemoteFilePath);

    if (_ftpParam.ftpRemoteFilePath[0] == '//' && _ftpParam.ftpRemoteFilePath[1] == '\0')
        _ftpcIsRootDir = TRUE;
    else
        _ftpcIsRootDir = FALSE;

    if (ftpParam->ftpRemoteFileName == NULL)
        return 0;

    if (ftpParam->ftpLocalFileName == NULL)
        return 0;

    strcpy(_ftpParam.ftpServerName, ftpParam->ftpServerName);
    strcpy(_ftpParam.ftpUserName, ftpParam->ftpUserName);
    strcpy(_ftpParam.ftpUserPass, ftpParam->ftpUserPass);

    strcpy(_ftpParam.ftpRemoteFileName, ftpParam->ftpRemoteFileName);
    strcpy(_ftpParam.ftpLocalFileName, ftpParam->ftpLocalFileName);

    _ftpParam.ftpFileMode = ftpParam->ftpFileMode;
    _ftpParam.ftpPort = ftpParam->ftpPort;


    if (_ftpParam.ftpPort == 0)
        _ftpParam.ftpPort = 21;

    _ftpLastError = FTP_NO_ERROR;
    _ftpCnxFor = FTP_DOWNLOAD_FILE;

    if (strlen(_ftpParam.ftpServerName) > 6)
    {
        smFTPC = SM_FTPC_HOME; // start connection attempt                      
        ftpcRunning = TRUE; // tells main() to keep calling FTPClient()      
        return TRUE;
    }
    return 0;
}

/*****************************************************************************
  Function:
        BOOL FTPC_UploadFromArray(FTP_PARAM *ftpParam, BYTE *ftpTxData, WORD bufLen)

  Description:
        sets FTP Client routine to upload specific data buffer to file in FTP Server

  Precondition: FTP_PARAM *ftpParam
        structure passed to this function should fill with appropriate values

  Parameters:
        FTP_PARAM *ftpParam - structre defining FTP operation details.
        BYTE *ftpTxData - pointer to databuffer that send to seerver.
        WORD bufLen - number of bytes o send to server.

  Return Values:
        1 - Success
        0 - fail
 ***************************************************************************/
BOOL FTPC_UploadFromArray(FTP_PARAM *ftpParam, BYTE *ftpTxData, WORD bufLen)
{
    if (ftpcRunning)
        return 0;

    if (ftpParam->ftpServerName == NULL)
        return 0;

    if (ftpParam->ftpUserName == NULL)
        return 0;

    if (ftpParam->ftpUserPass == NULL)
        return 0;

    if (ftpParam->ftpRemoteFilePath == NULL)
    {
        strcpy(_ftpParam.ftpRemoteFilePath, "//");
        //return 0;
    }
    else
        strcpy(_ftpParam.ftpRemoteFilePath, ftpParam->ftpRemoteFilePath);

    if (_ftpParam.ftpRemoteFilePath[0] == '//' && _ftpParam.ftpRemoteFilePath[1] == '\0')
        _ftpcIsRootDir = TRUE;
    else
        _ftpcIsRootDir = FALSE;

    if (ftpParam->ftpRemoteFileName == NULL)
        return 0;

    //     if(ftpParam->ftpLocalFileName == NULL)
    //        return 0;

    strcpy(_ftpParam.ftpServerName, ftpParam->ftpServerName);
    strcpy(_ftpParam.ftpUserName, ftpParam->ftpUserName);
    strcpy(_ftpParam.ftpUserPass, ftpParam->ftpUserPass);

    strcpy(_ftpParam.ftpRemoteFileName, ftpParam->ftpRemoteFileName);
    strcpy(_ftpParam.ftpLocalFileName, ftpParam->ftpLocalFileName);

    _ftpParam.ftpFileMode = ftpParam->ftpFileMode;
    _ftpParam.ftpPort = ftpParam->ftpPort;


    if (_ftpParam.ftpPort == 0)
        _ftpParam.ftpPort = 21;

    if (ftpTxData[0] == 0x00) //if Tx buffer is empty 
        return 0;

    vDataBuffer = ftpTxData;
    LogSize = bufLen; //strlen(ftpTxData);
    //rcvFileLimit = rcvFileSize;
    rcvFileLimit = 0;
    //LogSize = strlen(ftpTxData);

    _ftpLastError = FTP_NO_ERROR;
    _ftpCnxFor = FTP_UPLOAD_ARRAY;

    if (strlen(_ftpParam.ftpServerName) > 6)
    {
        smFTPC = SM_FTPC_HOME; // start connection attempt                      
        ftpcRunning = TRUE; // tells main() to keep calling FTPClient()      
        return TRUE;
    }
    return 0;
}

/*****************************************************************************
  Function:
        BOOL FTPC_DownLoadToArray(FTP_PARAM *ftpParam, BYTE *ftpRxData, WORD bufLen)

  Description:
        sets FTP Client routine to download specific file data in buffer from FTP Server

  Precondition: FTP_PARAM *ftpParam
        structure passed to this function should fill with appropriate values

  Parameters:
        FTP_PARAM *ftpParam - structre defining FTP operation details.
        BYTE *ftpRxData - pointer to databuffer to receive data from seerver.
        WORD bufLen - number of bytes to receive from server.

  Return Values:
        1 - Success
        0 - fail
 ***************************************************************************/
BOOL FTPC_DownLoadToArray(FTP_PARAM *ftpParam, BYTE *ftpRxData, WORD bufLen)
{
    if (ftpcRunning)
        return 0;

    if (ftpParam->ftpServerName == NULL)
        return 0;

    if (ftpParam->ftpUserName == NULL)
        return 0;

    if (ftpParam->ftpUserPass == NULL)
        return 0;

    if (ftpParam->ftpRemoteFilePath == NULL)
    {
        strcpy(_ftpParam.ftpRemoteFilePath, "//");
        //return 0;
    }
    else
        strcpy(_ftpParam.ftpRemoteFilePath, ftpParam->ftpRemoteFilePath);

    if (_ftpParam.ftpRemoteFilePath[0] == '//' && _ftpParam.ftpRemoteFilePath[1] == '\0')
        _ftpcIsRootDir = TRUE;
    else
        _ftpcIsRootDir = FALSE;

    if (ftpParam->ftpRemoteFileName == NULL)
        return 0;

    //     if(ftpParam->ftpLocalFileName == NULL)
    //        return 0;

    strcpy(_ftpParam.ftpServerName, ftpParam->ftpServerName);
    strcpy(_ftpParam.ftpUserName, ftpParam->ftpUserName);
    strcpy(_ftpParam.ftpUserPass, ftpParam->ftpUserPass);

    strcpy(_ftpParam.ftpRemoteFileName, ftpParam->ftpRemoteFileName);
    strcpy(_ftpParam.ftpLocalFileName, ftpParam->ftpLocalFileName);

    _ftpParam.ftpFileMode = ftpParam->ftpFileMode;
    _ftpParam.ftpPort = ftpParam->ftpPort;


    if (_ftpParam.ftpPort == 0)
        _ftpParam.ftpPort = 21;

    vDataBuffer = ftpRxData;
    *vDataBuffer = '\0'; //make clear buffer;

    LogSize = bufLen; //strlen(ftpRxData);
    //rcvFileLimit = rcvFileSize;
    rcvFileLimit = 0;

    _ftpLastError = FTP_NO_ERROR;
    _ftpCnxFor = FTP_DOWNLOAD_ARRAY;

    if (strlen(_ftpParam.ftpServerName) > 6)
    {
        smFTPC = SM_FTPC_HOME; // start connection attempt                      
        ftpcRunning = TRUE; // tells main() to keep calling FTPClient()      
        return TRUE;
    }
    return 0;
}

FTP_LAST_ERROR FTP_GetLastCmdError(void)
{
    return (_ftpLastError);
}

FTP_CNX_STATUS FTP_GetConnectionStatus(void)
{
    return (_ftpCnxStatus);
}


#endif