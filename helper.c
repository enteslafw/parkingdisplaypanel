
#include <string.h>
#include <math.h>

const unsigned char CharacterArray1[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

void ToUpper(char *s)
{
    unsigned int strLen, i;

    strLen = strlen(s);

    for (i = 0; i < strLen; i++)
    {
        if (s[i] >= 'a' && s[i] <= 'z')
        {
            s[i] -= 32; // 'a' - 'A' = 32
        }
    }
}

// reverses a string 'str' of length 'len'

void reversen(char *str, int len)
{
    int i = 0, j = len - 1, temp;
    while (i < j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}

void reverse(char *s)
{
    unsigned int i, j, strLen;
    unsigned char c;

    strLen = strlen(s);
    for (i = 0, j = strLen - 1; i < j; i++, j--)
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void IntToAscii(int n, char *s)
{
    int sign;
    unsigned int i;

    if ((sign = n) < 0) // record sign
        n = -n; // make n positive
    i = 0;
    do
    { // generate digits in reverse order
        s[i++] = n % 10 + '0'; // get next digit
    }
    while ((n /= 10) > 0); // delete it
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}

// Converts a given integer x to string str[].  d is the number
// of digits required in output. If d is more than the number
// of digits in x, then 0s are added at the beginning.

int intToStr(int x, char str[], int d)
{
    int i = 0;
    int sign;

    if ((sign = x) < 0) // record sign
        x = -x; // make n positive
    while (x)
    {
        str[i++] = (x % 10) + '0';
        x = x / 10;
    }

    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';
    if (sign < 0)
        str[i++] = '-';
    reversen(str, i);
    str[i] = '\0';
    return i;
}

// Converts a floating point number to string.

void Ftoa(float n, char *res, int afterpoint)
{
    // Extract integer part
    int ipart = (int) n;

    // Extract floating part
    float fpart = n - (float) ipart;

    // convert integer part to string
    int i = intToStr(ipart, res, 0);

    if(fpart < 0)
    {
        fpart = -fpart;
    }
    // check for display option after point
    if (afterpoint != 0)
    {
        res[i] = '.'; // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);

        intToStr((int) fpart, res + i + 1, afterpoint);
    }
}

void LongToAscii(long n, char *s)
{
    long sign;
    unsigned int i;

    if ((sign = n) < 0) // record sign
        n = -n; // make n positive
    i = 0;
    do
    { // generate digits in reverse order
        s[i++] = n % 10 + '0'; // get next digit
    }
    while ((n /= 10) > 0); // delete it
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}

unsigned char ConvertToHexLowerNibble(unsigned char num)
{
    num = num & 0x0F;

    if (num < 10)
    {
        return ('0' + num);
    }
    else
    {
        num = num - 10;
        return ('A' + num);
    }
}

unsigned char ConvertToHexHigherNibble(unsigned char num)
{
    num = num >> 4;

    if (num < 10)
    {
        return ('0' + num);
    }
    else
    {
        num = num - 10;
        return ('A' + num);
    }
}

unsigned int CpyChArray(unsigned char *arr1, unsigned char *arr2, unsigned int cnt)
{
    unsigned int i;

    for (i = 0; i < cnt; i++)
    {
        arr1[i] = arr2[i];
    }

    return 1;
}

unsigned int CmpChArray(unsigned char *arr1, unsigned char *arr2, unsigned int cnt)
{
    unsigned int i;

    for (i = 0; i < cnt; i++)
    {
        if (arr1[i] != arr2[i])
        {
            return 0;
        }
    }

    return 1;
}

unsigned int IntStrPadZeros(char *str, unsigned char size)
{
    char zeroStr[] = "0000000000";
    char _size;
    char _sizeof;

    _sizeof = strlen(str);

    _size = (char) size - _sizeof;
    if (_size <= 0)
    {
        return 0;
    }
    else
    {
        zeroStr[(unsigned int)_size] = 0;
        strcat(zeroStr, str);
        strcpy(str, zeroStr);
        return 1;
    }
}

unsigned int IsDecimalNum(char *s)
{
    unsigned int strLen, i;

    strLen = strlen(s);

    for (i = 0; i < strLen; i++)
    {
        if (!((s[i] >= '0') && (s[i] <= '9')))
        {
            if (s[i] != '.')
            {
                if (s[i] != '-')
                {
                    if (s[i] != '+')
                    {
                        return 0;
                    }
                }
            }
        }
    }

    return 1;
}

unsigned int IsNum(char *s)
{
    unsigned int strLen, i;

    strLen = strlen(s);

    for (i = 0; i < strLen; i++)
    {

        if (!((s[i] >= '0') && (s[i] <= '9')))
        {
            return 0;
        }
    }

    return 1;
}

unsigned char* StringToken(unsigned char *str, unsigned char *token)
{
    static unsigned char *_pStr = NULL;
    static unsigned char flagDone = 0;

    unsigned char *start;

    if (NULL == str)
    {
        if (1 == flagDone)
        {
            return NULL;
        }

        _pStr++;
        start = _pStr;
    }
    else
    {
        flagDone = 0;
        _pStr = str;
        start = _pStr;
    }

    while ((*_pStr != *token))
    {
        if ((*_pStr == NULL))
        {
            flagDone = 1;
            return start;
        }
        _pStr++;
    }
    *_pStr = 0;

    return start;
}

void HexStrToAsciiStr(char *Hex, char *Ascii, int numOfBytes)
{
    char tmpByte;
    int i,j;
    
    for ( i=0, j=0; i < numOfBytes; i++,j=j+2)
    {
        tmpByte = (Hex[i] >> 4)&0x0F;
        Ascii[j] = CharacterArray1[tmpByte];
        tmpByte = (Hex[i])&0x0F;
        Ascii[j+1] = CharacterArray1[tmpByte];
    }
}

unsigned int CRC_WORD(unsigned char code_arr[], unsigned int len)
{
    unsigned int error_word, LSB;
    unsigned char i, j;
    error_word = 0xFFFF;
    for (j = 0; j < len; j++)
    {
        error_word = error_word^code_arr[j];
        for (i = 0; i < 8; i++)
        {
            LSB = error_word & 0x0001;
            if (LSB == 1)
            {
                error_word = error_word - 1;
            }
            error_word = error_word / 2; //shifting word towords lsb by 1
            if (LSB == 1)
            {
                error_word = error_word ^ 0xa001;

            }


        }
    }

    return (error_word);

}

void UintToAsciiFourBytes(int mV, char *str)
{
    unsigned int q, r;

    q = mV / 1000;
    r = mV % 1000;
    str[0] = '0' + q;
    q = r / 100;
    r = r % 100;
    str[1] = '0' + q;
    q = r / 10;
    r = r % 10;
    str[2] = '0' + q;
    str[3] = '0' + r;
}

