/*********************************************************************
 *
 *  Generic TCP Client Example Application
 *  Module for Microchip TCP/IP Stack
 *   -Implements an example HTTP client and should be used as a basis 
 *	  for creating new TCP client applications
 *	 -Reference: None.  Hopefully AN833 in the future.
 *
 *********************************************************************
 * FileName:        GenericTCPClient.c
 * Dependencies:    TCP, DNS, ARP, Tick
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *					Microchip C30 v3.12 or higher
 *					Microchip C18 v3.30 or higher
 *					HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2009 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *		ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *		used in conjunction with a Microchip ethernet controller for
 *		the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder     8/01/06	Original
 ********************************************************************/
#define __GENERICTCPCLIENT_C

#include "TCPIPConfig.h"

#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)

#include "TCPIP Stack/TCPIP.h"
#include "global_vars.h"
#include "log.h"
#include "helper.h"
#include "CircularBuffer.h"
#include "pc_comm.h"

// Defines the server to be accessed for this application
#ifdef WIFI_NET_TEST
static BYTE ServerName[] = "www" WIFI_NET_TEST_DOMAIN;
#else
//static BYTE ServerName[] = "192.168.0.8";
static BYTE ServerName[] = "www.timemuster.com";
#endif

// Defines the port to be accessed for this application
#if defined(STACK_USE_SSL_CLIENT)
static WORD ServerPort = HTTPS_PORT;
// Note that if HTTPS is used, the ServerName and URL 
// must change to an SSL enabled server.
#else
static WORD ServerPort = 12345;
#endif

static enum _TCPState
{
    SM_HOME = 0,
    SM_SOCKET_OBTAINED,
    SM_SOCKET_DEFAULT
} TCPState = SM_HOME;

CIRCULAR_BUFFER tcpClientRxBuf, tcpClientTxBuf;
static TCP_SOCKET MySocket = INVALID_SOCKET;

/*****************************************************************************
  Function:
        void GenericTCPClient(void)

  Summary:
        Implements a simple HTTP client (over TCP).

  Description:
        This function implements a simple HTTP client, which operates over TCP.  
        The function is called periodically by the stack, and waits for BUTTON1 
        to be pressed.  When the button is pressed, the application opens a TCP
        connection to an Internet search engine, performs a search for the word
        "Microchip" on "microchip.com", and prints the resulting HTML page to
        the UART.
	
        This example can be used as a model for many TCP and HTTP client
        applications.

  Precondition:
        TCP is initialized.

  Parameters:
        None

  Returns:
        None
 ***************************************************************************/
void GenericTCPClient(void)
{
    WORD w, x, i;
    BYTE ch;
    static DWORD Timer;

    static unsigned int numOfLogs = 0, _clientState = 0;
    static TRANSACTION trans;
    static TICKS checkLogsTickMs = 0;
    static TIME _time;
    static CALENDAR _cal;
    static char cmdStr[32], replyStr[32], tempArr[30];

    switch (TCPState)
    {
    case SM_SOCKET_DEFAULT:
        break;

    case SM_HOME:
        // Connect a socket to the remote TCP server
        //        MySocket = TCPOpen((DWORD) (PTR_BASE) & ServerName[0], TCP_OPEN_RAM_HOST, ServerPort, TCP_PURPOSE_GENERIC_TCP_CLIENT);
        MySocket = TCPOpen((DWORD) (PTR_BASE) & systemParam.srvIp[0], TCP_OPEN_RAM_HOST, atoi(systemParam.srvPort), TCP_PURPOSE_GENERIC_TCP_CLIENT);
        //        MySocket = TCPOpen((DWORD) (PTR_BASE) & systemParam.cloudIpUrl[0], TCP_OPEN_RAM_HOST, atoi(systemParam.cloudPort), TCP_PURPOSE_GENERIC_TCP_CLIENT);
        // Abort operation if no TCP socket of type TCP_PURPOSE_GENERIC_TCP_CLIENT is available
        // If this ever happens, you need to go add one to TCPIPConfig.h
        if (MySocket == INVALID_SOCKET)
            break;

#if defined(STACK_USE_UART)
        putrsUART((ROM char*) "\r\n\r\nConnecting using Microchip TCP API...\r\n");
#endif

        // Eat the first TCPWasReset() response so we don't
        // infinitely create and reset/destroy client mode sockets
        //        TCPWasReset(MySocket);

        //        TCPState++;
        TCPState = SM_SOCKET_OBTAINED;
        Timer = TickGet();
        checkLogsTickMs = MULTITASKER_GET_TICK();
        _clientState = 0;
        InitQueue(&tcpClientRxBuf);
        InitQueue(&tcpClientTxBuf);
        break;

    case SM_SOCKET_OBTAINED:

        if (systemFlag.clientCnxStatus)
        {
            // <editor-fold defaultstate="collapsed" desc="Check TCP Get">
            // Get count of RX bytes waiting
            w = TCPIsGetReady(MySocket);
            if (w > 0)
            {
                if (w > SizeEmptyInQueue(&tcpClientRxBuf))
                {
                    w = SizeEmptyInQueue(&tcpClientRxBuf);
                }
                for (i = 0; i < w; i++)
                {
                    if (TCPGet(MySocket, &ch))
                    {
                        PushToQueue(&tcpClientRxBuf, ch);
                    }
                    else
                    {
                        break;
                    }
                }
                Nop();
            }

            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="Check TCP Put">
            // Make certain the socket can be written to
            w = TCPIsPutReady(MySocket);
            if (w < 450)
            {
                //            break;
            }
            else
            {
                x = SizeOfQueue(&tcpClientTxBuf);
                if (x > 0)
                {
                    if (w > x)
                    {
                        w = x;
                    }

                    for (i = 0; i < w; i++)
                    {
                        PopFromQueue(&tcpClientTxBuf, &ch);
                        TCPPut(MySocket, ch);
                    }
                }
            }
            // </editor-fold>
        }

        // Wait for the remote server to accept our connection request
        if (!TCPIsConnected(MySocket))
        {
            // Time out if too much time is spent in this state
            if (TickGet() - Timer > 3 * TICK_SECOND)
            {
                // Close the socket so it can be used by other modules
                TCPDisconnect(MySocket);
                MySocket = INVALID_SOCKET;
                // TCPState--;
                TCPState = SM_HOME;
                //                TCPState = SM_SOCKET_DEFAULT;
                systemFlag.clientCnxStatus = 0;
            }
            break;
        }
        systemFlag.clientCnxStatus = 1;
        Timer = TickGet();
        break;
    }
}

unsigned int TcpClientGetEmptyByteCntTx(void)
{
    return SizeEmptyInQueue(&tcpClientTxBuf);
}

unsigned int TcpClientPut(unsigned char ch)
{
    return PushToQueue(&tcpClientTxBuf, ch);
}

unsigned int TcpClientPutS(char *str)
{
    return PushToQueueS(&tcpClientTxBuf, str);
}

unsigned int TcpClientIsAvailable(void)
{
    return SizeOfQueue(&tcpClientRxBuf);
}

unsigned int TcpClientGetS(char *str)
{
    unsigned int i = 0;

    while (!PopFromQueue(&tcpClientRxBuf, (str + (i++))));
    str[i] = 0;

    return i;
}

unsigned int TcpClientIsStrReceived(char *str)
{
    int pos=0;
    pos = StrStrQueue(str, &tcpClientRxBuf);
    if(pos>0)
    {
        return pos;
    }
    
    return 0;
}

unsigned int TcpClientDisconnect(void)
{
    if (0 == systemFlag.clientCnxStatus)
    {
        return 1;
    }
    else
    {
        TCPDisconnect(MySocket);
        MySocket = INVALID_SOCKET;
        TCPState = SM_SOCKET_DEFAULT;
        systemFlag.clientCnxStatus = 0;
        return 1;
    }
}

unsigned int TcpClientConnect(void)
{
    if (systemFlag.clientCnxStatus)
    {
        return 1;
    }
    else
    {
        TCPState = SM_HOME;
        return 1;
    }
}

unsigned int TcpClientGet(char *arr, unsigned int len)
{
    unsigned int i;

    for (i = 0; i < len; i++)
    {
        if (!PopFromQueue(&tcpClientRxBuf, (arr + (i++))));
        {
            return i;
        }
    }

    return i;
}

static char _httpRes[513];

unsigned int TcpClientGetBody(char *body)
{
    unsigned int i;
    char *pCh;
    unsigned int bodyLen, flag;

    i = 0;
    while (!PopFromQueue(&tcpClientRxBuf, &_httpRes[i++]));
    _httpRes[i] = 0;

    pCh = strstr(_httpRes, "Content-Length: ");
    if (0 == pCh)
    {
        return 0;
    }

    i = 0;
    pCh = pCh + 16;
    for (i = 0, flag = 0; i < (strlen(_httpRes) - 16); i++)
    {
        if ('\r' == pCh[i])
        {
            pCh[i] = 0;
            if (IsNum(pCh))
            {
                bodyLen = atoi(pCh);
                pCh += (strlen(pCh) + 1);
                flag = 1;
            }
            else
            {
                return 0;
            }
            break;
        }
    }

    if (0 == flag)
    {
        return 0;
    }

    pCh = strstr(pCh, "\r\n\r\n");
    if (0 == pCh)
    {
        return 0;
    }

    *(pCh + 4 + bodyLen) = 0;
    strcpy(body, pCh + 4);

    return bodyLen;
}
#endif	//#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)
