/* 
 * File:   IO.h
 * Author: Umesh
 *
 * Created on August 20, 2016, 4:48 PM
 */

#ifndef IO_H
#define	IO_H

#ifdef	__cplusplus
extern "C" {
#endif

    typedef union _DI_STATUS
    {
        char diStat;
        struct
        {
            char in1Stat : 1;
            char in2Stat : 1;
            char in3Stat : 1;
            char in4Stat : 1;
            char in5Stat : 1;
            char in6Stat : 1;
            char in7Stat : 1;
            char in8Stat : 1;
        };
    }DI_STATUS;

    extern void TASK_CheckInputs(void *pArgStr);

#ifdef	__cplusplus
}
#endif

#endif	/* IO_H */

