
#ifndef _HELPER_H
#define _HELPER_H

extern void IntToAscii(int n, char *s);
extern void LongToAscii(long n, char *s);
extern unsigned char ConvertToHexLowerNibble(unsigned char num);
extern unsigned char ConvertToHexHigherNibble(unsigned char num);
extern unsigned int CmpChArray(unsigned char *arr1, unsigned char *arr2, unsigned int cnt);
extern unsigned int CpyChArray(unsigned char *arr1, unsigned char *arr2, unsigned int cnt);
extern unsigned int IntStrPadZeros(char *str, unsigned char size);
extern void ToUpper(char *s);
extern unsigned int IsNum(char *s);
extern unsigned int IsDecimalNum(char *s);
extern unsigned char* StringToken(unsigned char *str, unsigned char *token);
extern void Ftoa(float n, char *res, int afterpoint);
extern void HexStrToAsciiStr(char *Hex, char *Ascii, int numOfBytes);
extern unsigned int CRC_WORD(unsigned char code_arr[], unsigned int len);
extern void UintToAsciiFourBytes(int mV, char *str);

#endif  //  _HELPER_H
