
#ifndef _CIRCULAR_BUFFER_H
#define _CIRCULAR_BUFFER_H

#define BUFFER_SIZE 2048

typedef struct {
    char buffer[BUFFER_SIZE];
    unsigned int start;
    unsigned int end;
    unsigned int current;
} CIRCULAR_BUFFER;

extern void InitQueue(CIRCULAR_BUFFER *cirBuff);
extern unsigned int PushToQueue(CIRCULAR_BUFFER *cirBuff, char ch);
extern unsigned int PopFromQueue(CIRCULAR_BUFFER *cirBuff, \
                                                            char *pCh);
extern unsigned int PushToQueueS(CIRCULAR_BUFFER *cirBuff, \
                                                            char *str);
extern unsigned int GetFromQueue(CIRCULAR_BUFFER *cirBuff, char *pCh, \
            unsigned int loc);

extern unsigned int SizeOfQueue(CIRCULAR_BUFFER *cirBuff);
extern unsigned int SizeEmptyInQueue(CIRCULAR_BUFFER *cirBuff);
extern void ClearQueue(CIRCULAR_BUFFER *cirBuff);
extern unsigned int PeekQueue(CIRCULAR_BUFFER *cirBuff, char *str, unsigned int size);

extern unsigned int SeeLastInQueue(CIRCULAR_BUFFER *cirBuff, \
                                                            char *pCh);

extern unsigned int UpdateLastInQueue(CIRCULAR_BUFFER *cirBuff, \
                                                            char ch);

//Place holders for Future Development
extern unsigned int SeekQueue(void);
extern unsigned int FindInQueue(void);
extern unsigned int StrCpyQueue(char *DestStr, CIRCULAR_BUFFER *cirBuff);
extern unsigned int StrStrQueue(char *searchStr, CIRCULAR_BUFFER *cirBuff);

#endif	// _CIRCULAR_BUFFER_H
