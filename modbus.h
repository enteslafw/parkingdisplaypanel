/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _EXAMPLE_FILE_NAME_H    /* Guard against multiple inclusion */
#define _EXAMPLE_FILE_NAME_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    extern unsigned char ModbusPortOpen(unsigned long baudRateToSet, unsigned long timeoutMs);
    extern void ModbusGetCh(char *ch);
    extern void ModbusPutCh(char ch);
    extern void ModbusPutS(char *s);
    extern unsigned int ModbusGetS(char *s);
    extern void ModbusPutNum(unsigned int n);
    extern unsigned int ModbusIsAvailable(void);
    extern void ModbusClearRxBuff(void);

    extern void ExecuteModbusPacket(void);
    
    extern unsigned int ModbusPresetMultipleRegisters(unsigned char slaveId, unsigned int startAdd, unsigned int noOfReg, unsigned char *dataBytes);

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
