#include "rtc.h"
#include "global_vars.h"

const char FirmwareVersionBuildDate[] = "v1.0/ " __DATE__ " " __TIME__; 
const char firmwareVersion[] = "1.0";
const char firmwareBuild[] = __DATE__ " " __TIME__;

TIME rtcTime, setRtcTime;
CALENDAR rtcCal, setRtcCal;
unsigned int setRtcDay;
SYSTEM_PARAM systemParam;
SYSTEM_FLAG systemFlag;
unsigned long timeInSec = 0;
TICKS mainTimeStampMs=0;
unsigned int totalLogsInMem=0;
unsigned int updateKeyFrmSrvStatus=20;
DI_STATUS curInStat, PrevInStat;
