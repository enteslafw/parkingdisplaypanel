//OM GAN GANPTAYE NAMAHA

/************************ HEADERS **********************************/
//#include "WirelessProtocols/Console.h"
//#include "SystemProfile.h"
#include "Compiler.h"
#include "GenericTypeDefs.h"
#include "HardwareProfile.h"
#include "global_vars.h"
#include "multitasker.h"
#include "CircularBuffer.h"
#include "En_Consol.h"
#include "helper.h"

#define U4RX_Clear_Intr_Status_Bit IFS2bits.U4RXIF=0;
#define U4TX_Clear_Intr_Status_Bit IFS2bits.U4TXIF=0;

static TICKS _uart4TimeoutMs = 10, _timeoutMs;

static CIRCULAR_BUFFER _uart4RxBuff, _uart4TxBuff;

static unsigned int _Uart4PutCh(unsigned char ch);
static unsigned int _Uart4PutS(unsigned char *str);
static unsigned int _Uart4GetS(char *str);
static unsigned int _Uart4GetCh(char *pCh);

// timer module needs to be initialized before calling this function

unsigned char PcCommPortOpen(unsigned long baudRateToSet, unsigned long timeoutMs)
{
    unsigned int status;
    unsigned long closestSpbrgValue;

    _TRISD15 = 0;
    _TRISD14 = 1;

    InitQueue(&_uart4RxBuff);
    InitQueue(&_uart4TxBuff);

    _uart4TimeoutMs = timeoutMs;

    //    CloseUART2();
    //    //disable UART if enabled previously
    //    U2TX_Clear_Intr_Status_Bit;
    //
    //    ConfigIntUART2(UART_RX_INT_EN | UART_RX_INT_PR6 | UART_TX_INT_EN | UART_TX_INT_PR6);
    //    OpenUART2(UART_EN | UART_BRGH_FOUR, UART_TX_ENABLE, u2brg); //configure UART and enable it

    //    UARTConfigure(UART4, UART_ENABLE_PINS_TX_RX_ONLY);
    //    UARTSetFifoMode(UART4, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    //    UARTSetLineControl(UART4, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    //    UARTSetDataRate(UART4, GetPeripheralClock(), baudRateToSet);
    //    UARTEnable(UART4, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));

    U4MODE = 0x8000; // Set UARTEN.  Note: this must be done before setting UTXEN
    IPC12bits.U4IP = 7; // Priority level 6
    U4STA = 0x00001400; // RXEN set, TXEN set
    closestSpbrgValue = ((GetPeripheralClock() + 8 * baudRateToSet) / 16 / baudRateToSet - 1);
    U4BRG = closestSpbrgValue;
    IEC2bits.U4RXIE = 1;

    _timeoutMs = MULTITASKER_GET_TICK();

}

void PcComPutNum(unsigned int n)
{
    char str[15];

    IntToAscii(n, str);
    PcCommPutS(str);
}

void PcCommPutS(char *s)
{
    _Uart4PutS(s);
}

void PcCommPutROMString(ROM char* str)
{
    _Uart4PutS(str);
}

void PcCommPutStringLen(BYTE *s, int Len)
{
    BYTE c;

    while (Len-- > 0)
    {
        c = *s++;

        PushToQueue(&_uart4TxBuff, c);
    }
    INTEnable(INT_SOURCE_UART_TX(UART4), INT_ENABLED);
    IFS2bits.U4TXIF = 1;
}

void PcCommPrintHexChar(BYTE toPrint)
{
    char hex[2], ascii[3]; 
//    BYTE PRINT_VAR;
//    PRINT_VAR = toPrint;
//    toPrint = (toPrint >> 4)&0x0F;
//    ConsolePut(CharacterArray[toPrint]);
//    toPrint = (PRINT_VAR)&0x0F;
//    ConsolePut(CharacterArray[toPrint]);
//    return;
    hex[0] = toPrint;
    HexStrToAsciiStr(hex, ascii, 1);
    PcCommPutStringLen(ascii, 2);
}

void PcCommPrintMAC(BYTE *s, BYTE Len)
{
    int i;
    for (i = Len; i > 0; i--)
    {
        PcCommPrintHexChar(s[i - 1]);
    }
}

void PcCommPutCh(char ch)
{
    _Uart4PutCh(ch);
}

void PcCommPutSRx(char *str)
{
    while (*str)
    {
        PushToQueue(&_uart4RxBuff, *str);
        str++;
    }
}

void PcCommGetCh(char *ch)
{
    _Uart4GetCh(ch);
}

unsigned int PcCommIsAvailable(void)
{
    return SizeOfQueue(&_uart4RxBuff);
}

void PcCommClearRxBuff(void)
{
    ClearQueue(&_uart4RxBuff);
}

unsigned int PcCommGetS(char *str)
{
    return _Uart4GetS(str);
}

unsigned int  PcCommGetStringLen(char *str, int Len)
{
    unsigned int status = 0;
    unsigned int i = 0;

    while ((0 == status) && (Len-- > 0))
    {
        status = PopFromQueue(&_uart4RxBuff, (str + i));
        if (1 == status)
        {
            str[i] = 0;
        }
        else
        {
            i++;
        }
    }

    return i;
}

static unsigned int _Uart4GetCh(char *pCh)
{
    unsigned int status;

    status = PopFromQueue(&_uart4RxBuff, pCh);

    return status;
}

static unsigned int _Uart4PutCh(unsigned char ch)
{
    PushToQueue(&_uart4TxBuff, ch);
    //    EnableIntU2TX;
    INTEnable(INT_SOURCE_UART_TX(UART4), INT_ENABLED);
    IFS2bits.U4TXIF = 1;

    return 1;
}

static unsigned int _Uart4GetS(char *str)
{
    unsigned int status = 0;
    unsigned int i = 0;

    while (0 == status)
    {
        status = PopFromQueue(&_uart4RxBuff, (str + i));
        if (1 == status)
        {
            str[i] = 0;
        }
        else
        {
            i++;
        }
    }

    return i;
}

static unsigned int _Uart4PutS(unsigned char *str)
{
    while (*str)
    {
        PushToQueue(&_uart4TxBuff, *str);
        str++;
    }
    //    EnableIntU2TX;
    INTEnable(INT_SOURCE_UART_TX(UART4), INT_ENABLED);
    IFS2bits.U4TXIF = 1;

    return 1;
}

//
// Interrupt Service routine for UART2 Transmission
//

void _U4TXInterrupt(void)
{
    static char ch = 0;

    U4TX_Clear_Intr_Status_Bit; // clear the interrupt status of UART4 TX
    if (PopFromQueue(&_uart4TxBuff, &ch))
    {
        //DisableIntU2TX; // disable the UART TX interrupt after end of complete
        INTEnable(INT_SOURCE_UART_TX(UART4), INT_DISABLED);
        // transmission
    }
    else
    {
        //while (BusyUART4()); // wait till the UART is busy
        while (!UARTTransmitterIsReady(UART4));

        //WriteUART4((unsigned int) ch); // Transmit the data
        UARTSendDataByte(UART4, (unsigned int) ch);
    }
}


//
// Interrupt Service routine for UART2 reception
//

void _U4RXInterrupt(void)
{
    static unsigned char rxdCh = 0;

    //while (!DataRdyUART2()); //wait for data reception on RX
    //    while(!U4STAbits.URXDA);

    rxdCh = UARTGetDataByte(UART4);    

    if ('\r' == rxdCh)
    {
        systemFlag.pcCommCmdRxd = 1;
    }
    else
    {
        PushToQueue(&_uart4RxBuff, rxdCh);
    }

    U4RX_Clear_Intr_Status_Bit; //clear the interrupt status of UART RX after reading
    
    //    while (!DataRdyUART2()); //wait for data reception on RX
    //    PushToQueue(&_uart4RxBuff, UARTGetDataByte(UART4));
}

void __attribute((interrupt(IPL7SOFT), vector(_UART_4_VECTOR), nomips16)) U4Interrupt(void)
{
    if (IFS2bits.U4RXIF)
        _U4RXInterrupt();
    if (IEC2bits.U4TXIE)
    {
        if (IFS2bits.U4TXIF)
            _U4TXInterrupt();
    }
}

