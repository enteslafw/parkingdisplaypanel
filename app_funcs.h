/* 
 * File:   app_funcs.h
 * Author: neer
 *
 * Created on 1 March, 2016, 4:59 PM
 */

#ifndef APP_FUNCS_H
#define	APP_FUNCS_H

#ifdef	__cplusplus
extern "C" {
#endif
    extern void AppInit(void);
    extern unsigned int AppMakeDefaultSystemParamFile(void);
    extern unsigned int AppGetSystemParamFrmFile(SYSTEM_PARAM *sysPar);
    extern unsigned int AppMakeDefaultLogFile(void);
    extern unsigned int AppSaveSystemParamToFile(SYSTEM_PARAM *sysPar);

#ifdef	__cplusplus
}
#endif

#endif	/* APP_FUNCS_H */

